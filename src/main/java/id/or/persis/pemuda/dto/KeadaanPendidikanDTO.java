package id.or.persis.pemuda.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * Project : peta-dakwah-be
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/18/22
 * Time: 17:35
 * To change this template use File | Settings | File Templates.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class KeadaanPendidikanDTO {
    private UUID id;

    private PimpinanCabangDTO pc;

    private KelurahanDTO kelurahan;

    private Integer jumlahRa;

    private Integer jumlahDny;

    private Integer jumlahMi;

    private Integer jumlahSd;

    private Integer jumlahMts;

    private Integer jumlahSmp;

    private Integer jumlahMa;

    private Integer jumlahMln;

    private Integer jumlahSma;

    private Integer total;

    private Integer jumlahSantriRa;

    private Integer jumlahSantriDny;

    private Integer jumlahSantriMi;

    private Integer jumlahSantriSd;

    private Integer jumlahSantriMts;

    private Integer jumlahSantriSmp;

    private Integer jumlahSantriMa;

    private Integer jumlahSantriMln;

    private Integer jumlahSantriSma;

    private Date periodeStart;

    private Date periodeEnd;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    private LocalDateTime deletedAt;

    private String createdBy;

    private String updatedBy;

    private String deletedBy;

    private boolean deleted;

}
