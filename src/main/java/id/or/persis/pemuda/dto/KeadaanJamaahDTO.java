package id.or.persis.pemuda.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class KeadaanJamaahDTO {
    private UUID id;

    private PimpinanCabangDTO pc;

    private KelurahanDTO kelurahan;

    private Integer jmlMesjid;

    private Integer jmlMusholla;

    private Integer jmlPj;

    private Integer jmlAnggotaPersis;

    private Integer jmlAnggotaPersistri;

    private Integer jmlAnggotaPemuda;

    private Integer jmlAnggotaPemudi;

    private Integer jmlMuballighPersis;

    private Integer jmlMuballighPersistri;

    private Integer jmlMuballighPemuda;

    private Integer jmlMuballighPemudi;

    private Date periodeStart;

    private Date periodeEnd;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    private LocalDateTime deletedAt;

    private String createdBy;

    private String updatedBy;

    private String deletedBy;

    private Boolean deleted;
}
