package id.or.persis.pemuda.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AngketCabangDTO {
    private UUID id;

    private PimpinanCabangDTO pc;

    private KecamatanDTO kecamatan;

    private Integer tahunBerdiri;

    private String alamat;

    private String namaBidangDakwah;

    private String noHp1;

    private String noHp2;

    private String email;

    private String q1;

    private String q1AdditionalMessage;

    private String q2;

    private String q2AdditionalMessage;

    private String q3;

    private String q4;

    private String q5;

    private String q5AdditionalMessage;

    private String q6;

    private String q6AdditionalMessage;

    private String q7;

    private String q8;

    private String q8AdditionalMessage;

    private String q9;

    private String q9AdditionalMessage;

    private String q10;

    private String q11;

    private String q12;

    private String q12AdditionalMessage;

    private String q13;

    private String q13AdditionalMessage;

    private String q14;

    private String q14AdditionalMessage;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    private LocalDateTime deletedAt;

    private String createdBy;

    private String updatedBy;

    private String deletedBy;

    private Boolean deleted;
}
