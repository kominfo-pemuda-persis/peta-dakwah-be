package id.or.persis.pemuda.dto;

import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * Project : peta-dakwah-be
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/21/22
 * Time: 00:13
 * To change this template use File | Settings | File Templates.
 */
@Data
public class ProvinsiDTO {
    private String id;

    private String kode;

    private String nama;
}
