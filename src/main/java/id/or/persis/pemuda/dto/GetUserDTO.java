package id.or.persis.pemuda.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import id.or.persis.pemuda.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GetUserDTO {
    private UUID id;
    private String npa;

    private PimpinanCabangDTO pc;

    private KelurahanDTO kelurahan;

    private String username;

    private String namaLengkap;

    private JenisKelaminEnumDTO jenisKelamin;

    private String tempatLahir;

    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date date;

    private Boolean active;

    private OtonomEnumDTO otonom;

    private String email;

    private String photo;

    private String phone;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    private LocalDateTime deletedAt;

    private String createdBy;

    private String updatedBy;

    private String deletedBy;

    private Collection<Role> roles;
}
