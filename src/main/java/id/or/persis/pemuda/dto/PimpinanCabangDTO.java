package id.or.persis.pemuda.dto;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created by IntelliJ IDEA.
 * Project : peta-dakwah-be
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/21/22
 * Time: 00:10
 * To change this template use File | Settings | File Templates.
 */
@Data
public class PimpinanCabangDTO {
    private Integer id;

    private String kdPc;

    private PimpinanDaerahDTO pd;

    private String namaPc;

    private LocalDate diresmikan;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;
}
