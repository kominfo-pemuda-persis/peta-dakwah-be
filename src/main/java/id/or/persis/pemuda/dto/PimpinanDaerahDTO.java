package id.or.persis.pemuda.dto;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created by IntelliJ IDEA.
 * Project : peta-dakwah-be
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/21/22
 * Time: 00:09
 * To change this template use File | Settings | File Templates.
 */
@Data
public class PimpinanDaerahDTO {
    private Integer id;

    private String kdPd;

    private PimpinanWilayahDTO pw;

    private String namaPd;

    private LocalDate diresmikan;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;
}
