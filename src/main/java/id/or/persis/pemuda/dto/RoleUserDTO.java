package id.or.persis.pemuda.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleUserDTO {
    private UUID userId;
    private List<String> roleNames;
}
