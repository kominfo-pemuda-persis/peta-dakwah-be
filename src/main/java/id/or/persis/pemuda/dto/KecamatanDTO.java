package id.or.persis.pemuda.dto;

import jakarta.persistence.Column;
import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * Project : peta-dakwah-be
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/21/22
 * Time: 00:11
 * To change this template use File | Settings | File Templates.
 */
@Data
public class KecamatanDTO {
    private String id;

    private KotaDTO kota;

    @Column(name = "kode", nullable = false, length = 50)
    private String kode;

    @Column(name = "nama", nullable = false)
    private String nama;
}
