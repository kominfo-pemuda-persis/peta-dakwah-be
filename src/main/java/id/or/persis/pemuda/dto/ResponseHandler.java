package id.or.persis.pemuda.dto;

import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : abids-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/04/22
 * Time: 19.49
 */
public class ResponseHandler {
    private ResponseHandler() {
        throw new IllegalStateException("ResponseHandler class");
    }

    public static ResponseEntity<Object> generateResponse(String message, HttpStatusCode status,
                                                          HttpStatusCode httpStatusCode, Object responseObj) {
        Map<String, Object> map = new HashMap<>();
        map.put("message", message);
        map.put("status", status);
        map.put("httpCode", httpStatusCode.value());
        map.put("data", responseObj);

        return new ResponseEntity<>(map, status);
    }
}
