package id.or.persis.pemuda.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 * Project : peta-dakwah-be
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/5/22
 * Time: 17:29
 * To change this template use File | Settings | File Templates.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VillagesCoordinateDTO {
    private String kode;

    private String nama;

    private String ibukota;

    private Double latitude;

    private Double longitude;

    private Float elevation;

    private Integer timeZone;

    private Double luas;

    private Double penduduk;

    private String path;

    private Integer status;
}
