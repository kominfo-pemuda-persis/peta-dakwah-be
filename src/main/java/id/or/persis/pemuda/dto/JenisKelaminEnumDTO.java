package id.or.persis.pemuda.dto;

public enum JenisKelaminEnumDTO {
    /* Laki-laki (L), Perempuan (P) */
    L, P
}
