package id.or.persis.pemuda.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import id.or.persis.pemuda.entity.Kelurahan;
import id.or.persis.pemuda.entity.PimpinanCabang;
import id.or.persis.pemuda.enumeration.*;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * Project : peta-dakwah-be
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/18/22
 * Time: 11:50
 * To change this template use File | Settings | File Templates.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor

public class ProfilMuballighDTO {
    
    private UUID id;
    
    private PimpinanCabang pc;

    private Kelurahan kelurahan;

    @NotEmpty(message = "Nama Lengkap is required")
    @NotNull(message = "Nama Lengkap can not be null!!")
    private String namaLengkap;

    @NotEmpty(message = "No HP is required")
    @NotNull(message = "No HP can not be null!!")
    private String noHp1;

    private String noHp2;

    @NotEmpty(message = "No HP is required")
    @NotNull(message = "No HP can not be null!!")
    private String email;

    private JenisKelamin jenisKelamin;

    @NotEmpty(message = "Tempat Lahir is required")
    @NotNull(message = "Tempat Lahir can not be null!!")
    private String tempatLahir;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date tanggalLahir;

    @Column(name = "status_marital", nullable = false, columnDefinition = "ENUM('MENIKAH', 'BELUM_MENIKAH', 'DUDA', 'JANDA')")
    @Enumerated(EnumType.STRING)
    private StatusMarital statusMarital;

    @Column(name = "pendidikan_terakhir", nullable = false, columnDefinition = """
            ENUM('SD', 'MI', 'SMP', 'MTS', 'SMA',\
             'SMK', 'MA', 'MLN', 'D1', 'D2', 'D3', 'D4', 'S1', 'S2', 'S3')\
            """)
    @Enumerated(EnumType.STRING)
    private PendidikanTerakhir pendidikanTerakhir;

    private String pendidikanLuarSekolah;

    private String pekerjaanPokok;

    private String pekerjaanSambilan;

    private Boolean anggota;

    @Column(name = "otonom", columnDefinition = "ENUM('PERSIS','PERSISTRI', 'PEMUDA', 'PEMUDI')", nullable = false)
    @Enumerated(EnumType.STRING)
    private Otonom otonom;

    @Column(name = "status_dalam_jamiyyah", nullable = false, columnDefinition = """
            ENUM('ANGGOTA_BIASA', 'TASYKIL', \
            'BUKAN_ANGGOTA')\
            """)
    @Enumerated(EnumType.STRING)
    private StatusJamiyyah statusDalamJamiyyah;

    @Column(name = "upaya", nullable = false, columnDefinition = "ENUM('KITAB', 'BUKU', 'HALAQOH', 'LAINNYA')")
    @Enumerated(EnumType.STRING)
    private Upaya upaya;

    private String upayaAdditionalMessage;

    private String internal;

    private String internalAdditionalMessage;

    private String eksternal;

    private String eksternalAdditionalMessage;

    private String pembinaanInternal;

    private String pembinaanEksternal;

    private String pembinaanPC;

    private String pembinaanPCAdditionalMessage;

    private String pembinaanPD;

    private String pembinaanPDAdditionalMessage;

    private Integer lamaAktifBertabligh;

    private Integer frequensiBertabligh;

    private String selainMuballighPc;

    private String selainMuballighPcAdditionalMessage;

    private String diundangPcLain;

    private String diundangPcLainAdditionalMessage;

    private String diundangPdLain;

    private String diundangPdLainAdditionalMessage;

    private String bentukTabligh;

    private String bentukTablighAdditionalMessage;

    private String metodeTabligh;

    private String metodeTablighAdditionalMessage;

    private String temaTabligh;

    private String temaTablighAdditionalMessage;

    private String kendala;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    private LocalDateTime deletedAt;

    private String createdBy;

    private String updatedBy;

    private String deletedBy;

    private Boolean deleted;

}
