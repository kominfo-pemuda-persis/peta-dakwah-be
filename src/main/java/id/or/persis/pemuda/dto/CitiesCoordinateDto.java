package id.or.persis.pemuda.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CitiesCoordinateDto {

    private String kode;

    private String nama;

    private String ibukota;

    private Double latitude;

    private Double longitude;

    private Float elevation;

    private Integer timeZone;

    private Double luas;

    private Double penduduk;

    private String path;

    private Integer status;
}

