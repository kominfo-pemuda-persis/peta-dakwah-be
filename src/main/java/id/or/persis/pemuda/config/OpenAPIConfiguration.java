package id.or.persis.pemuda.config;

import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Arrays;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : peta-dakwah-be
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 1/20/23
 * Time: 09:11
 * To change this template use File | Settings | File Templates.
 */
@SecurityScheme(
        name = "Bearer Authentication",
        type = SecuritySchemeType.HTTP,
        bearerFormat = "JWT",
        scheme = "bearer"
)
@Configuration
public class OpenAPIConfiguration {
    @Value("${app.domain-list-fe}")
    List<String> domainListFe;

    @Value("${app.local}")
    private String localhost;

    @Value("${app.dev}")
    private String development;

    @Autowired
    Environment env;
    @Value("${app.prod}")
    private String prod;

    @Bean
    public OpenAPI customOpenAPI(@Value("${application-description}") String appDescription,
                                 @Value("${application-version}") String appVersion) {
        io.swagger.v3.oas.models.servers.Server localServer = new io.swagger.v3.oas.models.servers.Server();
        localServer.setDescription("local");
        localServer.setUrl(localhost);

        io.swagger.v3.oas.models.servers.Server devServer = new io.swagger.v3.oas.models.servers.Server();
        devServer.setDescription("dev");
        devServer.setUrl(development);

        io.swagger.v3.oas.models.servers.Server prodServer = new io.swagger.v3.oas.models.servers.Server();
        devServer.setDescription("prod");
        devServer.setUrl(prod);

        return new OpenAPI()
                .addServersItem(new Server().description("LOCAL").url(localhost))
                .addServersItem(new Server().description("DEV").url(development))
                .addServersItem(new Server().description("PROD").url(prod))
                .info(new Info()
                        .title("PETA DAKWAH REST API")
                        .version(appVersion)
                        .description(appDescription)
                        .contact(new io.swagger.v3.oas.models.info.Contact()
                                .name("Kominfo PP Pemuda Persis")
                                .url("https://s.id/pppemudapersis")
                                .email("pp.pemuda@persis.or.id"))
                        .termsOfService("http://swagger.io/terms/")
                        .license(new License().name("Apache 2.0").url("http://springdoc.org")));

    }

    @Bean
    public CorsFilter corsFilter() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        final CorsConfiguration config = new CorsConfiguration();
        config.setAllowedOriginPatterns(domainListFe);
        config.setAllowedHeaders(Arrays.asList("Origin", "Content-Type", "Accept", "Authorization"));
        config.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "OPTIONS", "DELETE", "PATCH"));
        config.setAllowCredentials(true);
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }

}
