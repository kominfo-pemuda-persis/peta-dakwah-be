package id.or.persis.pemuda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PetaDakwahBeApplication {

    public static void main(String[] args) {
        SpringApplication.run(PetaDakwahBeApplication.class, args);
    }

}
