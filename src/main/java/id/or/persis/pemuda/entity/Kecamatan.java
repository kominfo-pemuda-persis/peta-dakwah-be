package id.or.persis.pemuda.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.persistence.*;
import lombok.Data;

import java.io.Serializable;

@Entity
@Table(name = "kecamatan")
@Data
public class Kecamatan implements Serializable {
    private static final long serialVersionUID = 761512083895897310L;

    @Id
    @Column(name = "id", nullable = false, length = 36)
    private String id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, optional = false)
//    @JoinColumn(name = "id_kota", referencedColumnName = "id")

    @JoinColumn(
            name = "id_kota",
            referencedColumnName = "kode",
            foreignKey = @ForeignKey(name = "kota_kd_fk"),
            nullable = false)
    @JsonDeserialize(as = Kota.class)
    @JsonSerialize(as = Kota.class)
//    @JsonIgnore
//    @ToString.Exclude
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Kota kota;

    @Column(name = "kode", nullable = false, length = 50)
    private String kode;

    @Column(name = "nama", nullable = false)
    private String nama;
}
