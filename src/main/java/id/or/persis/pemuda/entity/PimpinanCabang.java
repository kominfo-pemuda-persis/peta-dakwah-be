package id.or.persis.pemuda.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.persistence.*;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "pc")
@Data
public class PimpinanCabang implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "kd_pc", nullable = false, length = 10)
    private String kdPc;

    @OneToOne
    @JoinColumn(
            name = "kd_pd",
            referencedColumnName = "kd_pd",
            foreignKey = @ForeignKey(name = "pd_kd_pd_fk"),
            nullable = false)
    @JsonDeserialize(as = PimpinanDaerah.class)
    @JsonSerialize(as = PimpinanDaerah.class)
    private PimpinanDaerah pd;

    @Column(name = "nama_pc", nullable = false)
    private String namaPc;

    @Column(name = "diresmikan", nullable = false)
    private LocalDate diresmikan;

    @CreatedDate
    @Column(name = "created_at", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime createdAt;

    @LastModifiedDate
    @Column(name = "updated_at", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    private LocalDateTime updatedAt;
}
