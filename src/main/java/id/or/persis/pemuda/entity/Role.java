package id.or.persis.pemuda.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.annotations.UuidGenerator;
import org.hibernate.type.SqlTypes;

import java.util.UUID;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "role")
public class Role {

    @Id
    @UuidGenerator
    @Basic
    @JdbcTypeCode(SqlTypes.VARCHAR)
    private UUID id;

    @Column(name = "role", length = 50, unique = true)
    private String roleName;

    @Column(name = "description", length = 250)
    private String description;
}
