package id.or.persis.pemuda.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.persistence.*;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "pd")
@Data
public class PimpinanDaerah implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "kd_pd", nullable = false)
    private String kdPd;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(
            name = "kd_pw",
            referencedColumnName = "kd_pw",
            foreignKey = @ForeignKey(name = "pd_kd_pw_fk"),
            nullable = false)
    @JsonDeserialize(as = PimpinanWilayah.class)
    @JsonSerialize(as = PimpinanWilayah.class)
    private PimpinanWilayah pw;

    @Column(name = "nama_pd", nullable = false)
    private String namaPd;

    @Column(name = "diresmikan", nullable = false)
    private LocalDate diresmikan;

    @Column(name = "created_at", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime createdAt;

    @Column(name = "updated_at", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    private LocalDateTime updatedAt;
}
