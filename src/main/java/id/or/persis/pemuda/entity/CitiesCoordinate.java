package id.or.persis.pemuda.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "cities_coordinate")
public class CitiesCoordinate {

    @Id
    @Column(name = "kode", unique = true, length = 13)
    private String kode;

    @Column(name = "nama", length = 100)
    private String nama;

    @Column(name = "ibukota", length = 100)
    private String ibukota;

    @Column(name = "lat")
    private Double latitude;

    @Column(name = "lng")
    private Double longitude;

    @Column(name = "elv")
    private Float elevation;

    @Column(name = "tz", columnDefinition = "TINYINT(4)")
//    @Type(type = "org.hibernate.type.IntegerType")
    private Integer timeZone;

    @Column(name = "luas")
    private Double luas;

    @Column(name = "penduduk")
    private Double penduduk;

    @Lob
    @Column(name = "path", columnDefinition = "LONGTEXT")
    private String path;

    @Column(name = "status", columnDefinition = "TINYINT(4)", length = 2000)
//    @Type(type = "org.hibernate.type.IntegerType")
    private Integer status;
}
