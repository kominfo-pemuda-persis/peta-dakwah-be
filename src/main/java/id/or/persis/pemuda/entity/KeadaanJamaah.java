package id.or.persis.pemuda.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "keadaan_jamaah")
@SQLDelete(sql = "UPDATE keadaan_jamaah SET deleted = 1, deleted_at = CURRENT_TIMESTAMP WHERE id = ?")
public class KeadaanJamaah extends BaseEntity {

    @OneToOne
    @JoinColumn(
            name = "kode_pc",
            referencedColumnName = "kd_pc",
            foreignKey = @ForeignKey(name = "keadaan_jamaah_kd_pc_foreign"),
            nullable = false)
    @JsonDeserialize(as = PimpinanCabang.class)
    @JsonSerialize(as = PimpinanCabang.class)
    private PimpinanCabang pc;

    @OneToOne
    @JoinColumn(
            name = "kelurahan_id",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "keadaan_jamaah_id_foreign"),
            nullable = false)
    @JsonDeserialize(as = Kelurahan.class)
    @JsonSerialize(as = Kelurahan.class)
    private Kelurahan kelurahan;

    @Column(name = "jml_mesjid")
    private Integer jmlMesjid;

    @Column(name = "jml_musholla")
    private Integer jmlMusholla;

    @Column(name = "jml_pj")
    private Integer jmlPj;

    @Column(name = "jml_anggota_persis")
    private Integer jmlAnggotaPersis;

    @Column(name = "jml_anggota_persistri")
    private Integer jmlAnggotaPersistri;

    @Column(name = "jml_anggota_pemuda")
    private Integer jmlAnggotaPemuda;

    @Column(name = "jml_anggota_pemudi")
    private Integer jmlAnggotaPemudi;

    @Column(name = "jml_muballigh_persis")
    private Integer jmlMuballighPersis;

    @Column(name = "jml_muballigh_persistri")
    private Integer jmlMuballighPersistri;

    @Column(name = "jml_muballigh_pemuda")
    private Integer jmlMuballighPemuda;

    @Column(name = "jml_muballigh_pemudi")
    private Integer jmlMuballighPemudi;

    @Column(name = "periode_start")
    private LocalDate periodeStart;

    @Column(name = "periode_end")
    private LocalDate periodeEnd;

}
