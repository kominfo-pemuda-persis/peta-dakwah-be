package id.or.persis.pemuda.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.or.persis.pemuda.enumeration.JenisKelamin;
import id.or.persis.pemuda.enumeration.Otonom;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UuidGenerator;
import org.hibernate.type.SqlTypes;
import org.hibernate.type.descriptor.java.StringJavaType;
import org.hibernate.usertype.UserTypeLegacyBridge;
import org.hibernate.usertype.UserTypeSupport;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Types;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue
    @UuidGenerator
    @Column(name="id", nullable = false, updatable = false, columnDefinition = "VARCHAR(36)")
    @JdbcTypeCode(Types.VARCHAR)
    private UUID id;

    @Column(name = "npa", length = 25, unique = true)
    private String npa;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
    @JoinColumn(name = "kode_pc", referencedColumnName = "kd_pc")
    private PimpinanCabang pc;

    @ManyToOne(
            fetch = FetchType.LAZY
    )
    @JoinColumn(name = "kelurahan_id", referencedColumnName = "id")
    @JsonIgnore
    private Kelurahan kelurahan;

    @Column(name = "username", length = 50, unique = true)
    private String username;

    @Column(name = "nama_lengkap", length = 100)
    private String namaLengkap;

    @Enumerated(EnumType.STRING)
    @Column(name = "jenis_kelamin", columnDefinition = "ENUM('M','S')", nullable = false)
    private JenisKelamin jenisKelamin;

    @Column(name = "tempat_lahir", length = 50)
    private String tempatLahir;

    @Column(name = "tanggal_lahir")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Temporal(TemporalType.DATE)
    private Date date;

    @Column(name = "active", nullable = false)
    private Boolean active;

    @Enumerated(EnumType.STRING)
    @Column(name = "otonom", columnDefinition = "ENUM('PERSIS','PERSISTRI', 'PEMUDA', 'PEMUDI')", nullable = false)
    private Otonom otonom;

    @Column(name = "password", length = 255)
    private String password;

    @Column(name = "email", length = 100, unique = true)
    private String email;

    @Column(name = "photo", length = 255)
    private String photo;

    @Column(name = "phone", length = 15, unique = true)
    private String phone;

    //    @ManyToMany(fetch = FetchType.EAGER)
//    @JsonBackReference
    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
//    @JoinTable(name = "USER_ROLES", joinColumns = {
//            @JoinColumn(name = "USER_ID", referencedColumnName = "ID")}, inverseJoinColumns = {
//            @JoinColumn(name = "ROLES_ID", referencedColumnName = "ID")})
    private Collection<Role> roles = new ArrayList<>();

//    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
//    @JoinTable(name = "USER_ROLES", joinColumns = {
//            @JoinColumn(name = "USER_ID", referencedColumnName = "ID")}, inverseJoinColumns = {
//            @JoinColumn(name = "ROLES_ID", referencedColumnName = "ID")})
//    private Set<Role> roles = new HashSet<>();

    @CreatedDate
    @Column(name = "created_at", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime createdAt;

    @LastModifiedDate
    @Column(name = "updated_at", nullable = false, columnDefinition = """
            TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE \
            CURRENT_TIMESTAMP\
            """)
    private LocalDateTime updatedAt;

    @Column(name = "deleted_at")
    private LocalDateTime deletedAt;

    @CreatedBy
    @Column(name = "created_by", length = 50)
    private String createdBy;

    @LastModifiedBy
    @Column(name = "updated_by", length = 50)
    private String updatedBy;

    @CreatedBy
    @Column(name = "deletedBy", length = 50)
    private String deletedBy;

    public User withId(UUID id) {
        this.id = id;

        return this;
    }

    public User deleted() {
        deletedAt = LocalDateTime.now();

        return this;
    }
}
