package id.or.persis.pemuda.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.or.persis.pemuda.enumeration.*;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.UuidGenerator;
import org.hibernate.type.SqlTypes;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "profil_muballigh")
@SQLDelete(sql = "UPDATE profil_muballigh SET deleted = 1, deleted_at = CURRENT_TIMESTAMP WHERE id = ?")
public class ProfilMuballigh {
    @Id
    @GeneratedValue
    @UuidGenerator
    @Column(name = "id", updatable = false, nullable = false, columnDefinition = "CHAR(36)")
    @Basic
    @JdbcTypeCode(SqlTypes.VARCHAR)
    private UUID id;

    @OneToOne
    @JoinColumn(
            name = "kode_pc",
            referencedColumnName = "kd_pc",
            foreignKey = @ForeignKey(name = "t_keadaan_pendidikan_kd_pc_foreign"),
            nullable = false)
    @JsonDeserialize(as = PimpinanCabang.class)
    @JsonSerialize(as = PimpinanCabang.class)
    private PimpinanCabang pc;

    @OneToOne
    @JoinColumn(
            name = "kelurahan_id",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "keadaan_pendidikan_id_foreign"),
            nullable = false)
    @JsonDeserialize(as = Kelurahan.class)
    @JsonSerialize(as = Kelurahan.class)
    private Kelurahan kelurahan;

    @NotEmpty(message = "Nama Lengkap is required")
    @NotNull(message = "Nama Lengkap can not be null!!")
    @Column(nullable = false, name = "nama_lengkap")
    private String namaLengkap;

    @Column(name = "no_hp1")
    private String noHp1;

    @Column(name = "no_hp2")
    private String noHp2;

    @Column(name = "email")
    private String email;

    @Column(name = "jenis_kelamin", columnDefinition = "ENUM('M','S')", nullable = false)
    @Enumerated(EnumType.STRING)
    private JenisKelamin jenisKelamin;

    @Column(name = "tempat_lahir")
    private String tempatLahir;

    @Column(name = "tanggal_lahir")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private LocalDate tanggalLahir;

    @Column(name = "status_marital", nullable = false, columnDefinition = "ENUM('MENIKAH', 'BELUM_MENIKAH', 'DUDA', 'JANDA')")
    @Enumerated(EnumType.STRING)
    private StatusMarital statusMarital;

    @Column(name = "pendidikan_terakhir", nullable = false, columnDefinition = """
            ENUM('SD', 'MI', 'SMP', 'MTS', 'SMA',\
             'SMK', 'MA', 'MLN', 'D1', 'D2', 'D3', 'D4', 'S1', 'S2', 'S3')\
            """)
    @Enumerated(EnumType.STRING)
    private PendidikanTerakhir pendidikanTerakhir;

    @Column(name = "pendidikan_luar_sekolah")
    private String pendidikanLuarSekolah;

    @Column(name = "pekerjaan_pokok")
    private String pekerjaanPokok;

    @Column(name = "pekerjaan_sambilan")
    private String pekerjaanSambilan;

    @Column(name = "anggota")
    private Boolean anggota;

    @Column(name = "otonom", columnDefinition = "ENUM('PERSIS','PERSISTRI', 'PEMUDA', 'PEMUDI')", nullable = false)
    @Enumerated(EnumType.STRING)
    private Otonom otonom;

    @Column(name = "status_dalam_jamiyyah", nullable = false, columnDefinition = """
            ENUM('ANGGOTA_BIASA', 'TASYKIL', \
            'BUKAN_ANGGOTA')\
            """)
    @Enumerated(EnumType.STRING)
    private StatusJamiyyah statusDalamJamiyyah;

    @Column(name = "upaya", nullable = false, columnDefinition = "ENUM('KITAB', 'BUKU', 'HALAQOH', 'LAINNYA')")
    @Enumerated(EnumType.STRING)
    private Upaya upaya;

    @Column(name = "upaya_additional_message", columnDefinition = "LONGTEXT")
    private String upayaAdditionalMessage;

    @Lob
    @Column(name = "internal", columnDefinition = "LONGTEXT")
    private String internal;

    @Column(name = "internal_additional_message", columnDefinition = "LONGTEXT")
    private String internalAdditionalMessage;

    @Lob
    @Column(name = "eksternal", columnDefinition = "LONGTEXT")
    private String eksternal;

    @Column(name = "eksternal_additional_message", columnDefinition = "LONGTEXT")
    private String eksternalAdditionalMessage;

    @Column(name = "pembinaan_internal", columnDefinition = "LONGTEXT")
    @Lob
    private String pembinaanInternal;

    @Column(name = "pembinaan_internal_additional_message", columnDefinition = "LONGTEXT")
    private String pembinaanInternalAdditionalMessage;

    @Lob
    @Column(name = "pembinaan_eksternal", columnDefinition = "LONGTEXT")
    private String pembinaanEksternal;

    @Column(name = "pembinaan_eksternal_additional_message", columnDefinition = "LONGTEXT")
    private String pembinaanEksternalAdditionalMessage;

    @Column(name = "pembinaan_pc", columnDefinition = "LONGTEXT")
    @Lob
    private String pembinaanPC;

    @Column(name = "pembinaan_pc_additional_message", columnDefinition = "LONGTEXT")
    private String pembinaanPCAdditionalMessage;

    @Column(name = "pembinaan_pd", columnDefinition = "LONGTEXT")
    @Lob
    private String pembinaanPD;

    @Column(name = "pembinaan_pd_additional_message", columnDefinition = "LONGTEXT")
    private String pembinaanPDAdditionalMessage;

    @Column(name = "lama_aktif_bertabligh")
    private Integer lamaAktifBertabligh;

    @Column(name = "frequensi_bertabligh")
    private Integer frequensiBertabligh;

    @Column(name = "selain_muballigh_pc")
    private String selainMuballighPc;

    @Column(name = "selainMuballighPc_additional_message", columnDefinition = "LONGTEXT")
    private String selainMuballighPcAdditionalMessage;

    @Column(name = "diundang_pc_lain")
    private String diundangPcLain;

    @Column(name = "diundang_pc_lain_additional_message", columnDefinition = "LONGTEXT")
    private String diundangPcLainAdditionalMessage;

    @Column(name = "diundang_pd_lain")
    private String diundangPdLain;

    @Column(name = "diundang_pd_lain_additional_message", columnDefinition = "LONGTEXT")
    private String diundangPdLainAdditionalMessage;

    @Column(name = "bentuk_tabligh")
    private String bentukTabligh;

    @Column(name = "bentuk_tabligh_additional_message", columnDefinition = "LONGTEXT")
    private String bentukTablighAdditionalMessage;

    @Column(name = "metode_tabligh")
    private String metodeTabligh;

    @Column(name = "metode_tabligh_additional_message", columnDefinition = "LONGTEXT")
    private String metodeTablighAdditionalMessage;

    @Column(name = "tema_tabligh")
    private String temaTabligh;

    @Column(name = "tema_tabligh_additional_message", columnDefinition = "LONGTEXT")
    private String temaTablighAdditionalMessage;

    @Lob
    @Column(name = "kendala", columnDefinition = "LONGTEXT")
    private String kendala;

    @CreatedDate
    @Column(name = "created_at", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime createdAt;

    @LastModifiedDate
    @Column(name = "updated_at", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    private LocalDateTime updatedAt;

    @Column(name = "deleted_at")
    private LocalDateTime deletedAt;

    @Column(name = "deleted", nullable = false)
    private boolean deleted = Boolean.FALSE;
}

