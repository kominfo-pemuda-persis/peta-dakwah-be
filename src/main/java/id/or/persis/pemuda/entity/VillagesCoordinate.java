package id.or.persis.pemuda.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;

/**
 * Created by IntelliJ IDEA.
 * Project : peta-dakwah-be
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/5/22
 * Time: 17:25
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Table(name = "villages_coordinate")
@SQLDelete(sql = "UPDATE villages_coordinate SET deleted = 1, deleted_at = CURRENT_TIMESTAMP WHERE kode = ?")
public class VillagesCoordinate {
    @Id
    @Column(name = "kode", unique = true, length = 13)
    private String kode;

    @Column(name = "nama", length = 100)
    private String nama;

    @Column(name = "ibukota", length = 100)
    private String ibukota;

    @Column(name = "lat")
    private Double latitude;

    @Column(name = "lng")
    private Double longitude;

    @Column(name = "elv")
    private Float elevation;

    @Column(name = "tz", columnDefinition = "TINYINT(4)")
//    @Type(type = "org.hibernate.type.IntegerType")
    private Integer timeZone;

    @Column(name = "luas")
    private Double luas;

    @Column(name = "penduduk")
    private Double penduduk;

    @Lob
    @Column(name = "path", columnDefinition = "LONGTEXT")
    private String path;

    @Column(name = "status", columnDefinition = "TINYINT(4)")
//    @Type(type = "org.hibernate.type.IntegerType")
    private Integer status;

    @Column(name = "deleted_at")
    private LocalDateTime deletedAt;

    @Column(name = "deleted", nullable = false)
    @Builder.Default
    private boolean deleted = Boolean.FALSE;
}
