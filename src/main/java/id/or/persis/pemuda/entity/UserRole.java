package id.or.persis.pemuda.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "user_roles")
public class UserRole implements Serializable {

    @Serial
    private static final long serialVersionUID = -3505713221459127151L;

    @Id
    @Column(name = "user_id")
    private String userId;

    @Column(name = "roles_id")
    private String roleId;

}
