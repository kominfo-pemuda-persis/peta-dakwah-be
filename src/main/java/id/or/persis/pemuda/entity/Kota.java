package id.or.persis.pemuda.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.persistence.*;
import lombok.Data;

import java.io.Serializable;

@Entity
@Table(name = "kota")
@Data
public class Kota implements Serializable {
    @Id
    @Column(name = "id", nullable = false, length = 36)
    private String id;

//    @ManyToOne(
//            fetch = FetchType.EAGER, cascade = {CascadeType.ALL},  optional = false
//    )
//    @JoinColumn(name = "id_provinsi", nullable = false)
//    @JsonIgnore
//    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(
            name = "id_provinsi",
            referencedColumnName = "kode",
            foreignKey = @ForeignKey(name = "provinsi_kd_fk"),
            nullable = false)
    @JsonDeserialize(as = Provinsi.class)
    @JsonSerialize(as = Provinsi.class)
    private Provinsi provinsi;

    @Column(name = "kode", nullable = false, length = 50)
    private String kode;

    @Column(name = "nama", nullable = false)
    private String nama;
}
