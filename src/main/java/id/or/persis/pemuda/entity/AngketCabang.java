package id.or.persis.pemuda.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "angket_cabang")
@SQLDelete(sql = "UPDATE angket_cabang SET deleted = 1, deleted_at = CURRENT_TIMESTAMP WHERE id = ?")
public class AngketCabang extends BaseEntity {

    @OneToOne
    @JoinColumn(
            name = "kode_pc",
            referencedColumnName = "kd_pc",
            foreignKey = @ForeignKey(name = "angket_cabang_kd_pc_foreign"),
            nullable = false)
    @JsonDeserialize(as = PimpinanCabang.class)
    @JsonSerialize(as = PimpinanCabang.class)
    private PimpinanCabang pc;

    @ManyToOne
    @JoinColumn(
            name = "kecamatan_id",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "angket_cabang_kecamatan_id_foreign"),
            nullable = false)
    @JsonDeserialize(as = Kecamatan.class)
    @JsonSerialize(as = Kecamatan.class)
    private Kecamatan kecamatan;

    @Column(name = "tahun_berdiri")
    private Integer tahunBerdiri;

    @Column(name = "alamat")
    private String alamat;

    @Column(name = "nama_bidang_dakwah", length = 100)
    private String namaBidangDakwah;

    @Column(name = "no_hp1", length = 15)
    private String noHp1;

    @Column(name = "no_hp2", length = 15)
    private String noHp2;

    @Column(name = "email", length = 100)
    private String email;

    @Column(name = "q1", columnDefinition = "TEXT")
    private String q1;

    @Column(name = "q1_additional_message", columnDefinition = "TEXT")
    private String q1AdditionalMessage;

    @Column(name = "q2", columnDefinition = "TEXT")
    private String q2;

    @Column(name = "q2_additional_message", columnDefinition = "TEXT")
    private String q2AdditionalMessage;

    @Column(name = "q3", columnDefinition = "TEXT")
    private String q3;

    @Column(name = "q4", columnDefinition = "TEXT")
    private String q4;

    @Column(name = "q5", columnDefinition = "TEXT")
    private String q5;

    @Column(name = "q5_additional_message", columnDefinition = "TEXT")
    private String q5AdditionalMessage;

    @Column(name = "q6", columnDefinition = "TEXT")
    private String q6;

    @Column(name = "q6_additional_message", columnDefinition = "TEXT")
    private String q6AdditionalMessage;

    @Column(name = "q7", columnDefinition = "TEXT")
    private String q7;

    @Column(name = "q8", length = 50)
    private String q8;

    @Column(name = "q8_additional_message", columnDefinition = "TEXT")
    private String q8AdditionalMessage;

    @Column(name = "q9", columnDefinition = "TEXT")
    private String q9;

    @Column(name = "q9_additional_message", columnDefinition = "TEXT")
    private String q9AdditionalMessage;

    @Column(name = "q10", columnDefinition = "TEXT")
    private String q10;

    @Column(name = "q11", columnDefinition = "TEXT")
    private String q11;

    @Column(name = "q12", columnDefinition = "TEXT")
    private String q12;

    @Column(name = "q12_additional_message", columnDefinition = "TEXT")
    private String q12AdditionalMessage;

    @Column(name = "q13", columnDefinition = "TEXT")
    private String q13;

    @Column(name = "q13_additional_message", columnDefinition = "TEXT")
    private String q13AdditionalMessage;

    @Column(name = "q14", length = 25)
    private String q14;

    @Column(name = "q14_additional_message", columnDefinition = "TEXT")
    private String q14AdditionalMessage;
}
