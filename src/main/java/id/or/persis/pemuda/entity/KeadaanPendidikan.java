package id.or.persis.pemuda.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "keadaan_pendidikan")
@SQLDelete(sql = "UPDATE keadaan_pendidikan SET deleted = 1, deleted_at = CURRENT_TIMESTAMP WHERE id = ?")
public class KeadaanPendidikan extends BaseEntity {

    @OneToOne
    @JoinColumn(
            name = "kode_pc",
            referencedColumnName = "kd_pc",
            foreignKey = @ForeignKey(name = "keadaan_pendidikan_kd_pc_foreign"),
            nullable = false)
    @JsonDeserialize(as = PimpinanCabang.class)
    @JsonSerialize(as = PimpinanCabang.class)
    private PimpinanCabang pc;

    @OneToOne
    @JoinColumn(
            name = "kelurahan_id",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "keadaan_pendidikan_id_foreign"),
            nullable = false)
    @JsonDeserialize(as = Kelurahan.class)
    @JsonSerialize(as = Kelurahan.class)
    private Kelurahan kelurahan;

    @Column(name = "jml_ra", nullable = false)
    private Integer jumlahRa;

    @Column(name = "jml_diniyyah", nullable = false)
    private Integer jumlahDny;

    @Column(name = "jml_mi", nullable = false)
    private Integer jumlahMi;

    @Column(name = "jml_sd", nullable = false)
    private Integer jumlahSd;

    @Column(name = "jml_mts", nullable = false)
    private Integer jumlahMts;

    @Column(name = "jml_smp", nullable = false)
    private Integer jumlahSmp;

    @Column(name = "jml_ma", nullable = false)
    private Integer jumlahMa;

    @Column(name = "jml_muallimin", nullable = false)
    private Integer jumlahMln;

    @Column(name = "jml_sma", nullable = false)
    private Integer jumlahSma;

    @Column(name = "total", nullable = true)
    private Integer total;

    @Column(name = "jml_santri_ra", nullable = false)
    private Integer jumlahSantriRa;

    @Column(name = "jml_santri_diniyyah", nullable = false)
    private Integer jumlahSantriDny;

    @Column(name = "jml_santri_mi", nullable = false)
    private Integer jumlahSantriMi;

    @Column(name = "jml_santri_sd", nullable = false)
    private Integer jumlahSantriSd;

    @Column(name = "jml_santri_mts", nullable = false)
    private Integer jumlahSantriMts;

    @Column(name = "jml_santri_smp", nullable = false)
    private Integer jumlahSantriSmp;

    @Column(name = "jml_santri_ma", nullable = false)
    private Integer jumlahSantriMa;

    @Column(name = "jml_santri_muallimin", nullable = false)
    private Integer jumlahSantriMln;

    @Column(name = "jml_santri_sma", nullable = false)
    private Integer jumlahSantriSma;

    @Column(name = "periode_start", nullable = true)
    private LocalDate periodeStart;

    @Column(name = "periode_end", nullable = true)
    private LocalDate periodeEnd;
}
