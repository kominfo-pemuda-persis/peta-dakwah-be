package id.or.persis.pemuda.exception;

public class KeadaanJamaahNotFoundException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    private String resourceName;

    private String fieldName;

    private Object fieldValue;

    public KeadaanJamaahNotFoundException(String resourceName, String fieldName, Object fieldValue) {
        super("%s with %S = %s is Not Found!".formatted(resourceName, fieldName, fieldValue));
        this.resourceName = resourceName;
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }
}
