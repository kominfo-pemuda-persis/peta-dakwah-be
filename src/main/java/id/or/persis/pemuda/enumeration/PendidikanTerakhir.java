package id.or.persis.pemuda.enumeration;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Created by IntelliJ IDEA.
 * Project : peta-dakwah-be
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/18/22
 * Time: 15:49
 * To change this template use File | Settings | File Templates.
 */
public enum PendidikanTerakhir {
    SD, MI, SMP, MTS, SMA, SMK, MA, MLN, D1, D2, D3, D4, S1, S2, S3;

    @JsonCreator
    public static PendidikanTerakhir create(String value) {
        return PendidikanTerakhir.valueOf(value.toUpperCase());
    }
}
