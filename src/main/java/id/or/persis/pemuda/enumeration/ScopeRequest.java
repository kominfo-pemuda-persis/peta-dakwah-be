package id.or.persis.pemuda.enumeration;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum ScopeRequest {
    SPECIFIC, ALL;

    @JsonCreator
    public static ScopeRequest create(String value) {
        return ScopeRequest.valueOf(value.toUpperCase());
    }

    public boolean isAll() {
        return ALL == this;
    }
}
