package id.or.persis.pemuda.enumeration;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Created by IntelliJ IDEA.
 * Project : peta-dakwah-be
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/18/22
 * Time: 15:49
 * To change this template use File | Settings | File Templates.
 */
public enum JenisKelamin {
    L, P;

    @JsonCreator
    public static JenisKelamin create(String value) {
        return JenisKelamin.valueOf(value.toUpperCase());
    }
}
