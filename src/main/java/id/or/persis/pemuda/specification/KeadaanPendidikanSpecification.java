package id.or.persis.pemuda.specification;

import java.text.MessageFormat;
import java.util.UUID;

import id.or.persis.pemuda.entity.KeadaanPendidikan;
import lombok.experimental.UtilityClass;
import org.springframework.data.jpa.domain.Specification;

@UtilityClass
public class KeadaanPendidikanSpecification {

    public static Specification<KeadaanPendidikan> isDeleted(boolean isDeleted) {
        return (rootQuery, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(rootQuery.get("deleted"), isDeleted);
    }

    public static Specification<KeadaanPendidikan> hasId(UUID id) {
        return (rootQuery, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(rootQuery.get("id"), id);
    }

    public static Specification<KeadaanPendidikan> hasKodePc(String keyword) {
        return (pc, cq, cb) -> cb.like(pc.join("pc").get("kdPc"),
            MessageFormat.format(SpecificationConstant.CONTAINS_WILDCARD_SQL, keyword));
    }

    public static Specification<KeadaanPendidikan> hasNamaPc(String keyword) {
        return (pc, cq, cb) -> cb.like(pc.join("pc").get("namaPc"),
            MessageFormat.format(SpecificationConstant.CONTAINS_WILDCARD_SQL, keyword));
    }

    public static Specification<KeadaanPendidikan> hasKodeKelurahan(String keyword) {
        return (pc, cq, cb) -> cb.like(pc.join("kelurahan").get("kode"),
            MessageFormat.format(SpecificationConstant.CONTAINS_WILDCARD_SQL, keyword));
    }

    public static Specification<KeadaanPendidikan> hasNamaKelurahan(String keyword) {
        return (pc, cq, cb) -> cb.like(pc.join("kelurahan").get("nama"),
            MessageFormat.format(SpecificationConstant.CONTAINS_WILDCARD_SQL, keyword));
    }

}
