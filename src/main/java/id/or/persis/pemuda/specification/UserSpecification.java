package id.or.persis.pemuda.specification;

import id.or.persis.pemuda.entity.User;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

@Value
public class UserSpecification implements Specification<User> {

    @Serial
    private static final long serialVersionUID = 5152353325983770310L;

    String npa;
    String email;
    String username;
    String kodePc;
    String kodePd;
    String kodePw;
    String namaLengkap;

    private static Predicate notDeleted(Root<User> root, CriteriaBuilder cb) {
        return cb.isNull(root.get("deletedAt"));
    }

    private Predicate equalNpa(Root<User> root, CriteriaBuilder cb) {
        return cb.equal(root.get("npa"), npa);
    }

    private Predicate equalEmail(Root<User> root, CriteriaBuilder cb) {
        return cb.equal(root.get("email"), email);
    }

    private Predicate equalUsername(Root<User> root, CriteriaBuilder cb) {
        return cb.equal(root.get("username"), username);
    }

    private Predicate equalPc(Root<User> root, CriteriaBuilder cb) {
        return cb.equal(root.get("pc").get("kdPc"), kodePc);
    }

    private Predicate equalPd(Root<User> root, CriteriaBuilder cb) {
        return cb.equal(root.get("pc").get("pd").get("kdPd"), kodePd);
    }

    private Predicate equalPw(Root<User> root, CriteriaBuilder cb) {
        return cb.equal(root.get("pc")
            .get("pd")
            .get("pw")
            .get("kdPw"), kodePw);
    }

    private Predicate equalNamaLengkap(Root<User> root, CriteriaBuilder cb) {
        return cb.equal(root.get("namaLengkap"), namaLengkap);
    }

    @Override
    public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(notDeleted(root, criteriaBuilder));

        if (StringUtils.isNotBlank(npa)) {
            predicates.add(equalNpa(root, criteriaBuilder));
        }

        if (StringUtils.isNotBlank(email)) {
            predicates.add(equalEmail(root, criteriaBuilder));
        }

        if (StringUtils.isNotBlank(username)) {
            predicates.add(equalUsername(root, criteriaBuilder));
        }

        if (StringUtils.isNotBlank(kodePc)) {
            predicates.add(equalPc(root, criteriaBuilder));
        }

        if (StringUtils.isNotBlank(kodePd)) {
            predicates.add(equalPd(root, criteriaBuilder));
        }

        if (StringUtils.isNotBlank(kodePw)) {
            predicates.add(equalPw(root, criteriaBuilder));
        }

        if (StringUtils.isNotBlank(namaLengkap)) {
            predicates.add(equalNamaLengkap(root, criteriaBuilder));
        }

        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }
}
