package id.or.persis.pemuda.specification;

final class SpecificationConstant {

    static final String CONTAINS_WILDCARD_SQL = "%{0}%";

    private SpecificationConstant() {

    }
}
