package id.or.persis.pemuda.specification;

import id.or.persis.pemuda.entity.AngketCabang;
import lombok.experimental.UtilityClass;
import org.springframework.data.jpa.domain.Specification;

import java.util.UUID;

@UtilityClass
public class AngketCabangSpecification {

    public static Specification<AngketCabang> isDeleted(boolean isDeleted) {
        return (rootQuery, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(rootQuery.get("deleted"), isDeleted);
    }

    public static Specification<AngketCabang> hasId(UUID id) {
        return (rootQuery, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(rootQuery.get("id"), id);
    }
}