package id.or.persis.pemuda.specification;

import id.or.persis.pemuda.entity.Kecamatan;
import id.or.persis.pemuda.entity.Kelurahan;
import id.or.persis.pemuda.entity.Kota;
import id.or.persis.pemuda.entity.Provinsi;
import org.springframework.data.jpa.domain.Specification;

import java.text.MessageFormat;

public class WilayahSpecification {
    private static final String FORMAT = "%{0}%";

    private WilayahSpecification() {
    }

    public static Specification<Provinsi> hasKodeProvinsi(String kodeProvinsi) {
        return (pc, cq, cb) -> cb.like(
                pc.get("kode"), MessageFormat.format(FORMAT, kodeProvinsi));
    }

    public static Specification<Provinsi> hasNamaProvinsi(String namaProvinsi) {
        return (pc, cq, cb) -> cb.like(
                pc.get("nama"), MessageFormat.format(FORMAT, namaProvinsi));
    }

    public static Specification<Kota> hasProvince(String idProvince) {
        return (pc, cq, cb) -> cb.like(
            pc.get("kode"), MessageFormat.format("{0}%", idProvince));
    }

    public static Specification<Kota> hasKodeKota(String kodeKota) {
        return (pc, cq, cb) -> cb.like(
                pc.get("kode"), MessageFormat.format(FORMAT, kodeKota));
    }

    public static Specification<Kota> hasNamaKota(String namaKota) {
        return (pc, cq, cb) -> cb.like(
                pc.get("nama"), MessageFormat.format(FORMAT, namaKota));
    }

    public static Specification<Kecamatan> hasKota(String idKota) {
        return (pc, cq, cb) -> cb.like(
            pc.get("kode"), MessageFormat.format("{0}%", idKota));
    }

    public static Specification<Kecamatan> hasKodeKecamatan(String kodeKecamatan) {
        return (pc, cq, cb) -> cb.like(
                pc.get("kode"), MessageFormat.format(FORMAT, kodeKecamatan));
    }

    public static Specification<Kecamatan> hasNamaKecamatan(String namaKecamatan) {
        return (pc, cq, cb) -> cb.like(
                pc.get("nama"), MessageFormat.format(FORMAT, namaKecamatan));
    }

    public static Specification<Kelurahan> hasKecamatan(String idKecamatan) {
        return (pc, cq, cb) -> cb.like(
            pc.get("kode"), MessageFormat.format("{0}%", idKecamatan));
    }

    public static Specification<Kelurahan> hasKodeKelurahan(String kodeKelurahan) {
        return (pc, cq, cb) -> cb.like(
                pc.get("kode"), MessageFormat.format(FORMAT, kodeKelurahan));
    }

    public static Specification<Kelurahan> hasNamaKelurahan(String namaKelurahan) {
        return (pc, cq, cb) -> cb.like(
                pc.get("nama"), MessageFormat.format(FORMAT, namaKelurahan));
    }
    
}
