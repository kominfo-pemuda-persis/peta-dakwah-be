package id.or.persis.pemuda.specification;

import java.text.MessageFormat;
import java.util.UUID;

import id.or.persis.pemuda.entity.KeadaanJamaah;
import org.springframework.data.jpa.domain.Specification;

public class KeadaanJamaahSpecification {

    private KeadaanJamaahSpecification() {
    }

    public static Specification<KeadaanJamaah> isDeleted(Boolean isDeleted) {
        return (rootQuery, criteriaQuery, criteriaBuilder) ->
            criteriaBuilder.equal(rootQuery.get("deleted"), isDeleted);
    }

    public static Specification<KeadaanJamaah> hasId(UUID id) {
        return (rootQuery, criteriaQuery, criteriaBuilder) ->
            criteriaBuilder.equal(rootQuery.get("id"), id);
    }

    public static Specification<KeadaanJamaah> hasKodePc(String keyword) {
        return (pc, cq, cb) -> cb.like(pc.join("pc").get("kdPc"),
            MessageFormat.format(SpecificationConstant.CONTAINS_WILDCARD_SQL, keyword));
    }

    public static Specification<KeadaanJamaah> hasNamaPc(String keyword) {
        return (pc, cq, cb) -> cb.like(pc.join("pc").get("namaPc"),
            MessageFormat.format(SpecificationConstant.CONTAINS_WILDCARD_SQL, keyword));
    }

    public static Specification<KeadaanJamaah> hasKodeKelurahan(String keyword) {
        return (pc, cq, cb) -> cb.like(pc.join("kelurahan").get("kode"),
            MessageFormat.format(SpecificationConstant.CONTAINS_WILDCARD_SQL, keyword));
    }

    public static Specification<KeadaanJamaah> hasNamaKelurahan(String keyword) {
        return (pc, cq, cb) -> cb.like(pc.join("kelurahan").get("nama"),
            MessageFormat.format(SpecificationConstant.CONTAINS_WILDCARD_SQL, keyword));
    }
}
