package id.or.persis.pemuda.specification;

import id.or.persis.pemuda.entity.CitiesCoordinate;
import org.springframework.data.jpa.domain.Specification;

import java.text.MessageFormat;

public class CitiesCoordinateSpecification {

    private static String pattern = "%{0}%";
    
    private CitiesCoordinateSpecification() {
    }
    
    public static Specification<CitiesCoordinate> hasKode(String kode) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("kode"), kode);
    }

    public static Specification<CitiesCoordinate> hasKodeLike(String kode) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.like(
            root.get("kode"), MessageFormat.format(pattern, kode));
    }

    public static Specification<CitiesCoordinate> hasNama(String nama) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.like(root.get("nama"),
                MessageFormat.format(pattern, nama));
    }

    public static Specification<CitiesCoordinate> hasIbuKota(String ibuKota) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.like(root.get("ibukota"),
                MessageFormat.format(pattern, ibuKota));
    }
}