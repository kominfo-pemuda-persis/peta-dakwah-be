package id.or.persis.pemuda.specification;

import java.text.MessageFormat;
import java.util.UUID;

import id.or.persis.pemuda.entity.ProfilMuballigh;
import org.springframework.data.jpa.domain.Specification;

/**
 * ProfilMuballighSpecification
 */
public class ProfilMuballighSpecification {

    ProfilMuballighSpecification(){

    }

    public static Specification<ProfilMuballigh> isDeleted(Boolean isDeleted) {
        return (rootQuery, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(rootQuery.get("deleted"), isDeleted);
    }

    public static Specification<ProfilMuballigh> hasId(UUID id) {
        return (rootQuery, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(rootQuery.get("id"), id);
    }

    public static Specification<ProfilMuballigh> hasKodePc(String keyword) {
        return (pc, cq, cb) -> cb.like(pc.join("pc").get("kdPc"),
                MessageFormat.format(SpecificationConstant.CONTAINS_WILDCARD_SQL, keyword));
    }

    public static Specification<ProfilMuballigh> hasNamaPc(String keyword) {
        return (pc, cq, cb) -> cb.like(pc.join("pc").get("namaPc"),
                MessageFormat.format(SpecificationConstant.CONTAINS_WILDCARD_SQL, keyword));
    }

    public static Specification<ProfilMuballigh> hasKodeKelurahan(String keyword) {
        return (pc, cq, cb) -> cb.like(pc.join("kelurahan").get("kode"),
                MessageFormat.format(SpecificationConstant.CONTAINS_WILDCARD_SQL, keyword));
    }

    public static Specification<ProfilMuballigh> hasNamaKelurahan(String keyword) {
        return (pc, cq, cb) -> cb.like(pc.join("kelurahan").get("nama"),
                MessageFormat.format(SpecificationConstant.CONTAINS_WILDCARD_SQL, keyword));
    }

    public static Specification<ProfilMuballigh> hasNamaLengkap(String keyword) {
        return (pc, cq, cb) -> cb.like(pc.get("namaLengkap"),
                MessageFormat.format(SpecificationConstant.CONTAINS_WILDCARD_SQL, keyword));
    }

    public static Specification<ProfilMuballigh> hasEmail(String keyword) {
        return (pc, cq, cb) -> cb.like(pc.get("email"),
                MessageFormat.format(SpecificationConstant.CONTAINS_WILDCARD_SQL, keyword));
    }
}
