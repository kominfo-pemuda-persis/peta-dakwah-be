package id.or.persis.pemuda.specification;

import id.or.persis.pemuda.entity.PimpinanDaerah;
import org.springframework.data.jpa.domain.Specification;

import java.text.MessageFormat;

public class PimpinanDaerahSpecification {
    private PimpinanDaerahSpecification() {
    }
    
    public static Specification<PimpinanDaerah> hasKodePW(String kodePW) {
        return (pw, cq, cb) -> cb.equal(pw.join("pw").get("kdPw"), kodePW);

    }
    
    public static Specification<PimpinanDaerah> hasKodePd(String kodePd) {
        return (pw, cq, cb) -> cb.like(pw.get("kdPd"), MessageFormat.format("%{0}%", kodePd));
    }
    
    public static Specification<PimpinanDaerah> hasNamaPd(String namaPd) {
        return (pw, cq, cb) -> cb.like(pw.get("namaPd"), MessageFormat.format("%{0}%", namaPd));
    }
}