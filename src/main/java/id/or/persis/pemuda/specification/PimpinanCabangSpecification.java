package id.or.persis.pemuda.specification;

import id.or.persis.pemuda.entity.PimpinanCabang;
import org.springframework.data.jpa.domain.Specification;

import java.text.MessageFormat;

public class PimpinanCabangSpecification {
    
    private PimpinanCabangSpecification() {
    }
    
    public static Specification<PimpinanCabang> hasKodePd(String kodePd) {
        return (pc, cq, cb) -> cb.equal(pc.join("pd").get("kdPd"), kodePd);
    }
    
    public static Specification<PimpinanCabang> hasKodePc(String kodePc) {
        return (pc, cq, cb) -> cb.like(pc.get("kdPc"), MessageFormat.format("%{0}%", kodePc));
    }
    
    public static Specification<PimpinanCabang> hasNamaPc(String namaPc) {
        return (pc, cq, cb) -> cb.like(pc.get("namaPc"), MessageFormat.format("%{0}%", namaPc));
    }
}