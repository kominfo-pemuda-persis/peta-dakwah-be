package id.or.persis.pemuda.specification;

import id.or.persis.pemuda.entity.PimpinanWilayah;
import org.springframework.data.jpa.domain.Specification;

import java.text.MessageFormat;

public class PimpinanWilayahSpecification {
    private PimpinanWilayahSpecification() {
    }

    public static Specification<PimpinanWilayah> hasKodePw(String kodePw) {
        return (pw, cq, cb) -> cb.like(
            pw.get("kdPw"), MessageFormat.format("%{0}%", kodePw));
    }

    public static Specification<PimpinanWilayah> hasNamaPw(String namaPw) {
        return (pw, cq, cb) -> cb.like(
            pw.get("namaPw"), MessageFormat.format("%{0}%", namaPw));
    }
}