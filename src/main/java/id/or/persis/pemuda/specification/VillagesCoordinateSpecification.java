package id.or.persis.pemuda.specification;

import java.text.MessageFormat;

import id.or.persis.pemuda.entity.VillagesCoordinate;
import org.springframework.data.jpa.domain.Specification;

/**
 * Created by IntelliJ IDEA.
 * Project : peta-dakwah-be
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/5/22
 * Time: 17:42
 * To change this template use File | Settings | File Templates.
 */
public class VillagesCoordinateSpecification {

    private VillagesCoordinateSpecification() {
    }

    public static Specification<VillagesCoordinate> hasKode(String kode) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("kode"), kode);
    }

    public static Specification<VillagesCoordinate> hasNama(String nama) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.like(root.get("nama"),
                MessageFormat.format(SpecificationConstant.CONTAINS_WILDCARD_SQL, nama));
    }

    public static Specification<VillagesCoordinate> hasIbuKota(String ibuKota) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.like(root.get("ibukota"),
                MessageFormat.format(SpecificationConstant.CONTAINS_WILDCARD_SQL, ibuKota));
    }

    public static Specification<VillagesCoordinate> isDeleted(boolean isDeleted) {
        return (rootQuery, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(rootQuery.get("deleted"), isDeleted);
    }
}
