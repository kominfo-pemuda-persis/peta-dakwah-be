package id.or.persis.pemuda.service;

import java.util.UUID;

public record GetDashboardSummaryRequest(UUID userId) {
}
