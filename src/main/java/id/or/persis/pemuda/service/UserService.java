package id.or.persis.pemuda.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

import id.or.persis.pemuda.dto.GetUserDTO;
import id.or.persis.pemuda.dto.UserDTO;
import id.or.persis.pemuda.entity.Role;
import id.or.persis.pemuda.entity.User;
import id.or.persis.pemuda.mapper.UserMapper;
import id.or.persis.pemuda.repository.RoleRepository;
import id.or.persis.pemuda.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

@Service
@RequiredArgsConstructor
public class UserService {
    private static final String USER_NOT_FOUND = "User not found";
    private final UserRepository userRepository;

    private final UserMapper userMapper;

    private final RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Transactional(readOnly = true)
    public Page<GetUserDTO> getAllUser(GetAllUsersRequest request) {
        return userRepository.findAll(request.toSpecification(), request.pageable())
            .map(userMapper::toDto);
    }

    @Transactional(readOnly = true)
    public GetUserDTO findById(UUID id) {
        return userMapper.toDto(userRepository.findByIdAndDeletedAtIsNull(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, USER_NOT_FOUND)));
    }

    @Transactional
    public GetUserDTO save(UserDTO userDTO) {
        userDTO.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        GetUserDTO getUserDTO = userMapper.toDto(userRepository.save(userMapper.toEntity(userDTO)));
        Optional<User> optUsr = userRepository.findByUsername(getUserDTO.getUsername());
        User usr = optUsr.get();
        Collection<Role> roles = new ArrayList<>();
        userDTO.getRoleNames().stream().forEach(r -> {
            Optional<Role> role = roleRepository.findByRoleName(r);
            roles.add(role.get());
        });

        usr.setRoles(roles);
        userRepository.save(usr);
        return userMapper.toDto(usr);
    }

    @Transactional
    public GetUserDTO update(UserDTO newUser, UUID id) {
        newUser.setPassword(passwordEncoder.encode(newUser.getPassword()));
        return userRepository.findByIdAndDeletedAtIsNull(id)
            .map(user -> userMapper.toEntity(newUser).withId(id))
            .map(userRepository::save)
            .map(userMapper::toDto)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, USER_NOT_FOUND));
    }

    @Transactional
    public String deleteById(UUID id) {
        return userRepository.findByIdAndDeletedAtIsNull(id)
                .map(User::deleted)
                .map(userRepository::save)
                .map(User::getId)
                .map(UUID::toString)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, USER_NOT_FOUND));
    }
}
