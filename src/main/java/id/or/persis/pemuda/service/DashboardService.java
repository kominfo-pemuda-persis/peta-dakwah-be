package id.or.persis.pemuda.service;

public interface DashboardService {

    GetDashboardSummaryDto getSummary(GetDashboardSummaryRequest request);
}
