package id.or.persis.pemuda.service;

import id.or.persis.pemuda.repository.AngketCabangRepository;
import id.or.persis.pemuda.repository.KeadaanJamaahRepository;
import id.or.persis.pemuda.repository.KeadaanPendidikanRepository;
import id.or.persis.pemuda.repository.ProfilMuballighRepository;
import id.or.persis.pemuda.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
class DashboardServiceImpl implements DashboardService {

    private final UserRepository userRepository;

    private final KeadaanJamaahRepository keadaanJamaahRepository;

    private final KeadaanPendidikanRepository keadaanPendidikanRepository;

    private final AngketCabangRepository angketCabangRepository;

    private final ProfilMuballighRepository profilMuballighRepository;

    @Override
    public GetDashboardSummaryDto getSummary(GetDashboardSummaryRequest request) {
        var kodePc = userRepository.findKdPcByUserId(request.userId())
            .orElse("");

        if (kodePc.isEmpty()) {
            return GetDashboardSummaryDto.empty();
        }

        // pc
        var keadaanJamaahPcCount = keadaanJamaahRepository.countByPcKdPc(kodePc);
        var keadaanPendidikanPcCount = keadaanPendidikanRepository.countByPcKdPc(kodePc);
        var angketCabangPcCount = angketCabangRepository.countByPcKdPc(kodePc);
        var profilMuballighPcCount = profilMuballighRepository.countByPcKdPc(kodePc);

        // pd
        var kodePd = userRepository.findKdPdByUserId(request.userId())
            .orElse("");
        var keadaanJamaahPdCount = keadaanJamaahRepository.countByKdPd(kodePd);
        var keadaanPendidikanPdCount = keadaanPendidikanRepository.countByKdPd(kodePd);
        var angketCabangPdCount = angketCabangRepository.countByKdPd(kodePd);
        var profilMuballighPdCount = profilMuballighRepository.countByKdPd(kodePd);

        // pw
        var kodePw = userRepository.findKdPwByUserId(request.userId())
            .orElse("");
        var keadaanJamaahPwCount = keadaanJamaahRepository.countByKdPw(kodePw);
        var keadaanPendidikanPwCount = keadaanPendidikanRepository.countByKdPw(kodePw);
        var angketCabangPwCount = angketCabangRepository.countByKdPw(kodePw);
        var profilMuballighPwCount = profilMuballighRepository.countByKdPw(kodePw);

        return GetDashboardSummaryDto.builder()
            .keadaanJamaahPc(keadaanJamaahPcCount)
            .keadaanPendidikanPc(keadaanPendidikanPcCount)
            .angketPc(angketCabangPcCount)
            .profilMuballighPc(profilMuballighPcCount)
            .keadaanJamaahPd(keadaanJamaahPdCount)
            .keadaanPendidikanPd(keadaanPendidikanPdCount)
            .angketPd(angketCabangPdCount)
            .profilMuballighPd(profilMuballighPdCount)
            .keadaanJamaahPw(keadaanJamaahPwCount)
            .keadaanPendidikanPw(keadaanPendidikanPwCount)
            .angketPw(angketCabangPwCount)
            .profilMuballighPw(profilMuballighPwCount)
            .build();
    }
}
