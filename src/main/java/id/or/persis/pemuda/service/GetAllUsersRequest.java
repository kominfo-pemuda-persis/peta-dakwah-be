package id.or.persis.pemuda.service;

import id.or.persis.pemuda.specification.UserSpecification;
import lombok.Builder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Pageable;

@Builder
public record GetAllUsersRequest(String npa, String email, String username, String kodePc, String kodePd, String kodePw, Pageable pageable,
                                 String namaLengkap) {
    public boolean hasNpa() {
        return StringUtils.isNotBlank(npa);
    }

    public boolean hasEmail() {
        return StringUtils.isNotBlank(email);
    }

    public boolean hasUsername() {
        return StringUtils.isNotBlank(username);
    }

    public boolean hasKodePc() {
        return StringUtils.isNotBlank(kodePc);
    }

    public boolean hasKodePd() {
        return StringUtils.isNotBlank(kodePd);
    }

    public boolean hasKodePw() {
        return StringUtils.isNotBlank(kodePw);
    }

    public boolean hasNamaLengkap() {
        return StringUtils.isNotBlank(namaLengkap);
    }

    UserSpecification toSpecification() {
        return new UserSpecification(
            npa, email, username, kodePc, kodePd, kodePw, namaLengkap
        );
    }
}
