package id.or.persis.pemuda.service;

import java.util.UUID;

import id.or.persis.pemuda.dto.KeadaanPendidikanDTO;
import id.or.persis.pemuda.entity.KeadaanPendidikan;
import id.or.persis.pemuda.enumeration.ScopeRequest;
import id.or.persis.pemuda.mapper.KeadaanPendidikanMapper;
import id.or.persis.pemuda.repository.KeadaanPendidikanRepository;
import id.or.persis.pemuda.specification.KeadaanPendidikanSpecification;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

@Service
@RequiredArgsConstructor
@Slf4j
public class KeadaanPendidikanService {
    private final KeadaanPendidikanMapper keadaanPendidikanMapper;

    private final KeadaanPendidikanRepository pendidikanRepo;

    @Transactional(propagation = Propagation.REQUIRED)
    public KeadaanPendidikanDTO saveKeadaanPendidikan(KeadaanPendidikanDTO keadaanPendidikanDTO) {
        log.info("Save new Keadaan Pendidikan ...");
        return getKeadaanPendidikanDTO(pendidikanRepo.save(getKeadaanPendidikan(keadaanPendidikanDTO)));
    }

    public Page<KeadaanPendidikanDTO> getAllKeadaanPendidikan(Specification<KeadaanPendidikan> specification, Pageable page) {
        return pendidikanRepo.findAll(specification, page).map(this::getKeadaanPendidikanDTO);
    }

    public KeadaanPendidikanDTO getKeadaanPendidikanById(UUID id) {
        KeadaanPendidikan keadaanPendidikan = pendidikanRepo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "DATA NOT FOUND"));
        return getKeadaanPendidikanDTO(keadaanPendidikan);
    }

    @Transactional
    public KeadaanPendidikanDTO updateKeadaanPendidikan(KeadaanPendidikanDTO keadaanPendidikanDTO, UUID id) {
        this.getKeadaanPendidikanById(id);
        return saveKeadaanPendidikan(keadaanPendidikanDTO);
    }

    @Transactional
    public KeadaanPendidikanDTO deleteKeadaanPendidikanById(UUID id) {
        KeadaanPendidikanDTO existingKeadaanPendidikan = this.getKeadaanPendidikanById(id);
        pendidikanRepo.delete(getKeadaanPendidikan(existingKeadaanPendidikan));
        return existingKeadaanPendidikan;
    }

    private KeadaanPendidikanDTO getKeadaanPendidikanDTO(KeadaanPendidikan keadaanPendidikan) {
        return keadaanPendidikanMapper.toDto(keadaanPendidikan);
    }

    private KeadaanPendidikan getKeadaanPendidikan(KeadaanPendidikanDTO keadaanPendidikanDTO) {
        return keadaanPendidikanMapper.toEntity(keadaanPendidikanDTO);
    }

    public Specification<KeadaanPendidikan> validateSpecification(boolean isDeleted, String scope) {
        Specification<KeadaanPendidikan> specification = KeadaanPendidikanSpecification.isDeleted(isDeleted);

        if (ScopeRequest.valueOf(scope).isAll() && isDeleted) {
            specification = null;
        }

        return specification;
    }

    public Specification<KeadaanPendidikan> addKeywordSpecification(Specification<KeadaanPendidikan> specification,
        String keyword) {
            Specification<KeadaanPendidikan> spec =
            KeadaanPendidikanSpecification.hasKodePc(keyword).
                or(KeadaanPendidikanSpecification.hasNamaKelurahan(keyword)).
                or(KeadaanPendidikanSpecification.hasNamaPc(keyword));
        return specification.and(spec);
    }
}
