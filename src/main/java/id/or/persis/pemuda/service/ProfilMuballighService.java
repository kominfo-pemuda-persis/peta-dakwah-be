package id.or.persis.pemuda.service;

import java.util.UUID;

import id.or.persis.pemuda.dto.ProfilMuballighDTO;
import id.or.persis.pemuda.entity.ProfilMuballigh;
import id.or.persis.pemuda.enumeration.ScopeRequest;
import id.or.persis.pemuda.mapper.ProfilMuballighMapper;
import id.or.persis.pemuda.repository.ProfilMuballighRepository;
import id.or.persis.pemuda.specification.ProfilMuballighSpecification;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProfilMuballighService {
    private final ProfilMuballighRepository profilMuballighRepository;
    private final ProfilMuballighMapper profilMuballighMapper;

    @Transactional
    public ProfilMuballighDTO saveProfilMuballigh(ProfilMuballighDTO profilMuballighDTO) {
        log.info("Save new Profil Muballigh ...");
        return profilMuballighMapper
                .toDto(profilMuballighRepository.save(profilMuballighMapper.toEntity(profilMuballighDTO)));
    }

    @Transactional(readOnly = true)
    public Page<ProfilMuballighDTO> getAllProfilMuballigh(Specification<ProfilMuballigh> specification, Pageable page) {
        return profilMuballighRepository.findAll(specification, page)
                .map(profilMuballighMapper::toDto);
    }

    public ProfilMuballighDTO getByProfilMuballighById(UUID id) {
        return profilMuballighMapper.toDto(profilMuballighRepository
                .findOne(ProfilMuballighSpecification.hasId(id))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "data not found")));
    }

    public ProfilMuballighDTO getByProfilMuballighById(UUID id, Boolean isDeleted) {
        return profilMuballighMapper.toDto(profilMuballighRepository
                .findOne(ProfilMuballighSpecification.hasId(id).and(ProfilMuballighSpecification.isDeleted(isDeleted)))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "data not found")));
    }

    public ProfilMuballigh updateProfilMuballigh(ProfilMuballighDTO profilMuballighDTO, UUID id) {
        this.getByProfilMuballighById(id);
        return profilMuballighRepository.save(profilMuballighMapper.toEntity(profilMuballighDTO));
    }

    public ProfilMuballighDTO deleteProfilMuballighById(UUID id) {
        ProfilMuballighDTO profilMuballighDto = this.getByProfilMuballighById(id);
        profilMuballighRepository.delete(profilMuballighMapper.toEntity(profilMuballighDto));
        return profilMuballighDto;
    }

    public Specification<ProfilMuballigh> validateSpecification(boolean isDeleted, String scope) {
        Specification<ProfilMuballigh> specification = ProfilMuballighSpecification.isDeleted(isDeleted);

        if (ScopeRequest.valueOf(scope).isAll() && isDeleted) {
            specification = null;
        }

        return specification;
    }

    public Specification<ProfilMuballigh> addKeywordSpecification(Specification<ProfilMuballigh> specification,
                                                                  String keyword) {
        Specification<ProfilMuballigh> spec =
                ProfilMuballighSpecification.hasKodePc(keyword).
                        or(ProfilMuballighSpecification.hasNamaKelurahan(keyword)).
                        or(ProfilMuballighSpecification.hasNamaLengkap(keyword)).
                        or(ProfilMuballighSpecification.hasEmail(keyword)).
                        or(ProfilMuballighSpecification.hasNamaPc(keyword));
        return specification.and(spec);
    }
}
