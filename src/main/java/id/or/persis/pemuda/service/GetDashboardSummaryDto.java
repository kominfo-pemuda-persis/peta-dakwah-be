package id.or.persis.pemuda.service;

import lombok.Builder;

@Builder
public record GetDashboardSummaryDto(Integer keadaanJamaahPc, Integer keadaanPendidikanPc, Integer angketPc,
                                     Integer profilMuballighPc, Integer keadaanJamaahPd, Integer keadaanPendidikanPd,
                                     Integer angketPd, Integer profilMuballighPd, Integer keadaanJamaahPw,
                                     Integer keadaanPendidikanPw, Integer angketPw, Integer profilMuballighPw) {

    static GetDashboardSummaryDto empty() {
        return new GetDashboardSummaryDto(0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0);
    }
}
