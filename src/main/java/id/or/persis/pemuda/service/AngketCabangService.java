package id.or.persis.pemuda.service;

import java.util.UUID;

import id.or.persis.pemuda.dto.AngketCabangDTO;
import id.or.persis.pemuda.entity.AngketCabang;
import id.or.persis.pemuda.enumeration.ScopeRequest;
import id.or.persis.pemuda.mapper.AngketCabangMapper;
import id.or.persis.pemuda.repository.AngketCabangRepository;
import id.or.persis.pemuda.specification.AngketCabangSpecification;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

@Service
@RequiredArgsConstructor
@Slf4j
public class AngketCabangService {

    private final AngketCabangMapper angketCabangMapper;
    private final AngketCabangRepository angketCabangRepository;

    public Page<AngketCabangDTO> getAllAngketCabang(Specification<AngketCabang> specification, Pageable page) {
        return angketCabangRepository.findAll(specification, page).map(this::getAngketCabangDto);
    }

    @Transactional
    public AngketCabangDTO saveAngketCabang(AngketCabangDTO angketCabangDTO) {
        return getAngketCabangDto(angketCabangRepository.save(getAngketCabang(angketCabangDTO)));
    }

    @Transactional
    public AngketCabangDTO updateAngketCabang(AngketCabangDTO angketCabangDTO, UUID id) {
        this.getAngketCabangById(id);
        return saveAngketCabang(angketCabangDTO);
    }

    @Transactional
    public AngketCabangDTO deleteAngketCabangById(UUID id) {
        AngketCabangDTO existingAngketCabang = this.getAngketCabangById(id);
        angketCabangRepository.delete(getAngketCabang(existingAngketCabang));
        return existingAngketCabang;
    }

    public AngketCabangDTO getAngketCabangById(UUID id) {
        Specification<AngketCabang> specification = AngketCabangSpecification.hasId(id).and(AngketCabangSpecification.isDeleted(false));
        AngketCabang angketCabang = angketCabangRepository.findOne(specification).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "DATA NOT FOUND"));
        return getAngketCabangDto(angketCabang);
    }

    private AngketCabangDTO getAngketCabangDto(AngketCabang angketCabang) {
        return angketCabangMapper.toDto(angketCabang);
    }

    private AngketCabang getAngketCabang(AngketCabangDTO angketCabangDTO) {
        return angketCabangMapper.toEntity(angketCabangDTO);
    }

    public Specification<AngketCabang> validateSpecification(boolean isDeleted, String scope) {
        Specification<AngketCabang> specification = AngketCabangSpecification.isDeleted(isDeleted);

        if (ScopeRequest.valueOf(scope).isAll() && isDeleted) {
            specification = null;
        }

        return specification;
    }
}
