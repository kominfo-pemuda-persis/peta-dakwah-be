package id.or.persis.pemuda.service;

import id.or.persis.pemuda.dto.VillagesCoordinateDTO;
import id.or.persis.pemuda.entity.VillagesCoordinate;
import id.or.persis.pemuda.enumeration.ScopeRequest;
import id.or.persis.pemuda.mapper.VillagesCoordinateMapper;
import id.or.persis.pemuda.repository.VillagesCoordinateRepository;
import id.or.persis.pemuda.specification.VillagesCoordinateSpecification;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

/**
 * Created by IntelliJ IDEA.
 * Project : peta-dakwah-be
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/5/22
 * Time: 17:38
 * To change this template use File | Settings | File Templates.
 */
@Service
@RequiredArgsConstructor
@Slf4j
@Transactional
public class VillagesCoordinateService {

    private final VillagesCoordinateRepository villagesCoordinateRepository;

    private final VillagesCoordinateMapper villagesCoordinateMapper;

    public Page<VillagesCoordinate> getAll(String kode, String nama, String ibukota, Pageable page) {
        Specification<VillagesCoordinate> spec = VillagesCoordinateSpecification.hasNama(nama);
        if (StringUtils.isNotEmpty(ibukota)) spec = VillagesCoordinateSpecification.hasIbuKota(ibukota);
        if (StringUtils.isNotEmpty(kode)) spec = spec.and(VillagesCoordinateSpecification.hasKode(kode));
        return villagesCoordinateRepository.findAll(spec, page);
    }

    public VillagesCoordinate update(VillagesCoordinateDTO villagesCoordinateDTO, String id) {
        VillagesCoordinate coordinate = villagesCoordinateRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Data not found"));
        coordinate = villagesCoordinateMapper.toEntity(villagesCoordinateDTO);
        return villagesCoordinateRepository.save(coordinate);
    }

    public VillagesCoordinate create(VillagesCoordinateDTO villagesCoordinateDTO) {
        return villagesCoordinateRepository.save(villagesCoordinateMapper.toEntity(villagesCoordinateDTO));
    }

    @Transactional
    public VillagesCoordinateDTO deleteVillagesCoordinateById(String id) {
        VillagesCoordinateDTO existingVillage = this.getVillageCoordinateById(id);
        villagesCoordinateRepository.delete(getVillageCoordinate(existingVillage));
        return existingVillage;
    }

    private VillagesCoordinate getVillageCoordinate(VillagesCoordinateDTO villagesCoordinateDTO) {
        return villagesCoordinateMapper.toEntity(villagesCoordinateDTO);
    }

    public VillagesCoordinateDTO getVillageCoordinateById(String id) {
        Specification<VillagesCoordinate> specification = VillagesCoordinateSpecification.hasKode(id).and(VillagesCoordinateSpecification.isDeleted(false));
        VillagesCoordinate villagesCoordinate = villagesCoordinateRepository.findOne(specification).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "DATA NOT FOUND"));
        return getVillageCoordinateDto(villagesCoordinate);
    }

    private VillagesCoordinateDTO getVillageCoordinateDto(VillagesCoordinate villagesCoordinate) {
        return villagesCoordinateMapper.toDTO(villagesCoordinate);
    }

    public Specification<VillagesCoordinate> validateSpecification(boolean isDeleted, String scope) {
        Specification<VillagesCoordinate> specification = VillagesCoordinateSpecification.isDeleted(isDeleted);

        if (ScopeRequest.valueOf(scope).isAll() && isDeleted) {
            specification = null;
        }

        return specification;
    }
}
