package id.or.persis.pemuda.service;

import id.or.persis.pemuda.dto.RoleUserDTO;
import id.or.persis.pemuda.entity.Role;
import id.or.persis.pemuda.entity.User;
import id.or.persis.pemuda.repository.RoleRepository;
import id.or.persis.pemuda.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RoleUserService {

    private static final String USER_NOT_FOUND = "User not found";
    private static final String ROLE_NOT_FOUND = "Role not found";

    private final RoleRepository roleRepository;

    private final UserRepository userRepository;

    public String validateDTO(RoleUserDTO dto) {
        Optional<User> user = userRepository.findByIdAndDeletedAtIsNull(dto.getUserId());

        if (Objects.isNull(dto.getUserId()) || dto.getUserId().equals("")) {
            return "User ID can't null or empty";
        } else if (user.isEmpty()) {
            return "User not found or deleted";
        } else if (dto.getRoleNames().isEmpty() || dto.getRoleNames().size() == 0)  {
            return "Roles can't null or empty";
        } else {
            return null;
        }
    }

    public User addRoleToUser(RoleUserDTO dto) {
        Optional<User> user = userRepository.findByIdAndDeletedAtIsNull(dto.getUserId());
        User usr = user.get();
        Collection<Role> roles = new ArrayList<>();
        dto.getRoleNames().stream().forEach(r -> {
            Optional<Role> role = roleRepository.findByRoleName(r);
            roles.add(role.get());
        });

        usr.setRoles(roles);
        userRepository.save(usr);

        return user.get();
    }
}
