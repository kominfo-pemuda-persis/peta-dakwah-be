package id.or.persis.pemuda.service;

import id.or.persis.pemuda.dto.RoleDTO;
import id.or.persis.pemuda.entity.Role;
import id.or.persis.pemuda.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class RoleService {

    private final RoleRepository roleRepository;

    public List<Role> getAll() {
        return roleRepository.findAll();
    }

    public Role findById(UUID id) {
        return roleRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Role not found"));
    }

    public Role save(RoleDTO dto) {
        return roleRepository.save(convertDTOToEntity(dto));
    }

    public Role update(RoleDTO dto, UUID id) {
        Role role = roleRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Role not found"));
        role.setRoleName(dto.getRoleName());
        role.setDescription(dto.getDescription());
        return roleRepository.save(role);
    }

    public void deleteById(UUID id) {
        roleRepository.deleteById(id);
    }

    public String validateDTO(RoleDTO dto) {
        if (Objects.isNull(dto.getRoleName()) || dto.getRoleName().isBlank()) {
            return "Role can't null or empty";
        } else if (Objects.isNull(dto.getDescription()) || StringUtils.isBlank(dto.getDescription())) {
            return "Description can't null or empty";
        } else if (dto.getRoleName().length() > 50) {
            return "Max character is 50";
        } else if (dto.getDescription().length() > 250) {
            return "Max character is 250";
        } else {
            return null;
        }
    }

    private Role convertDTOToEntity(RoleDTO dto) {
        return Role.builder()
                .roleName(dto.getRoleName())
                .description(dto.getDescription())
                .build();
    }
}
