package id.or.persis.pemuda.service;

import id.or.persis.pemuda.dto.StudentDTO;
import id.or.persis.pemuda.entity.Student;
import id.or.persis.pemuda.exception.StudentNotFoundException;
import id.or.persis.pemuda.mapper.StudentMapper;
import id.or.persis.pemuda.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * Project : student-crud-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/03/22
 * Time: 06.24
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class StudentService {
    private static final String STUDENT = "Student with ";
    private static final String NOTFOUND = " is Not Found!";
    private final StudentRepository studentRepository;
    private final StudentMapper studentMapper;


    @Transactional(readOnly = true)
    public List<StudentDTO> getAllStudents() {
        return studentMapper.toDtos(studentRepository.findAll());
    }

    @Transactional(readOnly = true)
    public Optional<Student> findById(UUID id) {
        return studentRepository.findById(id).map(student1 -> {
            student1.getPc().getPd().getPw();
            student1.getKelurahan().getKecamatan().getKota().getProvinsi();
            return student1;
        });
    }

    public Optional<Student> findByEmail(String email) {
        return studentRepository.findByEmail(email);
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public Student save(StudentDTO std) {
        return studentRepository.save(studentMapper.studentDtoToStudent(std));
    }

    public Student update(UUID uuid, StudentDTO newStd) {
        Student student = studentRepository.findById(uuid)
                .orElseThrow(() -> new StudentNotFoundException(STUDENT + uuid + NOTFOUND));
        student.setFirstName(newStd.getFirstName());
        student.setLastName(newStd.getLastName());
        student.setEmail(newStd.getEmail());
        student.setPhone(newStd.getPhone());
        return studentRepository.save(student);
    }

    public void deleteById(UUID id) {
        studentRepository.deleteById(id);
    }
}
