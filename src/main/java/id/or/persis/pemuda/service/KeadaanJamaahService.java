package id.or.persis.pemuda.service;

import java.util.UUID;

import id.or.persis.pemuda.dto.KeadaanJamaahDTO;
import id.or.persis.pemuda.entity.KeadaanJamaah;
import id.or.persis.pemuda.enumeration.ScopeRequest;
import id.or.persis.pemuda.exception.KeadaanJamaahNotFoundException;
import id.or.persis.pemuda.mapper.KeadaanJamaahMapper;
import id.or.persis.pemuda.repository.KeadaanJamaahRepository;
import id.or.persis.pemuda.specification.KeadaanJamaahSpecification;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class KeadaanJamaahService {
    private static final String KEADAAN = "Keadaan Jamaah";
    private final KeadaanJamaahRepository keadaanJamaahRepository;
    private final KeadaanJamaahMapper keadaanJamaahMapper;

    public Page<KeadaanJamaahDTO> getAllKeadaanJamaah(Specification<KeadaanJamaah> specification, Pageable page) {
        return keadaanJamaahRepository.findAll(specification, page)
                .map(jamaah -> keadaanJamaahMapper.toDto(jamaah));
    }

    public KeadaanJamaahDTO getKeadaanJamaahById(UUID id) {
        return keadaanJamaahMapper.toDto(keadaanJamaahRepository
                .findOne(KeadaanJamaahSpecification.hasId(id))
                .orElseThrow(() -> new KeadaanJamaahNotFoundException(KEADAAN, "id", id)));
    }

    public KeadaanJamaahDTO getKeadaanJamaahById(UUID id, Boolean isDeleted) {
        return keadaanJamaahMapper.toDto(keadaanJamaahRepository
                .findOne(KeadaanJamaahSpecification.isDeleted(isDeleted).and(KeadaanJamaahSpecification.hasId(id)))
                .orElseThrow(() -> new KeadaanJamaahNotFoundException(KEADAAN, "id", id)));
    }

    @Transactional
    public KeadaanJamaahDTO saveKeadaanJamaah(KeadaanJamaahDTO keadaanJamaahDTO) {
        log.info("Save new KeadaanJamaah ...");
        return keadaanJamaahMapper.toDto(keadaanJamaahRepository.save(keadaanJamaahMapper.toEntity(keadaanJamaahDTO)));
    }

    public KeadaanJamaahDTO updateKeadaanJamaah(KeadaanJamaahDTO keadaanJamaahDTO, UUID id) {
        this.getKeadaanJamaahById(id);
        return keadaanJamaahMapper.toDto(keadaanJamaahRepository.save(keadaanJamaahMapper.toEntity(keadaanJamaahDTO)));
    }

    public KeadaanJamaahDTO deleteKeadaanJamaahById(UUID id) {
        KeadaanJamaahDTO existingKeadaanJamaah = this.getKeadaanJamaahById(id);
        keadaanJamaahRepository.delete(keadaanJamaahMapper.toEntity(existingKeadaanJamaah));
        return existingKeadaanJamaah;
    }

    public Specification<KeadaanJamaah> validateSpecification(boolean isDeleted, String scope) {
        Specification<KeadaanJamaah> specification = KeadaanJamaahSpecification.isDeleted(isDeleted);

        if(ScopeRequest.valueOf(scope).isAll() && isDeleted) {
            specification = null;
        }

        return specification;
    }

    public Specification<KeadaanJamaah> addKeywordSpecification(Specification<KeadaanJamaah> specification,
        String keyword) {
            Specification<KeadaanJamaah> spec =
            KeadaanJamaahSpecification.hasKodePc(keyword).
                or(KeadaanJamaahSpecification.hasNamaKelurahan(keyword)).
                or(KeadaanJamaahSpecification.hasNamaPc(keyword));
        return specification.and(spec);
    }
}
