package id.or.persis.pemuda.service;

import id.or.persis.pemuda.dto.CitiesCoordinateDto;
import id.or.persis.pemuda.entity.CitiesCoordinate;
import id.or.persis.pemuda.mapper.CitiesCoordinateMapper;
import id.or.persis.pemuda.repository.CitiesCoordinateRepository;
import id.or.persis.pemuda.specification.CitiesCoordinateSpecification;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

@Service
@RequiredArgsConstructor
@Transactional
public class CitiesCoordinateService {

    private final CitiesCoordinateRepository citiesCoordinateRepository;

    private final CitiesCoordinateMapper citiesCoordinateMapper;

    public Page<CitiesCoordinate> getAll(String keyword, Pageable page) {
        Specification<CitiesCoordinate> spec = this.buildSpecification(keyword);
        return citiesCoordinateRepository.findAll(spec, page);
    }

    public Specification<CitiesCoordinate> buildSpecification(String keyword) {
        Specification<CitiesCoordinate> spec = 
            CitiesCoordinateSpecification.hasNama(keyword).
                or(CitiesCoordinateSpecification.hasIbuKota(keyword)).
                or(CitiesCoordinateSpecification.hasKodeLike(keyword));
        return spec;
    }

    public CitiesCoordinateDto getById(String id) {
        CitiesCoordinate coordinate = citiesCoordinateRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Data not found"));
        return citiesCoordinateMapper.toDto(coordinate);
    }

    public CitiesCoordinate create(CitiesCoordinateDto citiesCoordinateDto) {
        return citiesCoordinateRepository.save(citiesCoordinateMapper.toEntity(citiesCoordinateDto));
    }

    public CitiesCoordinate update(CitiesCoordinateDto coordinateDto, String id) {
        CitiesCoordinate coordinate = citiesCoordinateRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Data not found"));
        coordinate = citiesCoordinateMapper.toEntity(coordinateDto);
        return citiesCoordinateRepository.save(coordinate);
    }

    public String delete(String id) {
        citiesCoordinateRepository.deleteById(id);
        return "Cities Coordinate telah dihapus";
    }
}
