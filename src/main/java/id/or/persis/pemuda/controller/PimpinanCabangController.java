package id.or.persis.pemuda.controller;

import id.or.persis.pemuda.dto.ResponseHandler;
import id.or.persis.pemuda.entity.PimpinanCabang;
import id.or.persis.pemuda.repository.PimpinanCabangRepository;
import id.or.persis.pemuda.specification.PimpinanCabangSpecification;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequiredArgsConstructor
@Tag(name = "Pimpinan Cabang", description = "Endpoint for list all branch-headquarter")
@SecurityRequirement(name = "Bearer Authentication")
public class PimpinanCabangController {
    private final PimpinanCabangRepository pimpinanCabangRepository;

    @GetMapping("/api/pc")
    @Operation(
            summary = "List All Branch-Headquarter",
            description = "List All Branch-Headquarter."
    )
    @ApiResponses(value = {
           @io.swagger.v3.oas.annotations.responses.ApiResponse(
                   description = "Success",
                   responseCode = "200",
                   content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                           PimpinanCabang.class))
           ),
           @io.swagger.v3.oas.annotations.responses.ApiResponse(
                   description = "Not Found",
                   responseCode = "404",
                   content = @Content
           ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Internal Error",
                    responseCode = "404",
                    content = @Content
            )
    })
    public ResponseEntity<Object> getAllPimpinanCabang(
            @ParameterObject @PageableDefault(sort = {"id"}, direction = Sort.Direction.ASC, value = 5) Pageable pageable,
            @Parameter(description = "keyword Kode PC or Name PC") @RequestParam(required = false, defaultValue = "") String keyword,
			@Parameter(description = "keyword Kode PD to search only specific PD") @RequestParam(required = false, defaultValue = "", name = "kode-pd") String kodePd) {
        try {
        	Specification<PimpinanCabang> spec = PimpinanCabangSpecification.hasKodePc(keyword)
				.or(PimpinanCabangSpecification.hasNamaPc(keyword));

            if (StringUtils.isNotBlank(kodePd)) {
                spec = spec.and(PimpinanCabangSpecification.hasKodePd(kodePd));
            }
            return ResponseHandler.generateResponse("Successfully retrieved data!", HttpStatus.OK, HttpStatus.OK,
                    pimpinanCabangRepository.findAll(spec, pageable));
        } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR, null);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,
                    HttpStatus.INTERNAL_SERVER_ERROR, null);
        }
    }
}
