package id.or.persis.pemuda.controller;

import id.or.persis.pemuda.dto.CitiesCoordinateDto;
import id.or.persis.pemuda.dto.ResponseHandler;
import id.or.persis.pemuda.entity.CitiesCoordinate;
import id.or.persis.pemuda.entity.KeadaanPendidikan;
import id.or.persis.pemuda.service.CitiesCoordinateService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
@Tag(
        name = "Cities Coordinate",
        description = "Endpoints to manage Cities Coordinate"
)
@SecurityRequirement(name = "Bearer Authentication")
public class CitiesCoordinateController {

    private static final String SAVED_DATA = "Successfully saved data!";
    private static final String RETRIEVED = "Successfully retrieved data!";
    private final CitiesCoordinateService service;

    @PostMapping("/cities-coordinate")
    @Operation(
            summary = "Add New Cities Coordinate",
            description = "Add New Cities Coordinate.",
            tags = {"Cities Coordinate"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            CitiesCoordinate.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not Authorized", responseCode = "401",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Forbidden", responseCode = "403",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public ResponseEntity<Object> saveNewCities(@RequestBody CitiesCoordinateDto coordinateDto) {
        try {
            return ResponseHandler.generateResponse(SAVED_DATA, HttpStatus.OK, HttpStatus.OK,
                    service.create(coordinateDto));
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, HttpStatus.MULTI_STATUS,
                    null);
        }
    }

    @GetMapping("/cities-coordinate")
    @Operation(
            summary = "List Cities Coordinate",
            description = "List Cities Coordinate.",
            tags = {"Cities Coordinate"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            CitiesCoordinate.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not Authorized", responseCode = "401",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Forbidden", responseCode = "403",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public ResponseEntity<Object> getAllCities(@ParameterObject @PageableDefault(sort = {
            "kode"}, direction = Sort.Direction.ASC, value = 5) Pageable pageable,
        @Parameter(description = "Keyword for filter data") @RequestParam(required = false, defaultValue = "") String keyword) {
        try {
            return ResponseHandler.generateResponse(RETRIEVED, HttpStatus.OK, HttpStatus.OK,
                    service.getAll(keyword, pageable));
        } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR, null);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,
                    HttpStatus.INTERNAL_SERVER_ERROR, null);
        }
    }

    @GetMapping("/cities-coordinate/{id}")
    @Operation(
            summary = "Get Cities Coordinate By Id",
            description = "Get Cities Coordinate By Id.",
            tags = {"Cities Coordinate"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            CitiesCoordinate.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not Authorized", responseCode = "401",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Forbidden", responseCode = "403",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public ResponseEntity<Object> getCitiesById(@PathVariable String id) {
        try {
            return ResponseHandler.generateResponse(SAVED_DATA, HttpStatus.OK, HttpStatus.OK,
                    service.getById(id));
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, HttpStatus.MULTI_STATUS,
                    null);
        }
    }

    @PutMapping("/cities-coordinate/{id}")
    @Operation(
            summary = "Update Cities Coordinate",
            description = "Update Cities Coordinate",
            tags = {"Cities Coordinate"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            CitiesCoordinate.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not Authorized", responseCode = "401",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Forbidden", responseCode = "403",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public ResponseEntity<Object> updateCities(@RequestBody CitiesCoordinateDto coordinateDto, @PathVariable String id) {
        try {
            return ResponseHandler.generateResponse("Successfully updated data!", HttpStatus.OK, HttpStatus.OK,
                    service.update(coordinateDto, id));
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, HttpStatus.MULTI_STATUS,
                    null);
        }
    }

    @DeleteMapping("/cities-coordinate/{id}")
    @Operation(
            summary = "Delete Cities Coordinate",
            description = "Delete Cities Coordinate",
            tags = {"Cities Coordinate"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            KeadaanPendidikan.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not Authorized", responseCode = "401",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Forbidden", responseCode = "403",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public ResponseEntity<Object> deleteCities(@PathVariable String id) {
        try {
            return ResponseHandler.generateResponse("Successfully updated data!", HttpStatus.OK, HttpStatus.OK,
                    service.delete(id));
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, HttpStatus.MULTI_STATUS,
                    null);
        }
    }
}
