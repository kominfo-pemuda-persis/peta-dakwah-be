package id.or.persis.pemuda.controller;

import id.or.persis.pemuda.service.DashboardService;
import id.or.persis.pemuda.service.GetDashboardSummaryDto;
import id.or.persis.pemuda.service.GetDashboardSummaryRequest;
import id.or.persis.pemuda.service.UserDetailsImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dashboard/summary")
@RequiredArgsConstructor
@Tag(name = "Dashboard", description = "Endpoint for dashboard page")
@SecurityRequirement(name = "Bearer Authentication")
public class DashboardController {

    private final DashboardService dashboardService;

    @GetMapping
    @Operation(
        summary = "Dashboard summary",
        description = "Get dashboard summary",
        responses = @ApiResponse(
            description = HttpConstant.OK,
            responseCode = HttpConstant.CODE_OK,
            useReturnTypeSchema = true
        )
    )
    @PreAuthorize("""
        hasAnyAuthority('super_admin', 'admin_pp', 'tasykil_pp', \
        'admin_pw', 'tasykil_pw', \
        'admin_pd', 'tasykil_pd', \
        'admin_pc', 'tasykil_pc')\
        """)
    public BaseResponse<GetDashboardSummaryDto> getDashboardSummary(@AuthenticationPrincipal UserDetailsImpl userDetails) {
        var summary = dashboardService.getSummary(new GetDashboardSummaryRequest(userDetails.getId()));

        return BaseResponse.success(summary);
    }
}
