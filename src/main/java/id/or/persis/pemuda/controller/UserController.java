package id.or.persis.pemuda.controller;

import id.or.persis.pemuda.dto.GetUserDTO;
import id.or.persis.pemuda.dto.ResponseHandler;
import id.or.persis.pemuda.dto.UserDTO;
import id.or.persis.pemuda.service.GetAllUsersRequest;
import id.or.persis.pemuda.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.UUID;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
@Tag(name = "User", description = "Endpoint to manage User")
@SecurityRequirement(name = "Bearer Authentication")
public class UserController {
    private static final String SUCCESS = "SUCCESS";

    private final UserService userService;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @GetMapping
    @Operation(
        summary = "List all users",
        description = "Get list of all users",
        responses = @ApiResponse(
            description = HttpConstant.OK,
            responseCode = HttpConstant.CODE_OK,
            useReturnTypeSchema = true
        )
    )
    public BaseResponse<Page<GetUserDTO>> getAllUser(
        @Parameter(description = "user.npa") @RequestParam(required = false) String npa,
        @Parameter(description = "user.email") @RequestParam(required = false) String email,
        @Parameter(description = "user.username") @RequestParam(required = false) String username,
        @Parameter(description = "user.kode_pc.kd_pc") @RequestParam(required = false) String kodePc,
        @Parameter(description = "user.kode_pc.kd_pd.kd_pd") @RequestParam(required = false) String kodePd,
        @Parameter(description = "user.kode_pc.kd_pd.kd_pw.kd_pw") @RequestParam(required = false) String kodePw,
        @Parameter(description = "user.nama_lengkap") @RequestParam(required = false) String namaLengkap,
        @ParameterObject Pageable pageable
    ) {
        var userList = userService.getAllUser(GetAllUsersRequest.builder()
            .npa(npa)
            .email(email)
            .username(username)
            .kodePc(kodePc)
            .kodePd(kodePd)
            .kodePw(kodePw)
            .namaLengkap(namaLengkap)
            .pageable(pageable)
            .build());

        return new BaseResponse<>(
            HttpConstant.SUCCESS,
            HttpStatus.OK,
            userList
        );
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable UUID id) {
        try {
            GetUserDTO user = userService.findById(id);
            return ResponseHandler.generateResponse(SUCCESS, HttpStatus.OK, HttpStatus.OK, user);
        } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), e.getStatusCode(), e.getStatusCode(), null);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,
                    HttpStatus.INTERNAL_SERVER_ERROR, null);
        }
    }

    @PostMapping
    @Operation(
        summary = "Create an user",
        description = "Create user",
        responses = @ApiResponse(
            description = HttpConstant.OK,
            responseCode = HttpConstant.CODE_OK,
            useReturnTypeSchema = true
        )
    )
    public BaseResponse<GetUserDTO> saveUser(@RequestBody UserDTO user) {
        return new BaseResponse<>(
            HttpConstant.SUCCESS,
            HttpStatus.OK,
            userService.save(user)
        );
    }

    @PutMapping("/{id}")
    @Operation(
        summary = "Update an user",
        description = "Update user by its id",
        responses = @ApiResponse(
            description = HttpConstant.OK,
            responseCode = HttpConstant.CODE_OK,
            useReturnTypeSchema = true
        )
    )
    public BaseResponse<GetUserDTO> updateUser(@RequestBody UserDTO user, @PathVariable UUID id) {
        return new BaseResponse<>(
            HttpConstant.SUCCESS,
            HttpStatus.OK,
            userService.update(user, id)
        );
    }

    @DeleteMapping("/{id}")
    @Operation(
        summary = "Delete an user",
        description = "Delete user by its id",
        responses = @ApiResponse(
            description = HttpConstant.OK,
            responseCode = HttpConstant.CODE_OK,
            useReturnTypeSchema = true
        )
    )
    public BaseResponse<String> deleteUser(@PathVariable UUID id) {
        return new BaseResponse<>(
            HttpConstant.SUCCESS,
            HttpStatus.OK,
            userService.deleteById(id)
        );
    }

}
