package id.or.persis.pemuda.controller;

import id.or.persis.pemuda.dto.ResponseHandler;
import id.or.persis.pemuda.entity.PimpinanDaerah;
import id.or.persis.pemuda.repository.PimpinanWilayahRepository;
import id.or.persis.pemuda.specification.PimpinanWilayahSpecification;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequiredArgsConstructor
@Tag(name = "Pimpinan Wilayah", description = "Endpoint for list all PW")
@SecurityRequirement(name = "Bearer Authentication")
public class PimpinanWilayahController {
    private final PimpinanWilayahRepository pimpinanWilayahRepository;

    @GetMapping("/api/pw")
    @Operation(
            summary = "List All PW",
            description = "List All PW Pemuda Persis."
    )
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            PimpinanDaerah.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Not found",
                    responseCode = "404",
                    content = @Content
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Internal Error",
                    responseCode = "500",
                    content = @Content
            )
    })

    public ResponseEntity<Object> getAllPimpinanWilayah(
            @ParameterObject @PageableDefault(sort = {"id"}, direction = Sort.Direction.ASC, value = 5) Pageable pageable,
            @Parameter(description = "keyword Kode PW or Name PW") @RequestParam(required = false, defaultValue = "") String keyword) {
        try {
            return ResponseHandler.generateResponse("Successfully retrieved data!", HttpStatus.OK, HttpStatus.OK,
                    pimpinanWilayahRepository.findAll(
                        PimpinanWilayahSpecification.hasKodePw(keyword)
                                .or(PimpinanWilayahSpecification.hasNamaPw(keyword)), pageable));
        } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR, null);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,
                    HttpStatus.INTERNAL_SERVER_ERROR, null);
        }
    }
}
