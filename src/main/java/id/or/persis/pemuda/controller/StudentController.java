package id.or.persis.pemuda.controller;

import id.or.persis.pemuda.dto.StudentDTO;
import id.or.persis.pemuda.entity.Student;
import id.or.persis.pemuda.exception.StudentNotFoundException;
import id.or.persis.pemuda.service.StudentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * Project : student-crud-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/03/22
 * Time: 06.31
 */
@RestController
@RequestMapping("/api/students")
@RequiredArgsConstructor
@Slf4j
@Tag(name = "Student", description = "Endpoints to manage Data Student")
@SecurityRequirement(name = "Bearer Authentication")
public class StudentController {
    private static final String STUDENT = "Student with ";
    private static final String NOTFOUND = " is Not Found!";
    private final StudentService studentservice;

    @GetMapping
    @Operation(
            summary = "List All Students Data",
            description = "List All Students Data."
    )
    @ApiResponses(value = {
            @ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            StudentDTO.class))
            ),
            @ApiResponse(
                    description = "Not found",
                    responseCode = "404",
                    content = @Content
            ),
            @ApiResponse(
                    description = "Internal error",
                    responseCode = "500",
                    content = @Content
            )
    })
    public List<StudentDTO> getAllStudents() {
        return studentservice.getAllStudents();
    }

    @GetMapping(value = "/{id}")
    @Operation(
            summary = "List All Students Data by ID",
            description = "List All Students Data by ID."
    )
    @ApiResponses(value = {
            @ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            StudentDTO.class))
            ),
            @ApiResponse(
                    description = "Not found",
                    responseCode = "404",
                    content = @Content
            ),
            @ApiResponse(
                    description = "Internal error",
                    responseCode = "500",
                    content = @Content
            )
    })
    public Student getStudentById(@PathVariable UUID id) {
        return studentservice.findById(id)
                .orElseThrow(() -> new StudentNotFoundException(STUDENT + id + NOTFOUND));
    }

    @PostMapping
    @Operation(
            summary = "Add New Student Data",
            description = "Add New Student Data."
    )
    @ApiResponses(value = {
            @ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            StudentDTO.class))
            ),
            @ApiResponse(
                    description = "Not found",
                    responseCode = "404",
                    content = @Content
            ),
            @ApiResponse(
                    description = "Internal error",
                    responseCode = "500",
                    content = @Content
            )
    })
    public Student addStudent(@RequestBody StudentDTO std) {
        return studentservice.save(std);
    }

    @PutMapping(value = "/{id}")
    @Operation(
            summary = "Update Students Data by ID",
            description = "Update Students Data by ID."
    )
    @ApiResponses(value = {
            @ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            StudentDTO.class))
            ),
            @ApiResponse(
                    description = "Not found",
                    responseCode = "404",
                    content = @Content
            ),
            @ApiResponse(
                    description = "Internal error",
                    responseCode = "500",
                    content = @Content
            )
    })
    public Student updateStudent(@PathVariable UUID id, @Valid @RequestBody StudentDTO newStd) {
        return studentservice.update(id, newStd);
    }

    @DeleteMapping(value = "/{id}")
    @Operation(
            summary = "Delete Students Data by ID",
            description = "Delete Students Data by ID."
    )
    @ApiResponses(value = {
            @ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            StudentDTO.class))
            ),
            @ApiResponse(
                    description = "Not found",
                    responseCode = "404",
                    content = @Content
            ),
            @ApiResponse(
                    description = "Internal error",
                    responseCode = "500",
                    content = @Content
            )
    })
    public String deleteStudent(@PathVariable UUID id) {
        Student std = studentservice.findById(id)
                .orElseThrow(() -> new StudentNotFoundException(STUDENT + id + NOTFOUND));
        studentservice.deleteById(std.getId());
        return "Student with ID :" + id + " is deleted";
    }
}
