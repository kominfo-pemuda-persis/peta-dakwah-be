package id.or.persis.pemuda.controller;

import id.or.persis.pemuda.dto.ResponseHandler;
import id.or.persis.pemuda.dto.RoleUserDTO;
import id.or.persis.pemuda.entity.User;
import id.or.persis.pemuda.service.RoleUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@RestController
@RequestMapping("/api/roles/user")
@RequiredArgsConstructor
@Slf4j
@Tag(name = "RoleUser", description = "Endpoints to manage Data Role User")
@SecurityRequirement(name = "Bearer Authentication")
public class RoleUserController {

    private static final String SUCCESS = "SUCCESS";

    private final RoleUserService roleUserService;

    @PostMapping
    @Operation(
            summary = "Add New Role to User",
            description = "Add existing Role existing User."
    )
    @ApiResponses(value = {
            @ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            RoleUserDTO.class))
            ),
            @ApiResponse(
                    description = "Not found",
                    responseCode = "404",
                    content = @Content
            ),
            @ApiResponse(
                    description = "Internal error",
                    responseCode = "500",
                    content = @Content
            )
    })
    public ResponseEntity<Object> addRoleUser(@RequestBody RoleUserDTO dto) {
        String validation = roleUserService.validateDTO(dto);
        if (Objects.nonNull(validation)) {
            return ResponseHandler.generateResponse(validation, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST, null);
        }
        User user = roleUserService.addRoleToUser(dto);
        return ResponseHandler.generateResponse(SUCCESS, HttpStatus.OK, HttpStatus.OK, user);
    }
}
