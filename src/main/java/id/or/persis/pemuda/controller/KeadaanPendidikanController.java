package id.or.persis.pemuda.controller;

import id.or.persis.pemuda.dto.KeadaanPendidikanDTO;
import id.or.persis.pemuda.dto.ResponseHandler;
import id.or.persis.pemuda.entity.KeadaanPendidikan;
import id.or.persis.pemuda.service.KeadaanPendidikanService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
@Tag(
        name = "Keadaan Pendidikan",
        description = "Endpoints to manage Keadaan Pendidikan"
)
@SecurityRequirement(name = "Bearer Authentication")
public class KeadaanPendidikanController {

    private final KeadaanPendidikanService pendidikanService;

    @PostMapping("/pendidikan")
    @Operation(
            summary = "Add New Keadaan Pendidikan",
            description = "Add New Keadaan Pendidikan.",
            tags = {"Keadaan Pendidikan"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            KeadaanPendidikanDTO.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not Authorized", responseCode = "401",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Forbidden", responseCode = "403",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public ResponseEntity<Object> saveNewKeadaanPendidikan(@RequestBody KeadaanPendidikanDTO pendidikan) {
        try {
            return ResponseHandler.generateResponse("Successfully saved data!", HttpStatus.OK, HttpStatus.OK,
                    pendidikanService.saveKeadaanPendidikan(pendidikan));
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, HttpStatus.MULTI_STATUS,
                    null);
        }
    }

    @GetMapping("/pendidikan")
    @Operation(
            summary = "List Keadaan Pendidikan",
            description = "List Keadaan Pendidikan.",
            tags = {"Keadaan Pendidikan"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            KeadaanPendidikanDTO.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not Authorized", responseCode = "401",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Forbidden", responseCode = "403",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public ResponseEntity<Object> getAllKeadaanPendidikan(
            @ParameterObject @PageableDefault(sort = {
                    "createdAt"}, direction = Sort.Direction.ASC, value = 5) Pageable pageable,
            @Parameter(description = "Soft delete data status") @RequestParam(required = false, defaultValue = "false") String deleted,
            @Parameter(description = "Keyword for filter data") @RequestParam(required = false, defaultValue = "") String keyword,
            @Parameter(description = "Get Specific data or all data (deleted and active data)") @RequestParam(required = false, defaultValue = "SPECIFIC") String scope
    ) {
        try {
            Specification<KeadaanPendidikan> specification = pendidikanService.validateSpecification(Boolean.parseBoolean(deleted), scope);
            specification = pendidikanService.addKeywordSpecification(specification, keyword);
            return ResponseHandler.generateResponse("Successfully retrieve data!", HttpStatus.OK, HttpStatus.OK,
                    pendidikanService.getAllKeadaanPendidikan(specification, pageable));
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, HttpStatus.MULTI_STATUS,
                    null);
        }
    }

    @GetMapping("/pendidikan/{id}")
    @Operation(
            summary = "Get Keadaan Pendidikan by ID",
            description = "Get Keadaan Pendidikan by ID",
            tags = {"Keadaan Pendidikan"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            KeadaanPendidikan.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not Authorized", responseCode = "401",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Forbidden", responseCode = "403",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public ResponseEntity<Object> findById(@PathVariable UUID id) {
        try {
            return ResponseHandler.generateResponse("Successfully retrieve data!", HttpStatus.OK, HttpStatus.OK,
                    pendidikanService.getKeadaanPendidikanById(id));
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, HttpStatus.MULTI_STATUS,
                    null);
        }
    }

    @PutMapping("/pendidikan/{id}")
    @Operation(
            summary = "Update Keadaan Pendidikan",
            description = "Update Keadaan Pendidikan",
            tags = {"Keadaan Pendidikan"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            KeadaanPendidikan.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not Authorized", responseCode = "401",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Forbidden", responseCode = "403",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public ResponseEntity<Object> update(@RequestBody KeadaanPendidikanDTO pendidikan, @PathVariable UUID id) {
        try {
            return ResponseHandler.generateResponse("Successfully updated data!", HttpStatus.OK, HttpStatus.OK,
                    pendidikanService.updateKeadaanPendidikan(pendidikan, id));
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, HttpStatus.MULTI_STATUS,
                    null);
        }
    }

    @DeleteMapping("/pendidikan/{id}")
    @Operation(
            summary = "Delete Keadaan Pendidikan",
            description = "Delete Keadaan Pendidikan",
            tags = {"Keadaan Pendidikan"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            KeadaanPendidikan.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not Authorized", responseCode = "401",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Forbidden", responseCode = "403",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public ResponseEntity<Object> delete(@PathVariable UUID id) {
        try {
            return ResponseHandler.generateResponse("Successfully deleted data!", HttpStatus.OK, HttpStatus.OK,
                    pendidikanService.deleteKeadaanPendidikanById(id));
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, HttpStatus.MULTI_STATUS,
                    null);
        }
    }
}
