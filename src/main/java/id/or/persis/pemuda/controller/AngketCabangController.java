package id.or.persis.pemuda.controller;

import id.or.persis.pemuda.dto.AngketCabangDTO;
import id.or.persis.pemuda.dto.ResponseHandler;
import id.or.persis.pemuda.entity.AngketCabang;
import id.or.persis.pemuda.entity.KeadaanJamaah;
import id.or.persis.pemuda.service.AngketCabangService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/angket-cabang")
@Tag(name = "Angket Cabang", description = "Endpoints to manage Angket Cabang")
@SecurityRequirement(name = "Bearer Authentication")
public class AngketCabangController {
    private final AngketCabangService angketCabangService;

    @GetMapping
    @Operation(
            summary = "List All Angket Cabang",
            description = "List All Angket Cabang."
    )
    @ApiResponses(value = {
            @ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            AngketCabang.class))
            ),
            @ApiResponse(
                    description = "Not found",
                    responseCode = "404",
                    content = @Content
            ),
            @ApiResponse(
                    description = "Internal error",
                    responseCode = "500",
                    content = @Content
            )
    })
    public ResponseEntity<Object> getAllAngketCabang(
            @ParameterObject @PageableDefault(sort = {
                    "createdAt"}, direction = Sort.Direction.ASC, value = 5) Pageable pageable,
            @Parameter(description = "Soft delete data status") @RequestParam(required = false, defaultValue = "false") String deleted,
            @Parameter(description = "Get Specific data or all data (deleted and active data)") @RequestParam(required = false, defaultValue = "SPECIFIC") String scope
    ) {
        try {
            Specification<AngketCabang> specification = angketCabangService.validateSpecification(Boolean.parseBoolean(deleted), scope);
            return ResponseHandler.generateResponse("Successfully retrieved data!", HttpStatus.OK, HttpStatus.OK,
                    angketCabangService.getAllAngketCabang(specification, pageable));
        } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), e.getStatusCode(), e.getStatusCode(), null);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,
                    HttpStatus.INTERNAL_SERVER_ERROR, null);
        }

    }

    @GetMapping("{id}")
    @Operation(
            summary = "Get Angket Cabang by Id",
            description = "Get Angket Cabang by Id."
    )
    @ApiResponses(value = {
            @ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            AngketCabang.class))
            ),
            @ApiResponse(
                    description = "Not found",
                    responseCode = "404",
                    content = @Content
            ),
            @ApiResponse(
                    description = "Internal error",
                    responseCode = "500",
                    content = @Content
            )
    })
    public ResponseEntity<Object> getAngketCabangById(@PathVariable UUID id) {
        try {
            return ResponseHandler.generateResponse("Successfully retrieved data!", HttpStatus.OK, HttpStatus.OK,
                    angketCabangService.getAngketCabangById(id));
        } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), e.getStatusCode(), e.getStatusCode(), null);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,
                    HttpStatus.INTERNAL_SERVER_ERROR, null);
        }
    }

    @PostMapping
    @Operation(
            summary = "Add new Angket Cabang",
            description = "Add new Angket Cabang."
    )
    @ApiResponses(value = {
            @ApiResponse(
                    description = "Success",
                    responseCode = "201",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            AngketCabang.class))
            ),
            @ApiResponse(
                    description = "Not found",
                    responseCode = "404",
                    content = @Content
            ),
            @ApiResponse(
                    description = "Internal error",
                    responseCode = "500",
                    content = @Content
            )
    })
    public ResponseEntity<Object> saveAngketCabang(@RequestBody AngketCabangDTO angketCabangDTO) {
        try {
            return ResponseHandler.generateResponse("Data successfully saved!", HttpStatus.CREATED,
                    HttpStatus.CREATED, angketCabangService.saveAngketCabang(angketCabangDTO));
        } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), e.getStatusCode(), e.getStatusCode(), null);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,
                    HttpStatus.INTERNAL_SERVER_ERROR, null);
        }
    }

    @PutMapping("{id}")
    @Operation(
            summary = "Update Angket Cabang",
            description = "Update Angket Cabang."
    )
    @ApiResponses(value = {
            @ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            AngketCabang.class))
            ),
            @ApiResponse(
                    description = "Not found",
                    responseCode = "404",
                    content = @Content
            ),
            @ApiResponse(
                    description = "Internal error",
                    responseCode = "500",
                    content = @Content
            )
    })
    public ResponseEntity<Object> updateAngketCabang(@RequestBody AngketCabangDTO angketCabangDTO, @PathVariable UUID id) {
        try {
            return ResponseHandler.generateResponse("Data Successfully Updated!", HttpStatus.OK, HttpStatus.OK,
                    angketCabangService.updateAngketCabang(angketCabangDTO, id));
        } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), e.getStatusCode(), e.getStatusCode(), null);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,
                    HttpStatus.INTERNAL_SERVER_ERROR, null);
        }
    }

    @DeleteMapping("{id}")
    @Operation(
            summary = "Delete Angket Cabang",
            description = "Delete Angket Cabang."
    )
    @ApiResponses(value = {
            @ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            KeadaanJamaah.class))
            ),
            @ApiResponse(
                    description = "Not found",
                    responseCode = "404",
                    content = @Content
            ),
            @ApiResponse(
                    description = "Internal error",
                    responseCode = "500",
                    content = @Content
            )
    })
    public ResponseEntity<Object> deleteAngketCabang(@PathVariable UUID id) {
        try {
            return ResponseHandler.generateResponse("Data Successfully Deleted!", HttpStatus.OK, HttpStatus.OK,
                    angketCabangService.deleteAngketCabangById(id));
        } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), e.getStatusCode(), e.getStatusCode(), null);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,
                    HttpStatus.INTERNAL_SERVER_ERROR, null);
        }
    }
}
