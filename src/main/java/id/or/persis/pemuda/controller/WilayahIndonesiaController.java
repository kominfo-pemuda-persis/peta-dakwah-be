package id.or.persis.pemuda.controller;

import id.or.persis.pemuda.dto.ResponseHandler;
import id.or.persis.pemuda.entity.Kecamatan;
import id.or.persis.pemuda.entity.Kelurahan;
import id.or.persis.pemuda.entity.Kota;
import id.or.persis.pemuda.entity.Provinsi;
import id.or.persis.pemuda.repository.KecamatanRepository;
import id.or.persis.pemuda.repository.KelurahanRepository;
import id.or.persis.pemuda.repository.KotaRepository;
import id.or.persis.pemuda.repository.ProvinsiRepository;
import id.or.persis.pemuda.specification.WilayahSpecification;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springdoc.core.annotations.ParameterObject;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-wilayah-indonesia
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 06/04/22
 * Time: 07.16
 */
@RestController
@RequiredArgsConstructor
@Tag(name = "Wilayah Indonesia", description = "Endpoints for managing all Indonesia Regions")
@SecurityRequirement(name = "Bearer Authentication")
public class WilayahIndonesiaController {
    private static final String RETRIEVED = "Successfully retrieved data!";
    private final ProvinsiRepository provinsiRepository;

    private final KotaRepository kotaRepository;

    private final KecamatanRepository kecamatanRepository;

    private final KelurahanRepository kelurahanRepository;

    @GetMapping("/api/provinces")
    @Operation(
            summary = "List All Indonesia Provinces",
            description = "List All Indonesia Provinces.",
            tags = {"Wilayah Indonesia"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            Provinsi.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    @PageableAsQueryParam
    public ResponseEntity<Object> getAllProvinces(
            @ParameterObject
            @PageableDefault(sort = {"id"}, direction = Sort.Direction.ASC, value = 5) Pageable pageable,
            @Parameter(description = "keyword Kode Provinsi or Name Provinsi")
			@RequestParam(required = false, defaultValue = "") String keyword) {
        try {
            return ResponseHandler.generateResponse(RETRIEVED, HttpStatus.OK, HttpStatus.OK,
                    provinsiRepository.findAll(
                            WilayahSpecification.hasKodeProvinsi(keyword).or(
                                    WilayahSpecification.hasNamaProvinsi(keyword)), pageable));
        } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR,
                    null);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR,
                    null);
        }
    }

    @GetMapping("/api/districts")
    @Operation(
            summary = "List All Indonesia Districts",
            description = "List All Indonesia Districts.",
            tags = {"Wilayah Indonesia"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            Kota.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public ResponseEntity<Object> getAllDistricts(
            @ParameterObject 
			@PageableDefault(sort = {"id"}, direction = Sort.Direction.ASC, value = 5) Pageable pageable,
            @Parameter(description = "keyword Kode Kota/Kabupaten or Name Kota/Kabupaten") 
			@RequestParam(required = false, defaultValue = "") String keyword,
            @Parameter(description = "kode provinsi to get Kota/Kabupaten by specific provinsi") 
			@RequestParam(required = false, defaultValue = "", name = "kode-provinsi") String kodeProvinsi) {
        try {
			Specification<Kota> spec = WilayahSpecification.hasNamaKota(keyword)
				.or(WilayahSpecification.hasKodeKota(keyword));

            if (StringUtils.isNotBlank(kodeProvinsi)) {
               spec = spec.and(WilayahSpecification.hasProvince(kodeProvinsi));
            }
            return ResponseHandler.generateResponse(RETRIEVED,
                    HttpStatus.OK, HttpStatus.OK,
                    kotaRepository.findAll(spec, pageable));
        } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR,
                    null);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR,
                    null);
        }
    }

    @GetMapping("/api/sub-districts")
    @Operation(
            summary = "List All Indonesia Sub Districts",
            description = "List All Indonesia Sub Districts.",
            tags = {"Wilayah Indonesia"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            Kecamatan.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public ResponseEntity<Object> getAllSubDistricts(
            @ParameterObject 
			@PageableDefault(sort = {"id"}, direction = Sort.Direction.ASC, value = 5) Pageable pageable,
                        @Parameter(description = "keyword Kode Kecamatan or Name Kecamatan") 
			@RequestParam(required = false, defaultValue = "") String keyword,
			@Parameter(description = "kode kota to get Kecamatan by specific kota") 
			@RequestParam(required = false, defaultValue = "", name = "kode-kota") String kodeKota) {
        try {
			Specification<Kecamatan> spec = WilayahSpecification.hasNamaKecamatan(keyword)
				.or(WilayahSpecification.hasKodeKecamatan(keyword));

            if (StringUtils.isNotBlank(kodeKota)) {
               spec = spec.and(WilayahSpecification.hasKota(kodeKota));
            }
            return ResponseHandler.generateResponse(RETRIEVED, HttpStatus.OK, HttpStatus.OK,
                    kecamatanRepository.findAll(spec, pageable));
        } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR,
                    null);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR,
                    null);
        }
    }

    @GetMapping("/api/villages")
    @Operation(
            summary = "List All Indonesia Villages",
            description = "List All Indonesia Villages.",
            tags = {"Wilayah Indonesia"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            Kelurahan.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public ResponseEntity<Object> getAllVillages(
            @ParameterObject
			@PageableDefault(sort = {"id"}, direction = Sort.Direction.ASC, value = 5) Pageable pageable,
            @Parameter(description = "keyword Kode Kelurahan or Name Kelurahan") 
			@RequestParam(required = false, defaultValue = "") String keyword,
			@Parameter(description = "kode Kelurahan to get Kelurahan by specific Kecamatan") 
			@RequestParam(required = false, defaultValue = "", name = "kode-kecamatan") String kodeKecamatan) {
        try {
			Specification<Kelurahan> spec = WilayahSpecification.hasNamaKelurahan(keyword)
				.or(WilayahSpecification.hasKodeKelurahan(keyword));

            if (StringUtils.isNotBlank(kodeKecamatan)) {
               spec = spec.and(WilayahSpecification.hasKecamatan(kodeKecamatan));
            }
            return ResponseHandler.generateResponse(RETRIEVED, HttpStatus.OK, HttpStatus.OK,
                    kelurahanRepository.findAll(spec, pageable));
        } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR,
                    null);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR,
                    null);
        }
    }
}
