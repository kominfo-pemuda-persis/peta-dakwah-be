package id.or.persis.pemuda.controller;

import id.or.persis.pemuda.dto.ProfilMuballighDTO;
import id.or.persis.pemuda.dto.ResponseHandler;
import id.or.persis.pemuda.entity.KeadaanPendidikan;
import id.or.persis.pemuda.entity.ProfilMuballigh;
import id.or.persis.pemuda.service.ProfilMuballighService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
@Tag(
        name = "Profil Muballigh",
        description = "Endpoints to manage Profil Muballigh"
)
@SecurityRequirement(name = "Bearer Authentication")
public class ProfilMuballighController {

    private static final String SUCCESSFULLY_RETRIEVED_DATA = "Successfully retrieved data!";

    private final ProfilMuballighService profilMuballighService;

    @PostMapping("/profil-muballigh/")
    @Operation(
            summary = "Add New Profil Muballigh",
            description = "Add New Profil Muballigh.",
            tags = {"Profil Muballigh"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            ProfilMuballighDTO.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not Authorized", responseCode = "401",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Forbidden", responseCode = "403",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public ResponseEntity<Object> saveNewProfil(@RequestBody ProfilMuballighDTO muballigh) {
        try {
                return ResponseHandler.generateResponse(SUCCESSFULLY_RETRIEVED_DATA, HttpStatus.OK, HttpStatus.OK,
                        profilMuballighService.saveProfilMuballigh(muballigh));
              } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), e.getStatusCode(), e.getStatusCode(), null);
              } catch (Exception e) {
                  return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,
                          HttpStatus.INTERNAL_SERVER_ERROR, null);
          }
    }

    @GetMapping("/profil-muballigh")
    @Operation(
            summary = "List Profil Muballigh",
            description = "List Profil Muballigh.",
            tags = {"Profil Muballigh"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            ProfilMuballigh.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not Authorized", responseCode = "401",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Forbidden", responseCode = "403",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public ResponseEntity<Object> getAllProfilMuballigh(
            @ParameterObject @PageableDefault(sort = {
                    "createdAt"}, direction = Sort.Direction.ASC, value = 5) Pageable pageable,
            @Parameter(description = "Soft delete data status") @RequestParam(required = false, defaultValue = "false") String deleted,
            @Parameter(description = "Keyword for filter data") @RequestParam(required = false, defaultValue = "") String keyword,
            @Parameter(description = "Get Specific data or all data (deleted and active data)") @RequestParam(required = false, defaultValue = "SPECIFIC") String scope
    ) {
        try {
              Specification<ProfilMuballigh> spec = profilMuballighService.validateSpecification(Boolean.parseBoolean(deleted), scope);
            spec = profilMuballighService.addKeywordSpecification(spec, keyword);
              return ResponseHandler.generateResponse(SUCCESSFULLY_RETRIEVED_DATA, HttpStatus.OK, HttpStatus.OK,
                profilMuballighService.getAllProfilMuballigh(spec, pageable));
            } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), e.getStatusCode(), e.getStatusCode(), null);
            } catch (Exception e) {
                return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,
                        HttpStatus.INTERNAL_SERVER_ERROR, null);
        }
    }

    @GetMapping("/profil-muballigh/{UUID}")
    @Operation(
            summary = "Get Profil Muballigh by ID",
            description = "Get Profil Muballigh by ID",
            tags = {"Profil Muballigh"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            KeadaanPendidikan.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not Authorized", responseCode = "401",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Forbidden", responseCode = "403",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public ResponseEntity<Object> findById(@PathVariable("UUID") UUID id) {
        try {
                return ResponseHandler.generateResponse(SUCCESSFULLY_RETRIEVED_DATA, HttpStatus.OK, HttpStatus.OK,
                  profilMuballighService.getByProfilMuballighById(id));
              } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), e.getStatusCode(), e.getStatusCode(), null);
              } catch (Exception e) {
                  return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,
                          HttpStatus.INTERNAL_SERVER_ERROR, null);
          }
    }

    @PutMapping("/profil-muballigh/{UUID}")
    @Operation(
            summary = "Update Profil Muballigh",
            description = "Update Profil Muballigh",
            tags = {"Profil Muballigh"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            ProfilMuballigh.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not Authorized", responseCode = "401",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Forbidden", responseCode = "403",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public ResponseEntity<Object> update(@RequestBody ProfilMuballighDTO profilMuballigh, @RequestParam UUID id) {
        try {
                return ResponseHandler.generateResponse(SUCCESSFULLY_RETRIEVED_DATA, HttpStatus.OK, HttpStatus.OK,
                  profilMuballighService.updateProfilMuballigh(profilMuballigh, id));
              } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), e.getStatusCode(), e.getStatusCode(), null);
              } catch (Exception e) {
                  return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,
                          HttpStatus.INTERNAL_SERVER_ERROR, null);
          }
    }

    @DeleteMapping("/profil-muballigh/{id}")
    @Operation(
            summary = "Delete Profil Muballigh",
            description = "Delete Profil Muballigh",
            tags = {"Profil Muballigh"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            KeadaanPendidikan.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not Authorized", responseCode = "401",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Forbidden", responseCode = "403",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public ResponseEntity<Object> delete(@PathVariable UUID id) {
        try {
                return ResponseHandler.generateResponse(SUCCESSFULLY_RETRIEVED_DATA, HttpStatus.OK, HttpStatus.OK,
                  profilMuballighService.deleteProfilMuballighById(id));
              } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), e.getStatusCode(), e.getStatusCode(), null);
              } catch (Exception e) {
                  return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,
                          HttpStatus.INTERNAL_SERVER_ERROR, null);
          }
    }
}
