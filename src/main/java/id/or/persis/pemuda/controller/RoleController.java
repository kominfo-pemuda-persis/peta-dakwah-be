package id.or.persis.pemuda.controller;

import id.or.persis.pemuda.dto.ResponseHandler;
import id.or.persis.pemuda.dto.RoleDTO;
import id.or.persis.pemuda.entity.Role;
import id.or.persis.pemuda.service.RoleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@RestController
@RequestMapping("/api/roles")
@RequiredArgsConstructor
@Slf4j
@Tag(name = "Role", description = "Endpoints to manage Data Role")
@SecurityRequirement(name = "Bearer Authentication")
public class RoleController {

    private static final String SUCCESS = "SUCCESS";
    private final RoleService roleService;

    @GetMapping
    @Operation(
            summary = "List All Roles Data",
            description = "List All Roles Data."
    )
    @ApiResponses(value = {
            @ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            RoleDTO.class))
            ),
            @ApiResponse(
                    description = "Not found",
                    responseCode = "404",
                    content = @Content
            ),
            @ApiResponse(
                    description = "Internal error",
                    responseCode = "500",
                    content = @Content
            )
    })
    public ResponseEntity<Object> getAllRoles() {
        List<Role> roleList = roleService.getAll();
        return ResponseHandler.generateResponse(SUCCESS, HttpStatus.OK, HttpStatus.OK, roleList);
    }

    @GetMapping(value = "/{id}")
    @Operation(
            summary = "Get Role Data by ID",
            description = "Get Role Data by ID."
    )
    @ApiResponses(value = {
            @ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            RoleDTO.class))
            ),
            @ApiResponse(
                    description = "Not found",
                    responseCode = "404",
                    content = @Content
            ),
            @ApiResponse(
                    description = "Internal error",
                    responseCode = "500",
                    content = @Content
            )
    })
    public ResponseEntity<Object> getRoleById(@PathVariable UUID id) {
        Role role = roleService.findById(id);
        return ResponseHandler.generateResponse(SUCCESS, HttpStatus.OK, HttpStatus.OK, role);
    }

    @PostMapping
    @Operation(
            summary = "Add New Role Data",
            description = "Add New Role Data."
    )
    @ApiResponses(value = {
            @ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            RoleDTO.class))
            ),
            @ApiResponse(
                    description = "Not found",
                    responseCode = "404",
                    content = @Content
            ),
            @ApiResponse(
                    description = "Internal error",
                    responseCode = "500",
                    content = @Content
            )
    })
    public ResponseEntity<Object> addRole(@RequestBody RoleDTO dto) {
        String validation = roleService.validateDTO(dto);
        if (Objects.nonNull(validation)) {
            return ResponseHandler.generateResponse(validation, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST, null);
        }
        Role role = roleService.save(dto);
        return ResponseHandler.generateResponse(SUCCESS, HttpStatus.OK, HttpStatus.OK, role);
    }

    @PutMapping(value = "/{id}")
    @Operation(
            summary = "Update Role Data by ID",
            description = "Update Role Data by ID."
    )
    @ApiResponses(value = {
            @ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            RoleDTO.class))
            ),
            @ApiResponse(
                    description = "Not found",
                    responseCode = "404",
                    content = @Content
            ),
            @ApiResponse(
                    description = "Internal error",
                    responseCode = "500",
                    content = @Content
            )
    })
    public ResponseEntity<Object> updateRole(@PathVariable UUID id, @Valid @RequestBody RoleDTO dto) {
        String validation = roleService.validateDTO(dto);
        if (Objects.nonNull(validation)) {
            return ResponseHandler.generateResponse(validation, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST, null);
        }
        Role role = roleService.update(dto, id);
        return ResponseHandler.generateResponse(SUCCESS, HttpStatus.OK, HttpStatus.OK, role);
    }

    @DeleteMapping(value = "/{id}")
    @Operation(
            summary = "Delete Role Data by ID",
            description = "Delete Role Data by ID."
    )
    @ApiResponses(value = {
            @ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            RoleDTO.class))
            ),
            @ApiResponse(
                    description = "Not found",
                    responseCode = "404",
                    content = @Content
            ),
            @ApiResponse(
                    description = "Internal error",
                    responseCode = "500",
                    content = @Content
            )
    })
    public ResponseEntity<Object> deleteStudent(@PathVariable UUID id) {
        Role role = roleService.findById(id);
        roleService.deleteById(role.getId());
        return ResponseHandler.generateResponse(SUCCESS, HttpStatus.OK, HttpStatus.OK, null);
    }
}
