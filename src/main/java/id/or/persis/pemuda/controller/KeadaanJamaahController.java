package id.or.persis.pemuda.controller;

import id.or.persis.pemuda.dto.KeadaanJamaahDTO;
import id.or.persis.pemuda.dto.ResponseHandler;
import id.or.persis.pemuda.entity.KeadaanJamaah;
import id.or.persis.pemuda.service.KeadaanJamaahService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/keadaan-jamaah")
@Tag(name = "Keadaan Jamaah", description = "Endpoints to manage Keadaan Jamaah")
@SecurityRequirement(name = "Bearer Authentication")
public class KeadaanJamaahController {
    private final KeadaanJamaahService keadaanJamaahService;

    @GetMapping
    @Operation(
            summary = "List All Keadaan Jamaah",
            description = "List All Keadaan Jamaah."
    )
    @ApiResponses(value = {
            @ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            KeadaanJamaah.class))
            ),
            @ApiResponse(
                    description = "Not found",
                    responseCode = "404",
                    content = @Content
            ),
            @ApiResponse(
                    description = "Internal error",
                    responseCode = "500",
                    content = @Content
            )
    })
    public ResponseEntity<Object> getAllKeadaanJamaah(
        @ParameterObject @PageableDefault(sort = {
                "createdAt" }, direction = Sort.Direction.ASC, value = 5) Pageable pageable,
        @Parameter(description = "Soft delete data status") @RequestParam(required = false, defaultValue = "false") String deleted,
        @Parameter(description = "Keyword for filter data") @RequestParam(required = false, defaultValue = "") String keyword,
        @Parameter(description = "Get Specific data or all data (deleted and active data)") @RequestParam(required = false, defaultValue = "SPECIFIC") String scope
    ) {
        try {
            Specification<KeadaanJamaah> specification = keadaanJamaahService
                .validateSpecification(Boolean.parseBoolean(deleted), scope);
            specification = keadaanJamaahService.addKeywordSpecification(specification, keyword);
            return ResponseHandler.generateResponse("Successfully retrieved data!", HttpStatus.OK, HttpStatus.OK,
                    keadaanJamaahService.getAllKeadaanJamaah(specification, pageable));
        } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR, null);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,
                    HttpStatus.INTERNAL_SERVER_ERROR, null);
        }

    }

    @GetMapping("{id}")
    @Operation(
            summary = "Get Keadaan Jamaah by Id",
            description = "Get Keadaan Jamaah by Id."
    )
    @ApiResponses(value = {
            @ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            KeadaanJamaah.class))
            ),
            @ApiResponse(
                    description = "Not found",
                    responseCode = "404",
                    content = @Content
            ),
            @ApiResponse(
                    description = "Internal error",
                    responseCode = "500",
                    content = @Content
            )
    })
    public ResponseEntity<Object> getKeadaanJamaahById(@PathVariable UUID id) {
        try {
            return ResponseHandler.generateResponse("Successfully retrieved data!", HttpStatus.OK, HttpStatus.OK,
                    keadaanJamaahService.getKeadaanJamaahById(id));
        } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR, null);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,
                    HttpStatus.INTERNAL_SERVER_ERROR, null);
        }
    }

    @PostMapping
    @Operation(
            summary = "Add new Keadaan Jamaah",
            description = "Add new Keadaan Jamaah."
    )
    @ApiResponses(value = {
            @ApiResponse(
                    description = "Success",
                    responseCode = "201",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            KeadaanJamaah.class))
            ),
            @ApiResponse(
                    description = "Not found",
                    responseCode = "404",
                    content = @Content
            ),
            @ApiResponse(
                    description = "Internal error",
                    responseCode = "500",
                    content = @Content
            )
    })
    public ResponseEntity<Object> saveKeadaanJamaah(@RequestBody KeadaanJamaahDTO keadaanJamaahDTO) {
        try {
            return ResponseHandler.generateResponse("Data successfully saved!", HttpStatus.CREATED,
                    HttpStatus.CREATED, keadaanJamaahService.saveKeadaanJamaah(keadaanJamaahDTO));
        } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR, null);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,
                    HttpStatus.INTERNAL_SERVER_ERROR, null);
        }
    }

    @PutMapping("{id}")
    @Operation(
            summary = "Update Keadaan Jamaah",
            description = "Update Keadaan Jamaah."
    )
    @ApiResponses(value = {
            @ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            KeadaanJamaah.class))
            ),
            @ApiResponse(
                    description = "Not found",
                    responseCode = "404",
                    content = @Content
            ),
            @ApiResponse(
                    description = "Internal error",
                    responseCode = "500",
                    content = @Content
            )
    })
    public ResponseEntity<Object> updateKeadaanJamaah(@RequestBody KeadaanJamaahDTO keadaanJamaahDTO, @PathVariable UUID id) {
        try {
            return ResponseHandler.generateResponse("Data Successfully Updated!", HttpStatus.OK, HttpStatus.OK,
                    keadaanJamaahService.updateKeadaanJamaah(keadaanJamaahDTO, id));
        } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR, null);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,
                    HttpStatus.INTERNAL_SERVER_ERROR, null);
        }
    }

    @DeleteMapping("{id}")
    @Operation(
            summary = "Delete Keadaan Jamaah",
            description = "Delete Keadaan Jamaah."
    )
    @ApiResponses(value = {
            @ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            KeadaanJamaah.class))
            ),
            @ApiResponse(
                    description = "Not found",
                    responseCode = "404",
                    content = @Content
            ),
            @ApiResponse(
                    description = "Internal error",
                    responseCode = "500",
                    content = @Content
            )
    })
    public ResponseEntity<Object> deleteKeadaanJamaah(@PathVariable UUID id) {
        try {
            return ResponseHandler.generateResponse("Data Successfully Deleted!", HttpStatus.OK, HttpStatus.OK,
                    keadaanJamaahService.deleteKeadaanJamaahById(id));
        } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR, null);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,
                    HttpStatus.INTERNAL_SERVER_ERROR, null);
        }
    }
}
