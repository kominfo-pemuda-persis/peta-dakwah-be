package id.or.persis.pemuda.controller;

import id.or.persis.pemuda.dto.ResponseHandler;
import id.or.persis.pemuda.entity.PimpinanDaerah;
import id.or.persis.pemuda.repository.PimpinanDaerahRepository;
import id.or.persis.pemuda.specification.PimpinanDaerahSpecification;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequiredArgsConstructor
@Tag(name = "Pimpinan Daerah", description = "Endpoint for list all local-headquarter")
@SecurityRequirement(name = "Bearer Authentication")
public class PimpinanDaerahController {
    private final PimpinanDaerahRepository pimpinanDaerahRepository;

    @GetMapping("/api/pd")
    @Operation(
            summary = "List All Local-Headquarter",
            description = "List All Local-Headquarter."
    )
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            PimpinanDaerah.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Not found",
                    responseCode = "404",
                    content = @Content
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Internal Error",
                    responseCode = "500",
                    content = @Content
            )
    })

    public ResponseEntity<Object> getAllPimpinanDaerah(
            @ParameterObject @PageableDefault(sort = {"id"}, direction = Sort.Direction.ASC, value = 5) Pageable pageable,
            @Parameter(description = "keyword Kode PD or Name PD") @RequestParam(required = false, defaultValue = "") String keyword,
            @Parameter(description = "keyword Kode PW to search only specific PW") @RequestParam(required = false, defaultValue = "", name = "kode-pw") String kodePw) {
        try {
                
            Specification<PimpinanDaerah> spec = PimpinanDaerahSpecification.hasKodePd(keyword)
				.or(PimpinanDaerahSpecification.hasNamaPd(keyword));

            if (StringUtils.isNotBlank(kodePw)) {
                spec = spec.and(PimpinanDaerahSpecification.hasKodePW(kodePw));
            }

            return ResponseHandler.generateResponse("Successfully retrieved data!", HttpStatus.OK, HttpStatus.OK,
                    pimpinanDaerahRepository.findAll(spec, pageable));
        } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), e.getStatusCode(), e.getStatusCode(), null);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,
                    HttpStatus.INTERNAL_SERVER_ERROR, null);
        }
    }
}
