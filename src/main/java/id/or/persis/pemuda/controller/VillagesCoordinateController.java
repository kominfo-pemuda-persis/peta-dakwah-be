package id.or.persis.pemuda.controller;

import id.or.persis.pemuda.dto.ResponseHandler;
import id.or.persis.pemuda.dto.VillagesCoordinateDTO;
import id.or.persis.pemuda.entity.CitiesCoordinate;
import id.or.persis.pemuda.entity.VillagesCoordinate;
import id.or.persis.pemuda.service.VillagesCoordinateService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

/**
 * Created by IntelliJ IDEA.
 * Project : peta-dakwah-be
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/5/22
 * Time: 17:33
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
@Tag(
        name = "Villages Coordinate",
        description = "Endpoints to manage Villages Coordinate"
)
@SecurityRequirement(name = "Bearer Authentication")
public class VillagesCoordinateController {
    private static final String SAVED_DATA = "Successfully saved data!";
    private static final String RETRIEVED = "Successfully retrieved data!";
    private final VillagesCoordinateService villagesCoordinateService;

    @PostMapping("/villages-coordinate/")
    @Operation(
            summary = "Add New Village Coordinate",
            description = "Add New Village Coordinate.",
            tags = {"Village Coordinate"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            VillagesCoordinate.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not Authorized", responseCode = "401",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Forbidden", responseCode = "403",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public ResponseEntity<Object> saveNewVillage(@RequestBody VillagesCoordinateDTO coordinateDto) {
        try {
            return ResponseHandler.generateResponse(SAVED_DATA, HttpStatus.OK, HttpStatus.OK,
                    villagesCoordinateService.create(coordinateDto));
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, HttpStatus.MULTI_STATUS,
                    null);
        }
    }

    @GetMapping("/villages-coordinate")
    @Operation(
            summary = "List Villages Coordinate",
            description = "List Villages Coordinate.",
            tags = {"Villages Coordinate"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            CitiesCoordinate.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not Authorized", responseCode = "401",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Forbidden", responseCode = "403",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public ResponseEntity<Object> getAllVillages(@ParameterObject @PageableDefault(sort = {
            "kode"}, direction = Sort.Direction.ASC, value = 5) Pageable pageable,
                                                 @Parameter(description = "Kode for coordinate area") @RequestParam(required = false, defaultValue = "") String kode,
                                                 @Parameter(description = "nama kota") @RequestParam(required = false, defaultValue = "") String nama,
                                                 @Parameter(description = "nama ibukota") @RequestParam(required = false, defaultValue = "", name = "ibukota") String ibuKota) {
        try {
            return ResponseHandler.generateResponse(RETRIEVED, HttpStatus.OK, HttpStatus.OK,
                    villagesCoordinateService.getAll(kode, nama, ibuKota, pageable));
        } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR, null);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,
                    HttpStatus.INTERNAL_SERVER_ERROR, null);
        }
    }

    @PutMapping("/villages-coordinate/{id}")
    @Operation(
            summary = "Update Villages Coordinate",
            description = "Update Villages Coordinate",
            tags = {"Villages Coordinate"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            VillagesCoordinate.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not Authorized", responseCode = "401",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Forbidden", responseCode = "403",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public ResponseEntity<Object> updateVillage(@RequestBody VillagesCoordinateDTO coordinateDto, @PathVariable String id) {
        try {
            return ResponseHandler.generateResponse("Successfully updated data!", HttpStatus.OK, HttpStatus.OK,
                    villagesCoordinateService.update(coordinateDto, id));
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, HttpStatus.MULTI_STATUS,
                    null);
        }
    }

    @DeleteMapping("/villages-coordinate/{id}")
    @Operation(
            summary = "Delete Village Coordinate",
            description = "Delete Village Coordinate",
            tags = {"Villages Coordinate"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            VillagesCoordinate.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not Authorized", responseCode = "401",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Forbidden", responseCode = "403",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public ResponseEntity<Object> deleteVillageCoordinateById(@PathVariable String id) {
        try {
            return ResponseHandler.generateResponse("Data Successfully Deleted!", HttpStatus.OK, HttpStatus.OK,
                    villagesCoordinateService.deleteVillagesCoordinateById(id));
        } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR, null);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,
                    HttpStatus.INTERNAL_SERVER_ERROR, null);
        }
    }

}
