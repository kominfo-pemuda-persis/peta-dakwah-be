package id.or.persis.pemuda.mapper;

import id.or.persis.pemuda.dto.AngketCabangDTO;
import id.or.persis.pemuda.entity.AngketCabang;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AngketCabangMapper {
    AngketCabangDTO toDto(final AngketCabang angketCabang);

    List<AngketCabangDTO> toDtos(final List<AngketCabang> angketCabang);

    @InheritInverseConfiguration
    AngketCabang toEntity(final AngketCabangDTO angketCabangDTO);
}
