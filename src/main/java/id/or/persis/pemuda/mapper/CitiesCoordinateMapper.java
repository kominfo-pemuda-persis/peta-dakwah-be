package id.or.persis.pemuda.mapper;

import id.or.persis.pemuda.dto.CitiesCoordinateDto;
import id.or.persis.pemuda.entity.CitiesCoordinate;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CitiesCoordinateMapper {

    CitiesCoordinateDto toDto(CitiesCoordinate coordinate);

    @InheritInverseConfiguration
    CitiesCoordinate toEntity(CitiesCoordinateDto coordinateDto);

    List<CitiesCoordinateDto> toDtos(List<CitiesCoordinate> coordinates);
}
