package id.or.persis.pemuda.mapper;

import id.or.persis.pemuda.dto.StudentDTO;
import id.or.persis.pemuda.entity.Student;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : peta-dakwah-be
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/21/22
 * Time: 00:02
 * To change this template use File | Settings | File Templates.
 */
@Mapper(componentModel = "spring")
public interface StudentMapper {
    StudentDTO toDto(final Student student);

    List<StudentDTO> toDtos(final List<Student> studentList);

    @InheritInverseConfiguration
    Student studentDtoToStudent(final StudentDTO studentDTO);
}
