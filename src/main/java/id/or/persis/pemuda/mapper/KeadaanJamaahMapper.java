package id.or.persis.pemuda.mapper;

import id.or.persis.pemuda.dto.KeadaanJamaahDTO;
import id.or.persis.pemuda.entity.KeadaanJamaah;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : peta-dakwah-be
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/22/22
 * Time: 14:45
 * To change this template use File | Settings | File Templates.
 */
@Mapper(componentModel = "spring")
public interface KeadaanJamaahMapper {
    KeadaanJamaahDTO toDto(final KeadaanJamaah keadaanJamaah);

    List<KeadaanJamaahDTO> toDtos(final List<KeadaanJamaah> keadaanJamaahList);

    @InheritInverseConfiguration
    KeadaanJamaah toEntity(final KeadaanJamaahDTO keadaanJamaahDTO);
}
