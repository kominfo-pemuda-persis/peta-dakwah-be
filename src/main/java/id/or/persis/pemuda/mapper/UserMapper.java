package id.or.persis.pemuda.mapper;

import id.or.persis.pemuda.dto.GetUserDTO;
import id.or.persis.pemuda.dto.UserDTO;
import id.or.persis.pemuda.entity.User;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : peta-dakwah-be
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/22/22
 * Time: 22:32
 * To change this template use File | Settings | File Templates.
 */
@Mapper(componentModel = "spring")
public interface UserMapper {
    GetUserDTO toDto(final User user);

    @InheritInverseConfiguration
    User toEntity(final UserDTO userDTO);

    List<GetUserDTO> toDtos(final List<User> userList);
}
