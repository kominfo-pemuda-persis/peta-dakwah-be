package id.or.persis.pemuda.mapper;

import id.or.persis.pemuda.dto.VillagesCoordinateDTO;
import id.or.persis.pemuda.entity.VillagesCoordinate;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : peta-dakwah-be
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/5/22
 * Time: 17:31
 * To change this template use File | Settings | File Templates.
 */
@Mapper(componentModel = "spring")
public interface VillagesCoordinateMapper {
    VillagesCoordinateDTO toDTO(final VillagesCoordinate villagesCoordinate);

    @InheritInverseConfiguration
    VillagesCoordinate toEntity(final VillagesCoordinateDTO villagesCoordinateDTO);

    List<VillagesCoordinateDTO> toDTOs(final List<VillagesCoordinate> coordinateList);
}
