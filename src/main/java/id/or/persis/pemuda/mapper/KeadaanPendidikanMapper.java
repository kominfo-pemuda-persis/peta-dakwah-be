package id.or.persis.pemuda.mapper;

import id.or.persis.pemuda.dto.KeadaanPendidikanDTO;
import id.or.persis.pemuda.entity.KeadaanPendidikan;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

/**
 * Created by IntelliJ IDEA.
 * Project : peta-dakwah-be
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/21/22
 * Time: 20:15
 * To change this template use File | Settings | File Templates.
 */
@Mapper(componentModel = "spring")
public interface KeadaanPendidikanMapper {
    KeadaanPendidikanDTO toDto(final KeadaanPendidikan keadaanPendidikan);

    @InheritInverseConfiguration
    KeadaanPendidikan toEntity(final KeadaanPendidikanDTO keadaanPendidikanDTO);
}
