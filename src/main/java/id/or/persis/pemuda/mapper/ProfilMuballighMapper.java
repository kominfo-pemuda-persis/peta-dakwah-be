package id.or.persis.pemuda.mapper;

import id.or.persis.pemuda.dto.ProfilMuballighDTO;
import id.or.persis.pemuda.entity.ProfilMuballigh;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : peta-dakwah-be
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/19/22
 * Time: 20:48
 * To change this template use File | Settings | File Templates.
 */
@Mapper(componentModel = "spring")
public interface ProfilMuballighMapper {
    ProfilMuballighDTO toDto(final ProfilMuballigh profilMuballigh);

    List<ProfilMuballighDTO> toDtos(final List<ProfilMuballigh> profilMuballighList);

    @InheritInverseConfiguration
    ProfilMuballigh toEntity(final ProfilMuballighDTO profilMuballighDTO);
}
