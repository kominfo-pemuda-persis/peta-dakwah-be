package id.or.persis.pemuda.repository;

import id.or.persis.pemuda.entity.CitiesCoordinate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CitiesCoordinateRepository extends JpaRepository<CitiesCoordinate, String>, JpaSpecificationExecutor<CitiesCoordinate> {
}
