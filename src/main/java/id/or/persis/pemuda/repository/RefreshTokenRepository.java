package id.or.persis.pemuda.repository;

import id.or.persis.pemuda.entity.RefreshToken;
import id.or.persis.pemuda.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

import java.util.Optional;
import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * Project : peta-dakwah-be
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 1/15/23
 * Time: 19:05
 * To change this template use File | Settings | File Templates.
 */
public interface RefreshTokenRepository extends JpaRepository<RefreshToken, UUID> {
    @Override
    Optional<RefreshToken> findById(UUID id);

    Optional<RefreshToken> findByToken(String token);

    @Modifying
    int deleteByUser(User user);
}
