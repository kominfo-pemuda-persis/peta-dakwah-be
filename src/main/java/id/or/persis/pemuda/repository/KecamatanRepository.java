package id.or.persis.pemuda.repository;

import id.or.persis.pemuda.entity.Kecamatan;
import id.or.persis.pemuda.entity.Kota;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-wilayah-indonesia
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 06/04/22
 * Time: 07.14
 */
public interface KecamatanRepository extends JpaRepository<Kecamatan, String>, JpaSpecificationExecutor<Kecamatan> {
    Iterable<Kecamatan> findByKota(Kota kota);
}
