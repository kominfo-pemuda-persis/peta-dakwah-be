package id.or.persis.pemuda.repository;

import java.util.UUID;

import id.or.persis.pemuda.entity.ProfilMuballigh;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfilMuballighRepository extends JpaRepository<ProfilMuballigh, UUID>, JpaSpecificationExecutor<ProfilMuballigh> {
    int countByPcKdPc(String kdPc);

    @Query("""
        select count(pm.id) from ProfilMuballigh pm \
        where pm.pc.pd.kdPd = ?1
        """)
    int countByKdPd(String kdPd);

    @Query("""
        select count(pm.id) from ProfilMuballigh pm \
        where pm.pc.pd.pw.kdPw = ?1
        """)
    int countByKdPw(String kdPw);
}
