package id.or.persis.pemuda.repository;

import id.or.persis.pemuda.entity.PimpinanCabang;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

public interface PimpinanCabangRepository extends JpaRepository<PimpinanCabang, Integer>, JpaSpecificationExecutor<PimpinanCabang> {
    List<PimpinanCabang> findAll();

    Optional<PimpinanCabang> findPimpinanCabangByKdPc(String kodePc);
}
