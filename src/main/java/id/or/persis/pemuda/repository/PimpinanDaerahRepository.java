package id.or.persis.pemuda.repository;

import id.or.persis.pemuda.entity.PimpinanDaerah;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

public interface PimpinanDaerahRepository extends JpaRepository<PimpinanDaerah, Integer>, JpaSpecificationExecutor<PimpinanDaerah> {
    List<PimpinanDaerah> findAll();

    Optional<PimpinanDaerah> findPimpinanDaerahByKdPd(String kodePd);
}
