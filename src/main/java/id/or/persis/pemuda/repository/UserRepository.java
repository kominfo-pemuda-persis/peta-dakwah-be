package id.or.persis.pemuda.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import id.or.persis.pemuda.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, UUID>, JpaSpecificationExecutor<User> {
    Optional<User> findByIdAndDeletedAtIsNull(UUID id);

    List<User> findAllByDeletedAtIsNull();

    Optional<User> findByUsername(String username);

    @Query("""
        select u.pc.kdPc from User u \
        where u.id = ?1 \
        """)
    Optional<String> findKdPcByUserId(UUID userId);

    @Query("""
        select u.pc.pd.kdPd from User u \
        where u.id = ?1 \
        """)
    Optional<String> findKdPdByUserId(UUID userId);

    @Query("""
        select u.pc.pd.pw.kdPw from User u \
        where u.id = ?1 \
        """)
    Optional<String> findKdPwByUserId(UUID userId);

}
