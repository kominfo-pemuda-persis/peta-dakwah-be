package id.or.persis.pemuda.repository;

import id.or.persis.pemuda.entity.Kota;
import id.or.persis.pemuda.entity.Provinsi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-wilayah-indonesia
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 06/04/22
 * Time: 07.14
 */
public interface KotaRepository extends JpaRepository<Kota, String>, JpaSpecificationExecutor<Kota> {
    Page<Kota> findAll(Pageable pageable);
    Optional<Kota> findByKode(String kode);
    Iterable<Kota> findByProvinsi(Provinsi provinsi);
}
