package id.or.persis.pemuda.repository;

import id.or.persis.pemuda.entity.VillagesCoordinate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Created by IntelliJ IDEA.
 * Project : peta-dakwah-be
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/5/22
 * Time: 17:28
 * To change this template use File | Settings | File Templates.
 */
public interface VillagesCoordinateRepository extends JpaRepository<VillagesCoordinate, String>, JpaSpecificationExecutor<VillagesCoordinate> {
}
