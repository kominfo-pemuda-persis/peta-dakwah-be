package id.or.persis.pemuda.repository;

import id.or.persis.pemuda.entity.PimpinanWilayah;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

public interface PimpinanWilayahRepository extends JpaRepository<PimpinanWilayah, Integer>, JpaSpecificationExecutor<PimpinanWilayah> {
    List<PimpinanWilayah> findAll();

    Optional<PimpinanWilayah> findPimpinanWilayahByKdPw(String kodePw);

}
