package id.or.persis.pemuda.repository;

import id.or.persis.pemuda.entity.Provinsi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-wilayah-indonesia
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 06/04/22
 * Time: 07.13
 */
public interface ProvinsiRepository extends JpaRepository<Provinsi, String>, JpaSpecificationExecutor<Provinsi> {
    Page<Provinsi> findAll(Pageable pageable);
}
