package id.or.persis.pemuda.repository;

import java.util.UUID;

import id.or.persis.pemuda.entity.KeadaanPendidikan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface KeadaanPendidikanRepository extends JpaRepository<KeadaanPendidikan, UUID>, JpaSpecificationExecutor<KeadaanPendidikan> {
    int countByPcKdPc(String kdPc);

    @Query("""
        select count(kp.id) from KeadaanPendidikan kp \
        where kp.pc.pd.kdPd = ?1 \
        """)
    int countByKdPd(String kdPd);

    @Query("""
        select count(kp.id) from KeadaanPendidikan kp \
        where kp.pc.pd.pw.kdPw = ?1 \
        """)
    int countByKdPw(String kdPw);
}
