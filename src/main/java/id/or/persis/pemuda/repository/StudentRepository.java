package id.or.persis.pemuda.repository;

import id.or.persis.pemuda.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * Project : student-crud-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/03/22
 * Time: 06.21
 */
public interface StudentRepository extends JpaRepository<Student, UUID> {

    // Query method
    Optional<Student> findByEmail(String email);
}
