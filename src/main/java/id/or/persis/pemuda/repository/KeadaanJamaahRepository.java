package id.or.persis.pemuda.repository;

import java.util.UUID;

import id.or.persis.pemuda.entity.KeadaanJamaah;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface KeadaanJamaahRepository
        extends JpaRepository<KeadaanJamaah, UUID>, JpaSpecificationExecutor<KeadaanJamaah> {
    int countByPcKdPc(String kdPc);

    @Query("""
        select count(kj.id) from KeadaanJamaah kj \
        where kj.pc.pd.kdPd = ?1 \
        """)
    int countByKdPd(String kdPd);

    @Query("""
        select count(kj.id) from KeadaanJamaah kj \
        where kj.pc.pd.pw.kdPw = ?1 \
        """)
    int countByKdPw(String kdPw);
}
