package id.or.persis.pemuda.repository;

import id.or.persis.pemuda.entity.Kecamatan;
import id.or.persis.pemuda.entity.Kelurahan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-wilayah-indonesia
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 06/04/22
 * Time: 07.15
 */
public interface KelurahanRepository extends JpaRepository<Kelurahan, String>, JpaSpecificationExecutor<Kelurahan> {
    Iterable<Kelurahan> findByKecamatan(Kecamatan kecamatan);
}
