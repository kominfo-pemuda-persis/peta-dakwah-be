package id.or.persis.pemuda.repository;

import java.util.UUID;

import id.or.persis.pemuda.entity.AngketCabang;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AngketCabangRepository extends JpaRepository<AngketCabang, UUID>, JpaSpecificationExecutor<AngketCabang> {
    int countByPcKdPc(String kdPc);

    @Query("""
        select count(ac.id) from AngketCabang ac \
        where ac.pc.pd.kdPd = ?1 \
        """)
    int countByKdPd(String kdPd);

    @Query("""
        select count(ac.id) from AngketCabang ac \
        where ac.pc.pd.pw.kdPw = ?1 \
        """)
    int countByKdPw(String kdPw);
}
