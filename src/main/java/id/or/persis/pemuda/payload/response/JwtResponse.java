package id.or.persis.pemuda.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;
import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * Project : peta-dakwah-be
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 1/16/23
 * Time: 08:00
 * To change this template use File | Settings | File Templates.
 */
@Data
public class JwtResponse {
    private UUID id;

    @JsonProperty("access_token")
    private String accessToken;

    private String type = "Bearer";
    @JsonProperty("refresh_token")
    private String refreshToken;
    private String username;
    private String email;
    private List<String> roles;

    public JwtResponse(String accessToken, String refreshToken, UUID id, String username, String email,
                       List<String> roles) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.id = id;
        this.username = username;
        this.email = email;
        this.roles = roles;
    }
}
