package id.or.persis.pemuda.payload.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * Project : peta-dakwah-be
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 1/16/23
 * Time: 08:02
 * To change this template use File | Settings | File Templates.
 */
@Data
public class TokenRefreshRequest {
    @NotBlank
    private String refreshToken;
}
