CREATE TABLE user
(
    id            varchar(36) PRIMARY KEY,
    npa           varchar(25) UNIQUE,
    kode_pc       varchar(10),
    FOREIGN KEY (kode_pc)
        REFERENCES pc (kd_pc)
        ON DELETE CASCADE ON UPDATE CASCADE,
    kelurahan_id  varchar(13),
    FOREIGN KEY (kelurahan_id)
        REFERENCES kelurahan (id)
        ON DELETE CASCADE ON UPDATE CASCADE,
    username      varchar(50) UNIQUE,
    nama_lengkap  varchar(100),
    jenis_kelamin enum ('L', 'P'),
    tempat_lahir  varchar(50),
    tanggal_lahir date,
    active        boolean NOT NULL,
    otonom        enum( 'PERSIS', 'PERSISTRI', 'PEMUDA', 'PEMUDI'),
    password      varchar(255),
    email         varchar(100) UNIQUE,
    photo         varchar(255),
    phone         varchar(15) UNIQUE,
    created_at    timestamp,
    updated_at    timestamp,
    deleted_at    timestamp,
    created_by    varchar(50),
    updated_by    varchar(50),
    deleted_by    varchar(50)
)ENGINE = InnoDB COLLATE = utf8mb4_unicode_ci;

CREATE TABLE role
(
    id          varchar(36) PRIMARY KEY,
    role        varchar(50) UNIQUE,
    description varchar(250) DEFAULT NULL
)ENGINE = InnoDB COLLATE = utf8mb4_unicode_ci;

CREATE TABLE user_roles
(
    user_id  varchar(36) NOT NULL PRIMARY KEY REFERENCES user (id) ON DELETE CASCADE,
    roles_id varchar(36) NOT NULL REFERENCES role (id) ON DELETE CASCADE
) ENGINE = InnoDB
  COLLATE = utf8mb4_unicode_ci;
