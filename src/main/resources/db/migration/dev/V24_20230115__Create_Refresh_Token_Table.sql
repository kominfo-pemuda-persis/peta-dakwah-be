CREATE TABLE refresh_token
(
    id          varchar(36),
    user_id     varchar(36) NOT NULL,
    token       varchar(50) NOT NULL,
    expiry_date TIMESTAMP   NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES user (id)
) ENGINE = InnoDB COLLATE = utf8mb4_unicode_ci;
