INSERT INTO `user` (`id`,`npa`,`kode_pc`,`kelurahan_id`,`username`,`nama_lengkap`,`jenis_kelamin`,`tempat_lahir`,`tanggal_lahir`,`active`,`otonom`,`password`,`email`,`phone`,`created_at`,`updated_at`)
VALUES
  (uuid(),"22.4444","PC.3","32.04.10.2003","user_UEQ63MOW2QJ","Cynthia Ortiz","L","Dumai","2022-10-03","0","PERSIS","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","elit.curabitur@icloud.couk","087406814276","2022-05-19 02:04:14","2022-03-02 07:03:54"),
  (uuid(),"22.4888","PC.1","32.04.10.2004","user_YPF33YWU0OL","Kyra Horton","P","Singkawang","2021-11-29","0","PERSISTRI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","mauris.molestie@yahoo.net","082170035568","2022-04-30 11:06:38","2022-04-01 03:05:45"),
  (uuid(),"22.6526","PC.2","32.04.10.2004","user_JSX07JIG3FW","Isabella Mullen","P","Jayapura","2022-01-17","0","PEMUDA","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","ipsum.dolor@google.ca","081787430977","2022-07-04 06:01:24","2022-12-30 07:04:5"),
  (uuid(),"22.8662","PC.4","32.04.10.2005","user_MPG78RPM4WA","Zachary Singleton","L","Medan","2023-01-24","1","PERSISTRI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","lobortis.nisi@aol.ca","082108322224","2022-12-07 09:03:46","2023-02-07 07:02:29"),
  (uuid(),"22.2856","PC.4","32.04.10.2004","user_EJY16YVM8SR","Shoshana Gill","P","Jayapura","2023-01-30","0","PEMUDA","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","pede@aol.couk","082645837978","2023-05-15 04:01:37","2023-05-31 07:03:45"),
  (uuid(),"22.7834","PC.4","32.04.10.2001","user_QXB21QSS3WK","Tashya Callahan","P","Singkawang","2022-03-17","1","PEMUDA","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","parturient.montes.nascetur@hotmail.edu","086932796554","2021-10-15 04:05:55","2022-07-07 05:04:9"),
  (uuid(),"22.5654","PC.2","32.04.10.2003","user_YKQ66BID5RE","Jordan Waters","L","Pontianak","2022-05-21","0","PEMUDI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","non.cursus@yahoo.ca","080077772984","2022-05-17 07:02:40","2023-05-06 10:06:36"),
  (uuid(),"22.4253","PC.4","32.04.10.2004","user_DJJ72DSO5SJ","Dillon Velasquez","P","Dumai","2022-10-02","1","PERSISTRI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","amet@hotmail.org","084441851401","2023-09-23 04:06:1","2022-04-09 01:06:28"),
  (uuid(),"22.4621","PC.4","32.04.10.2003","user_VKW95OGY3HC","Christian Solomon","P","Bandar Lampung","2023-07-10","1","PEMUDA","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","natoque.penatibus@outlook.ca","087921804281","2023-07-10 10:01:32","2021-12-14 06:02:6"),
  (uuid(),"22.3433","PC.2","32.04.10.2004","user_AKK71JRT1GX","Eleanor Ramirez","P","Manokwari","2023-09-23","0","PERSISTRI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","sed@aol.com","084193723866","2023-04-21 02:05:9","2022-12-28 12:02:44");
INSERT INTO `user` (`id`,`npa`,`kode_pc`,`kelurahan_id`,`username`,`nama_lengkap`,`jenis_kelamin`,`tempat_lahir`,`tanggal_lahir`,`active`,`otonom`,`password`,`email`,`phone`,`created_at`,`updated_at`)
VALUES
  (uuid(),"22.5834","PC.3","32.04.10.2003","user_RWE23RKN1PC","Angelica Pickett","P","Palangka Raya","2021-09-27","1","PEMUDA","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","sit@outlook.edu","081406752542","2022-06-15 11:03:51","2022-08-07 07:07:43"),
  (uuid(),"22.4362","PC.1","32.04.10.2005","user_KOE18LST4QH","Kermit Hampton","P","Sungai Penuh","2022-07-06","1","PERSISTRI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","hymenaeos@hotmail.com","081089383683","2023-05-04 01:04:14","2023-04-07 08:05:23"),
  (uuid(),"22.5549","PC.3","32.04.10.2003","user_WSS96OUM4FQ","Samuel Delaney","P","Palangka Raya","2022-08-31","1","PERSISTRI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","adipiscing.lacus@aol.ca","087127160467","2022-02-10 04:04:57","2021-11-30 02:02:10"),
  (uuid(),"22.9786","PC.4","32.04.10.2001","user_YZS38KSS5DF","Rebekah Fitzpatrick","P","Palopo","2022-06-08","1","PEMUDA","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","nullam.ut.nisi@yahoo.couk","087478836547","2021-11-11 02:04:40","2023-03-20 06:01:43"),
  (uuid(),"22.7717","PC.3","32.04.10.2004","user_BPY18UPP1HO","Jin Tate","P","Pangkalpinang","2022-09-16","1","PEMUDI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","pellentesque.tincidunt@aol.edu","088238042726","2022-01-10 07:01:15","2022-08-24 06:03:40"),
  (uuid(),"22.5581","PC.3","32.04.10.2001","user_QEJ64GOW3JT","Lev Best","L","Denpasar","2021-12-17","0","PEMUDI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","sit.amet@outlook.edu","083862681160","2022-06-23 04:04:26","2023-07-05 08:03:30"),
  (uuid(),"22.6252","PC.3","32.04.10.2003","user_RXY64HCK8YQ","Hedy Holland","P","Denpasar","2021-11-23","0","PERSISTRI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","amet.consectetuer@hotmail.edu","080746940772","2023-08-22 12:02:13","2022-09-11 01:07:34"),
  (uuid(),"22.5562","PC.3","32.04.10.2004","user_IVD44VTI8ST","Daniel Medina","L","Ternate","2022-10-05","0","PEMUDI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","egestas.rhoncus@yahoo.ca","086523877244","2022-08-02 11:02:11","2022-12-25 05:07:19"),
  (uuid(),"22.8647","PC.3","32.04.10.2004","user_RTD17FCW3VD","Lee Wall","L","Palopo","2023-07-04","0","PERSIS","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","eget.volutpat@icloud.com","081102884156","2022-07-22 01:05:14","2022-04-07 08:04:3"),
  (uuid(),"22.3178","PC.1","32.04.10.2005","user_KGX81BBL9BV","Tarik Kemp","P","Pangkalpinang","2023-09-21","0","PERSIS","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","donec.nibh@google.couk","087527671525","2022-02-25 02:05:10","2021-12-21 08:02:50");
INSERT INTO `user` (`id`,`npa`,`kode_pc`,`kelurahan_id`,`username`,`nama_lengkap`,`jenis_kelamin`,`tempat_lahir`,`tanggal_lahir`,`active`,`otonom`,`password`,`email`,`phone`,`created_at`,`updated_at`)
VALUES
  (uuid(),"22.7568","PC.2","32.04.10.2005","user_IGB58RDO3BG","Leonard Morrow","P","Kendari","2023-01-18","1","PERSISTRI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","elit.elit.fermentum@protonmail.ca","080467118366","2022-12-17 04:06:25","2022-12-28 03:02:5"),
  (uuid(),"22.3603","PC.3","32.04.10.2003","user_EQB07IUI3CQ","Rigel William","P","Banda Aceh","2021-12-22","1","PEMUDI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","ut@google.couk","083304233573","2022-07-13 08:03:14","2022-10-27 02:04:0"),
  (uuid(),"22.5634","PC.3","32.04.10.2003","user_PCQ57CQN7OT","Maggy Roman","P","Pangkalpinang","2022-12-29","1","PERSISTRI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","pharetra.nibh@yahoo.edu","082761813612","2022-03-16 01:03:13","2022-03-03 05:04:6"),
  (uuid(),"22.7355","PC.4","32.04.10.2005","user_CRI14DJU7NK","Clarke Guzman","P","Denpasar","2021-10-25","1","PEMUDI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","amet.ante.vivamus@outlook.com","082035836594","2022-05-20 09:05:54","2022-02-26 10:06:14"),
  (uuid(),"22.2710","PC.2","32.04.10.2001","user_NUY26WFF3BY","Xenos Morin","L","Mataram","2023-07-01","1","PERSISTRI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","lorem.vehicula.et@hotmail.com","085214604436","2022-12-27 04:02:31","2022-06-16 08:04:57"),
  (uuid(),"22.0642","PC.1","32.04.10.2003","user_NFB66PSA6IB","Kevin Maldonado","L","Pekanbaru","2022-12-26","1","PERSIS","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","ultricies.dignissim.lacus@yahoo.net","083414307146","2022-10-01 04:06:8","2021-12-25 01:06:26"),
  (uuid(),"22.5289","PC.2","32.04.10.2004","user_GWM84LJM2SI","Kevin Gibbs","L","Mamuju","2022-06-24","1","PERSISTRI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","lacus.etiam@outlook.edu","081132240721","2023-03-04 03:06:32","2023-04-03 09:01:4"),
  (uuid(),"22.8453","PC.2","32.04.10.2002","user_IGR27PFH4RU","Lucius Leach","P","Padang Sidempuan","2022-11-16","0","PERSISTRI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","tempor.erat.neque@aol.ca","083355159301","2023-06-26 08:01:22","2021-10-16 04:06:22"),
  (uuid(),"22.0173","PC.3","32.04.10.2001","user_KMW62UEK4BD","Briar Moss","L","Banjarmasin","2023-03-23","1","PEMUDA","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","tempus.non@icloud.com","086686082803","2023-02-26 06:07:0","2023-04-13 06:04:46"),
  (uuid(),"22.3132","PC.4","32.04.10.2005","user_KBP48XGF2CC","Rosalyn Patrick","L","Ambon","2023-06-21","1","PERSIS","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","massa@icloud.couk","087745771182","2021-10-05 01:02:6","2023-07-25 11:02:32");
INSERT INTO `user` (`id`,`npa`,`kode_pc`,`kelurahan_id`,`username`,`nama_lengkap`,`jenis_kelamin`,`tempat_lahir`,`tanggal_lahir`,`active`,`otonom`,`password`,`email`,`phone`,`created_at`,`updated_at`)
VALUES
  (uuid(),"22.4824","PC.1","32.04.10.2002","user_MXO67OBI3VJ","Galvin Hickman","L","Kupang","2022-08-28","1","PEMUDI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","curabitur.vel.lectus@aol.ca","086755442453","2022-03-07 11:01:16","2022-08-09 09:02:40"),
  (uuid(),"22.8867","PC.1","32.04.10.2002","user_GXJ57BPI4XK","Vernon Langley","L","Bandar Lampung","2023-02-01","1","PEMUDA","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","proin.vel.nisl@yahoo.org","085312405277","2023-03-24 08:05:44","2022-05-03 09:02:17"),
  (uuid(),"22.5232","PC.3","32.04.10.2001","user_TFF22ADB2VG","Jelani Goodwin","P","Denpasar","2022-08-16","0","PERSISTRI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","est@yahoo.net","088973961322","2022-12-28 08:02:43","2021-10-26 09:02:11"),
  (uuid(),"22.4822","PC.3","32.04.10.2002","user_BIR02ZXY0JJ","Cara Mcmillan","P","Probolinggo","2022-06-29","1","PERSIS","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","non@protonmail.net","082078666218","2023-07-09 09:07:33","2022-08-08 12:01:0"),
  (uuid(),"22.3154","PC.2","32.04.10.2004","user_QCT58AMT2LL","Ira Cantu","P","Palembang","2022-09-30","0","PEMUDI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","at.nisi@hotmail.edu","089820283816","2022-11-01 12:02:42","2022-06-07 06:02:39"),
  (uuid(),"22.6388","PC.4","32.04.10.2004","user_TDR58PJW1KC","Daria Cummings","P","Singkawang","2022-06-19","1","PEMUDI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","dolor@google.org","086507417291","2023-01-14 04:06:12","2023-08-01 08:02:52"),
  (uuid(),"22.5832","PC.4","32.04.10.2004","user_NWX44UXP7KD","Guinevere Rowland","L","Pangkalpinang","2023-02-28","1","PEMUDI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","velit@icloud.org","083754465564","2022-11-12 06:06:23","2023-02-13 12:01:45"),
  (uuid(),"22.2761","PC.4","32.04.10.2002","user_HWP25TSJ7US","Aiko Downs","L","Gorontalo","2022-01-07","1","PERSISTRI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","sollicitudin@icloud.couk","089219178636","2023-03-12 09:07:25","2023-02-21 11:02:32"),
  (uuid(),"22.1838","PC.3","32.04.10.2004","user_VPC41SJW7FX","Farrah Harrington","L","Palopo","2022-09-11","1","PERSISTRI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","iaculis.lacus.pede@protonmail.ca","087234581528","2023-06-04 01:07:40","2021-12-18 09:06:59"),
  (uuid(),"22.4564","PC.3","32.04.10.2004","user_ETK61DKL6KS","Jermaine Cook","L","Balikpapan","2022-01-29","0","PEMUDI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","taciti.sociosqu@outlook.edu","086387717924","2023-03-11 02:06:7","2023-04-16 12:07:54");
INSERT INTO `user` (`id`,`npa`,`kode_pc`,`kelurahan_id`,`username`,`nama_lengkap`,`jenis_kelamin`,`tempat_lahir`,`tanggal_lahir`,`active`,`otonom`,`password`,`email`,`phone`,`created_at`,`updated_at`)
VALUES
  (uuid(),"22.5443","PC.3","32.04.10.2002","user_POT18TXS7DS","Eleanor Mills","L","Dumai","2023-05-01","0","PERSISTRI","$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga","eu.ultrices@aol.ca","081263667517","2023-02-20 06:01:23","2022-04-10 04:07:46"),
  (UUID(), "22.7742", "PC.2", "32.04.10.2005", "user_BBF38SDA8AP", "Neil Flowers", "L", "Palangka Raya", "2023-01-29",
   "0", "PEMUDI", "$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga", "faucibus.orci.luctus@outlook.couk",
   "083314947251", "2022-03-27 11:07:18", "2022-05-13 02:05:17"),
  (UUID(), "22.6912", "PC.2", "32.04.10.2003", "user_RQK83IQF1FV", "Lane Freeman", "L", "Semarang", "2022-05-20", "0",
   "PEMUDA", "$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga", "dis.parturient@yahoo.org", "083347218126",
   "2022-06-12 01:07:29", "2022-11-02 08:03:10"),
  (UUID(), "22.7964", "PC.3", "32.04.10.2002", "user_TJQ88FKN2VE", "Chandler Greer", "L", "Pekanbaru", "2022-12-14",
   "0", "PEMUDI", "$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga", "egestas.nunc@protonmail.com",
   "081286136203", "2023-08-20 03:07:40", "2021-12-24 01:05:45"),
  (UUID(), "22.0904", "PC.1", "32.04.10.2003", "user_VLL33UHH8VH", "Driscoll Pratt", "L", "Banda Aceh", "2023-06-28",
   "1", "PEMUDA", "$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga", "lectus.pede@aol.org", "086792268325",
   "2021-12-11 06:06:8", "2023-07-30 08:07:29"),
  (UUID(), "22.8744", "PC.1", "32.04.10.2003", "user_GLQ54BQS7HG", "Serina Weeks", "P", "Pangkalpinang", "2022-01-07",
   "0", "PERSIS", "$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga", "risus.donec@yahoo.org",
   "087264761332", "2022-12-25 04:07:40", "2021-10-12 12:02:25"),
  (UUID(), "22.6784", "PC.3", "32.04.10.2004", "user_VMS18NDC0OV", "Graham Joyce", "P", "Pontianak", "2022-08-06", "0",
   "PERSISTRI", "$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga", "at.risus@icloud.com", "085927789776",
   "2022-06-13 06:01:51", "2022-07-19 05:02:40"),
  (UUID(), "22.2481", "PC.1", "32.04.10.2002", "user_DDU38NNG6AL", "Aaron Robles", "P", "Jayapura", "2022-07-13", "0",
   "PERSISTRI", "$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga", "ullamcorper.velit@hotmail.com",
   "088577843123", "2022-04-17 10:07:30", "2023-02-17 04:05:15"),
  (UUID(), "22.9583", "PC.1", "32.04.10.2004", "user_TEG28GVK9RU", "Todd Hogan", "P", "Jayapura", "2021-12-14", "0",
   "PERSIS", "$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga", "vestibulum@icloud.edu", "081304848796",
   "2022-05-21 03:06:22", "2023-06-30 10:05:4"),
  (UUID(), "22.9432", "PC.3", "32.04.10.2003", "user_LXO88YHO7ES", "Brendan Dickerson", "P", "Jayapura", "2023-06-01",
   "0", "PERSIS", "$2a$12$MqYCiQV9mDZnUAJE2nrVtOTdVNOcSeM6Ta16GuxP3hoApXv8RW.Ga", "ante.ipsum.primis@protonmail.ca",
   "084577618827", "2022-12-01 02:04:9", "2023-02-10 10:05:25");

-- Naruto2023!
INSERT INTO user (id, npa, kode_pc, kelurahan_id, username, nama_lengkap, jenis_kelamin, tempat_lahir, tanggal_lahir,
                  active, otonom, password, email, photo, phone, created_at, updated_at, deleted_at, created_by,
                  updated_by, deleted_by)

VALUES ('7b7927dc-e8c2-478c-86c4-cd788f18d92c', '03.1042', 'PC.30', '32.04.10.2005', 'hendi', 'Hendi Santika', 'L',
        'BANDUNG',
        '1986-09-19', 1, 'PEMUDA', '$2a$12$8XPbV0SOddbqM.eBhmmzwu/75LtnvpYsfVvAoRiiRmRBqbpYGj14q', 'hendi@yopmail.com',
        NULL, '081321411881', CURRENT_TIMESTAMP, NULL, NULL, 'hendi', 'hendi', NULL),
       ('7b7927dc-e8c2-478c-86c4-cd788f18d92d', '23.0001', 'PC.30', '32.04.10.2005', 'superadmin', 'SUPER ADMIN', 'L',
        'BANDUNG', '1986-09-19', 1, 'PEMUDA', '$2a$12$8XPbV0SOddbqM.eBhmmzwu/75LtnvpYsfVvAoRiiRmRBqbpYGj14q',
        'superadmin@yopmail.com', NULL, '081321411800', CURRENT_TIMESTAMP, NULL, NULL, 'hendi', 'hendi', NULL),
       ('7b7927dc-e8c2-478c-86c4-cd788f18d90d', '23.0002', 'PC.30', '32.04.10.2005', 'adminpp', 'SUPER ADMIN', 'L',
        'BANDUNG', '1986-09-19', 1, 'PEMUDA', '$2a$12$8XPbV0SOddbqM.eBhmmzwu/75LtnvpYsfVvAoRiiRmRBqbpYGj14q',
        'adminpp@yopmail.com', NULL, '081321411801', CURRENT_TIMESTAMP, NULL, NULL, 'hendi', 'hendi', NULL),
       ('7b7927dc-e8c2-478c-86c4-cd788f18d91d', '23.0003', 'PC.30', '32.04.10.2005', 'adminpw', 'SUPER ADMIN', 'L',
        'BANDUNG', '1986-09-19', 1, 'PEMUDA', '$2a$12$8XPbV0SOddbqM.eBhmmzwu/75LtnvpYsfVvAoRiiRmRBqbpYGj14q',
        'adminpw@yopmail.com', NULL, '081321411802', CURRENT_TIMESTAMP, NULL, NULL, 'hendi', 'hendi', NULL),
       ('8b7927dc-e8c2-478c-86c4-cd788f18d92d', '23.0004', 'PC.30', '32.04.10.2005', 'adminpd', 'SUPER ADMIN', 'L',
        'BANDUNG', '1986-09-19', 1, 'PEMUDA', '$2a$12$8XPbV0SOddbqM.eBhmmzwu/75LtnvpYsfVvAoRiiRmRBqbpYGj14q',
        'adminpd@yopmail.com', NULL, '0813214118003', CURRENT_TIMESTAMP, NULL, NULL, 'hendi', 'hendi', NULL),
       ('7b7927dc-e8c2-478c-86c4-cd788f18d93d', '23.0005', 'PC.30', '32.04.10.2005', 'adminpc', 'SUPER ADMIN', 'L',
        'BANDUNG', '1986-09-19', 1, 'PEMUDA', '$2a$12$8XPbV0SOddbqM.eBhmmzwu/75LtnvpYsfVvAoRiiRmRBqbpYGj14q',
        'adminpc@yopmail.com', NULL, '081321411804', CURRENT_TIMESTAMP, NULL, NULL, 'hendi', 'hendi', NULL),
       ('7b7927dc-e8c2-478c-86c4-cd788f18d94d', '23.0006', 'PC.30', '32.04.10.2005', 'adminpj', 'SUPER ADMIN', 'L',
        'BANDUNG', '1986-09-19', 1, 'PEMUDA', '$2a$12$8XPbV0SOddbqM.eBhmmzwu/75LtnvpYsfVvAoRiiRmRBqbpYGj14q',
        'adminpj@yopmail.com', NULL, '081321411805', CURRENT_TIMESTAMP, NULL, NULL, 'hendi', 'hendi', NULL),
       ('7b7927dc-e8c2-478c-86c4-cd788f18d95d', '23.0007', 'PC.30', '32.04.10.2005', 'tasykilpp', 'SUPER ADMIN', 'L',
        'BANDUNG', '1986-09-19', 1, 'PEMUDA', '$2a$12$8XPbV0SOddbqM.eBhmmzwu/75LtnvpYsfVvAoRiiRmRBqbpYGj14q',
        'tasykilpp@yopmail.com', NULL, '081321411806', CURRENT_TIMESTAMP, NULL, NULL, 'hendi', 'hendi', NULL),
       ('7b7927dc-e8c2-478c-86c4-cd788f18d96d', '23.0008', 'PC.30', '32.04.10.2005', 'tasykilpw', 'SUPER ADMIN', 'L',
        'BANDUNG', '1986-09-19', 1, 'PEMUDA', '$2a$12$8XPbV0SOddbqM.eBhmmzwu/75LtnvpYsfVvAoRiiRmRBqbpYGj14q',
        'tasykilpw@yopmail.com', NULL, '081321411807', CURRENT_TIMESTAMP, NULL, NULL, 'hendi', 'hendi', NULL),
       ('7b7927dc-e8c2-478c-86c4-cd788f18d97d', '23.0009', 'PC.30', '32.04.10.2005', 'tasykilpd', 'SUPER ADMIN', 'L',
        'BANDUNG', '1986-09-19', 1, 'PEMUDA', '$2a$12$8XPbV0SOddbqM.eBhmmzwu/75LtnvpYsfVvAoRiiRmRBqbpYGj14q',
        'tasykilpd@yopmail.com', NULL, '081321411808', CURRENT_TIMESTAMP, NULL, NULL, 'hendi', 'hendi', NULL),
       ('7b7927dc-e8c2-478c-86c4-cd788f18d98d', '23.0010', 'PC.30', '32.04.10.2005', 'tasykilpc', 'SUPER ADMIN', 'L',
        'BANDUNG', '1986-09-19', 1, 'PEMUDA', '$2a$12$8XPbV0SOddbqM.eBhmmzwu/75LtnvpYsfVvAoRiiRmRBqbpYGj14q',
        'tasykilpc@yopmail.com', NULL, '081321411809', CURRENT_TIMESTAMP, NULL, NULL, 'hendi', 'hendi', NULL),
       ('7b7927dc-e8c2-478c-86c4-cd788f18d99d', '23.0011', 'PC.30', '32.04.10.2005', 'anggota', 'SUPER ADMIN', 'L',
        'BANDUNG', '1986-09-19', 1, 'PEMUDA', '$2a$12$8XPbV0SOddbqM.eBhmmzwu/75LtnvpYsfVvAoRiiRmRBqbpYGj14q',
        'anggota@yopmail.com', NULL, '081321411810', CURRENT_TIMESTAMP, NULL, NULL, 'hendi', 'hendi', NULL);

INSERT INTO user_roles (roles_id, user_id)
VALUES ('256d9f49-b851-4bd7-bb1b-4d2e2ebcecc5', '7b7927dc-e8c2-478c-86c4-cd788f18d92c');
INSERT INTO user_roles (user_id, roles_id)
VALUES ('7b7927dc-e8c2-478c-86c4-cd788f18d92d', '256d9f49-b851-4bd7-bb1b-4d2e2ebcecc5');
INSERT INTO user_roles (user_id, roles_id)
VALUES ('7b7927dc-e8c2-478c-86c4-cd788f18d90d', '156d9f49-b851-4bd7-bb1b-4d2e2ebcecc5');
INSERT INTO user_roles (user_id, roles_id)
VALUES ('7b7927dc-e8c2-478c-86c4-cd788f18d91d', '456d9f49-b851-4bd7-bb1b-4d2e2ebcecc5');
INSERT INTO user_roles (user_id, roles_id)
VALUES ('8b7927dc-e8c2-478c-86c4-cd788f18d92d', '656d9f49-b851-4bd7-bb1b-4d2e2ebcecc5');
INSERT INTO user_roles (user_id, roles_id)
VALUES ('7b7927dc-e8c2-478c-86c4-cd788f18d93d', '856d9f49-b851-4bd7-bb1b-4d2e2ebcecc5');
INSERT INTO user_roles (user_id, roles_id)
VALUES ('7b7927dc-e8c2-478c-86c4-cd788f18d94d', '106d9f49-b851-4bd7-bb1b-4d2e2ebcecc5');
INSERT INTO user_roles (user_id, roles_id)
VALUES ('7b7927dc-e8c2-478c-86c4-cd788f18d95d', '356d9f49-b851-4bd7-bb1b-4d2e2ebcecc5');
INSERT INTO user_roles (user_id, roles_id)
VALUES ('7b7927dc-e8c2-478c-86c4-cd788f18d96d', '556d9f49-b851-4bd7-bb1b-4d2e2ebcecc5');
INSERT INTO user_roles (user_id, roles_id)
VALUES ('7b7927dc-e8c2-478c-86c4-cd788f18d97d', '756d9f49-b851-4bd7-bb1b-4d2e2ebcecc5');
INSERT INTO user_roles (user_id, roles_id)
VALUES ('7b7927dc-e8c2-478c-86c4-cd788f18d98d', '956d9f49-b851-4bd7-bb1b-4d2e2ebcecc5');
INSERT INTO user_roles (user_id, roles_id)
VALUES ('7b7927dc-e8c2-478c-86c4-cd788f18d99d', '116d9f49-b851-4bd7-bb1b-4d2e2ebcecc5');



