CREATE TABLE cities_coordinate
(
    kode     varchar(13) PRIMARY KEY NOT NULL,
    nama     varchar(100)                     DEFAULT NULL,
    ibukota  varchar(100)                     DEFAULT NULL,
    lat      double                           DEFAULT NULL COMMENT 'latitude in degrees',
    lng      double                           DEFAULT NULL COMMENT 'longitude in degrees',
    elv      float                   NOT NULL DEFAULT '0' COMMENT 'elevation in meters',
    tz       tinyint                          DEFAULT NULL COMMENT 'timezone in hour',
    luas     double                           DEFAULT NULL COMMENT 'an area in km2',
    penduduk double                           DEFAULT NULL COMMENT 'population',
    path     longtext CHARACTER SET UTF8MB4 COLLATE utf8mb4_unicode_ci COMMENT 'boundaries/polygon area',
    status   tinyint                          DEFAULT NULL,
    UNIQUE KEY kode (kode)
) ENGINE = MyISAM
  DEFAULT CHARSET = UTF8MB4;