CREATE TABLE student
(
    id           varchar(36) PRIMARY KEY NOT NULL,
    email        varchar(255) NULL,
    first_name   varchar(255) NULL,
    last_name    varchar(255) NULL,
    phone_number varchar(255) NULL,
    jurusan      varchar(255)            NOT NULL,
    birth_date   date                    NOT NULL,
    kode_pc      varchar(10),
    FOREIGN KEY (kode_pc) REFERENCES pc (kd_pc),
    kelurahan_id varchar(13),
    FOREIGN KEY (kelurahan_id)
        REFERENCES kelurahan (id)
        ON DELETE CASCADE ON UPDATE CASCADE,
    created_at   timestamp NULL,
    updated_at   timestamp NULL,
    deleted_at   timestamp NULL,
    created_by   varchar(50) NULL,
    updated_by   varchar(50) NULL,
    deleted_by   varchar(50) NULL,
    CONSTRAINT UK_fe0i52si7ybu0wjedj6motiim
        UNIQUE (email),
    CONSTRAINT UK_i3xrfnuv2icsd1vhvn6c108ec
        UNIQUE (phone_number)
) ENGINE = InnoDB COLLATE = utf8mb4_unicode_ci;
