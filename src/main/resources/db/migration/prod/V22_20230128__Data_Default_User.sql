-- Naruto2023!
INSERT INTO user (id, npa, kode_pc, kelurahan_id, username, nama_lengkap, jenis_kelamin, tempat_lahir, tanggal_lahir,
                  active, otonom, password, email, photo, phone, created_at, updated_at, deleted_at, created_by,
                  updated_by, deleted_by)

VALUES ('7b7927dc-e8c2-478c-86c4-cd788f18d92c', '03.1042', 'PC.30', '32.04.10.2005', 'hendi', 'Hendi Santika', 'L',
        'BANDUNG',
        '1986-09-19', 1, 'PEMUDA', '$2a$12$8XPbV0SOddbqM.eBhmmzwu/75LtnvpYsfVvAoRiiRmRBqbpYGj14q', 'hendi@yopmail.com',
        NULL, '081321411881', CURRENT_TIMESTAMP, NULL, NULL, 'hendi', 'hendi', NULL),
       ('7b7927dc-e8c2-478c-86c4-cd788f18d92d', '23.0001', 'PC.30', '32.04.10.2005', 'superadmin', 'SUPER ADMIN', 'L',
        'BANDUNG', '1986-09-19', 1, 'PEMUDA', '$2a$12$8XPbV0SOddbqM.eBhmmzwu/75LtnvpYsfVvAoRiiRmRBqbpYGj14q',
        'superadmin@yopmail.com', NULL, '081321411800', CURRENT_TIMESTAMP, NULL, NULL, 'hendi', 'hendi', NULL),
       ('7b7927dc-e8c2-478c-86c4-cd788f18d90d', '23.0002', 'PC.30', '32.04.10.2005', 'adminpp', 'SUPER ADMIN', 'L',
        'BANDUNG', '1986-09-19', 1, 'PEMUDA', '$2a$12$8XPbV0SOddbqM.eBhmmzwu/75LtnvpYsfVvAoRiiRmRBqbpYGj14q',
        'adminpp@yopmail.com', NULL, '081321411801', CURRENT_TIMESTAMP, NULL, NULL, 'hendi', 'hendi', NULL),
       ('7b7927dc-e8c2-478c-86c4-cd788f18d91d', '23.0003', 'PC.30', '32.04.10.2005', 'adminpw', 'SUPER ADMIN', 'L',
        'BANDUNG', '1986-09-19', 1, 'PEMUDA', '$2a$12$8XPbV0SOddbqM.eBhmmzwu/75LtnvpYsfVvAoRiiRmRBqbpYGj14q',
        'adminpw@yopmail.com', NULL, '081321411802', CURRENT_TIMESTAMP, NULL, NULL, 'hendi', 'hendi', NULL),
       ('8b7927dc-e8c2-478c-86c4-cd788f18d92d', '23.0004', 'PC.30', '32.04.10.2005', 'adminpd', 'SUPER ADMIN', 'L',
        'BANDUNG', '1986-09-19', 1, 'PEMUDA', '$2a$12$8XPbV0SOddbqM.eBhmmzwu/75LtnvpYsfVvAoRiiRmRBqbpYGj14q',
        'adminpd@yopmail.com', NULL, '0813214118003', CURRENT_TIMESTAMP, NULL, NULL, 'hendi', 'hendi', NULL),
       ('7b7927dc-e8c2-478c-86c4-cd788f18d93d', '23.0005', 'PC.30', '32.04.10.2005', 'adminpc', 'SUPER ADMIN', 'L',
        'BANDUNG', '1986-09-19', 1, 'PEMUDA', '$2a$12$8XPbV0SOddbqM.eBhmmzwu/75LtnvpYsfVvAoRiiRmRBqbpYGj14q',
        'adminpc@yopmail.com', NULL, '081321411804', CURRENT_TIMESTAMP, NULL, NULL, 'hendi', 'hendi', NULL),
       ('7b7927dc-e8c2-478c-86c4-cd788f18d94d', '23.0006', 'PC.30', '32.04.10.2005', 'adminpj', 'SUPER ADMIN', 'L',
        'BANDUNG', '1986-09-19', 1, 'PEMUDA', '$2a$12$8XPbV0SOddbqM.eBhmmzwu/75LtnvpYsfVvAoRiiRmRBqbpYGj14q',
        'adminpj@yopmail.com', NULL, '081321411805', CURRENT_TIMESTAMP, NULL, NULL, 'hendi', 'hendi', NULL),
       ('7b7927dc-e8c2-478c-86c4-cd788f18d95d', '23.0007', 'PC.30', '32.04.10.2005', 'tasykilpp', 'SUPER ADMIN', 'L',
        'BANDUNG', '1986-09-19', 1, 'PEMUDA', '$2a$12$8XPbV0SOddbqM.eBhmmzwu/75LtnvpYsfVvAoRiiRmRBqbpYGj14q',
        'tasykilpp@yopmail.com', NULL, '081321411806', CURRENT_TIMESTAMP, NULL, NULL, 'hendi', 'hendi', NULL),
       ('7b7927dc-e8c2-478c-86c4-cd788f18d96d', '23.0008', 'PC.30', '32.04.10.2005', 'tasykilpw', 'SUPER ADMIN', 'L',
        'BANDUNG', '1986-09-19', 1, 'PEMUDA', '$2a$12$8XPbV0SOddbqM.eBhmmzwu/75LtnvpYsfVvAoRiiRmRBqbpYGj14q',
        'tasykilpw@yopmail.com', NULL, '081321411807', CURRENT_TIMESTAMP, NULL, NULL, 'hendi', 'hendi', NULL),
       ('7b7927dc-e8c2-478c-86c4-cd788f18d97d', '23.0009', 'PC.30', '32.04.10.2005', 'tasykilpd', 'SUPER ADMIN', 'L',
        'BANDUNG', '1986-09-19', 1, 'PEMUDA', '$2a$12$8XPbV0SOddbqM.eBhmmzwu/75LtnvpYsfVvAoRiiRmRBqbpYGj14q',
        'tasykilpd@yopmail.com', NULL, '081321411808', CURRENT_TIMESTAMP, NULL, NULL, 'hendi', 'hendi', NULL),
       ('7b7927dc-e8c2-478c-86c4-cd788f18d98d', '23.0010', 'PC.30', '32.04.10.2005', 'tasykilpc', 'SUPER ADMIN', 'L',
        'BANDUNG', '1986-09-19', 1, 'PEMUDA', '$2a$12$8XPbV0SOddbqM.eBhmmzwu/75LtnvpYsfVvAoRiiRmRBqbpYGj14q',
        'tasykilpc@yopmail.com', NULL, '081321411809', CURRENT_TIMESTAMP, NULL, NULL, 'hendi', 'hendi', NULL),
       ('7b7927dc-e8c2-478c-86c4-cd788f18d99d', '23.0011', 'PC.30', '32.04.10.2005', 'anggota', 'SUPER ADMIN', 'L',
        'BANDUNG', '1986-09-19', 1, 'PEMUDA', '$2a$12$8XPbV0SOddbqM.eBhmmzwu/75LtnvpYsfVvAoRiiRmRBqbpYGj14q',
        'anggota@yopmail.com', NULL, '081321411810', CURRENT_TIMESTAMP, NULL, NULL, 'hendi', 'hendi', NULL);


INSERT INTO user_roles (roles_id, user_id)
VALUES ('256d9f49-b851-4bd7-bb1b-4d2e2ebcecc5', '7b7927dc-e8c2-478c-86c4-cd788f18d92c');
INSERT INTO user_roles (user_id, roles_id)
VALUES ('7b7927dc-e8c2-478c-86c4-cd788f18d92d', '256d9f49-b851-4bd7-bb1b-4d2e2ebcecc5');
INSERT INTO user_roles (user_id, roles_id)
VALUES ('7b7927dc-e8c2-478c-86c4-cd788f18d90d', '156d9f49-b851-4bd7-bb1b-4d2e2ebcecc5');
INSERT INTO user_roles (user_id, roles_id)
VALUES ('7b7927dc-e8c2-478c-86c4-cd788f18d91d', '456d9f49-b851-4bd7-bb1b-4d2e2ebcecc5');
INSERT INTO user_roles (user_id, roles_id)
VALUES ('8b7927dc-e8c2-478c-86c4-cd788f18d92d', '656d9f49-b851-4bd7-bb1b-4d2e2ebcecc5');
INSERT INTO user_roles (user_id, roles_id)
VALUES ('7b7927dc-e8c2-478c-86c4-cd788f18d93d', '856d9f49-b851-4bd7-bb1b-4d2e2ebcecc5');
INSERT INTO user_roles (user_id, roles_id)
VALUES ('7b7927dc-e8c2-478c-86c4-cd788f18d94d', '106d9f49-b851-4bd7-bb1b-4d2e2ebcecc5');
INSERT INTO user_roles (user_id, roles_id)
VALUES ('7b7927dc-e8c2-478c-86c4-cd788f18d95d', '356d9f49-b851-4bd7-bb1b-4d2e2ebcecc5');
INSERT INTO user_roles (user_id, roles_id)
VALUES ('7b7927dc-e8c2-478c-86c4-cd788f18d96d', '556d9f49-b851-4bd7-bb1b-4d2e2ebcecc5');
INSERT INTO user_roles (user_id, roles_id)
VALUES ('7b7927dc-e8c2-478c-86c4-cd788f18d97d', '756d9f49-b851-4bd7-bb1b-4d2e2ebcecc5');
INSERT INTO user_roles (user_id, roles_id)
VALUES ('7b7927dc-e8c2-478c-86c4-cd788f18d98d', '956d9f49-b851-4bd7-bb1b-4d2e2ebcecc5');
INSERT INTO user_roles (user_id, roles_id)
VALUES ('7b7927dc-e8c2-478c-86c4-cd788f18d99d', '116d9f49-b851-4bd7-bb1b-4d2e2ebcecc5');



