CREATE TABLE provinsi
(
    id   varchar(2),
    kode varchar(2)   NOT NULL,
    nama varchar(255) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (kode)
) ENGINE = InnoDB COLLATE = utf8mb4_unicode_ci;

CREATE TABLE kota
(
    id          varchar(5),
    id_provinsi varchar(2)   NOT NULL,
    kode        varchar(5)   NOT NULL,
    nama        varchar(255) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (kode),
    FOREIGN KEY (id_provinsi) REFERENCES provinsi (id)
) ENGINE = InnoDB COLLATE = utf8mb4_unicode_ci;

CREATE TABLE kecamatan
(
    id      varchar(8),
    id_kota varchar(5)   NOT NULL,
    kode    varchar(8)   NOT NULL,
    nama    varchar(255) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (kode),
    FOREIGN KEY (id_kota) REFERENCES kota (id)
) ENGINE = InnoDB COLLATE = utf8mb4_unicode_ci;

CREATE TABLE kelurahan
(
    id           varchar(13),
    id_kecamatan varchar(8)   NOT NULL,
    kode         varchar(13)  NOT NULL,
    nama         varchar(255) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (kode),
    FOREIGN KEY (id_kecamatan) REFERENCES kecamatan (id)
) ENGINE = InnoDB COLLATE = utf8mb4_unicode_ci;
