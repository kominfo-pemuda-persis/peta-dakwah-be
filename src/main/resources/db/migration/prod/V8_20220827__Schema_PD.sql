CREATE TABLE pd
(
    id         int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    kd_pw      varchar(10)                         NOT NULL,
    FOREIGN KEY (kd_pw) REFERENCES pw (kd_pw),
    kd_pd      varchar(10)                         NOT NULL UNIQUE,
    nama_pd    varchar(255)                        NOT NULL,
    diresmikan date                                NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE = InnoDB
    COLLATE = utf8mb4_unicode_ci;