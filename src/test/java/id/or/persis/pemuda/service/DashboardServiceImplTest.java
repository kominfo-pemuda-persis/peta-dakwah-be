package id.or.persis.pemuda.service;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import id.or.persis.pemuda.BaseTest;
import id.or.persis.pemuda.repository.AngketCabangRepository;
import id.or.persis.pemuda.repository.KeadaanJamaahRepository;
import id.or.persis.pemuda.repository.KeadaanPendidikanRepository;
import id.or.persis.pemuda.repository.ProfilMuballighRepository;
import id.or.persis.pemuda.repository.UserRepository;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

class DashboardServiceImplTest extends BaseTest {

    @InjectMocks
    private DashboardServiceImpl service;

    @Mock
    private UserRepository userRepository;

    @Mock
    private KeadaanJamaahRepository keadaanJamaahRepository;

    @Mock
    private KeadaanPendidikanRepository keadaanPendidikanRepository;

    @Mock
    private AngketCabangRepository angketCabangRepository;

    @Mock
    private ProfilMuballighRepository profilMuballighRepository;

    @Nested
    class GetSummary {

        @Test
        void shouldReturnEmptyWhenPcIdIs0() {
            given(userRepository.findKdPcByUserId(any(UUID.class)))
                .willReturn(Optional.empty());

            var actual = service.getSummary(new GetDashboardSummaryRequest(
                UUID.fromString("7b7927dc-e8c2-478c-86c4-cd788f18d90d")));

            assertThat(actual).isEqualTo(GetDashboardSummaryDto.empty());
        }

        @Test
        void shouldReturnDto() {
            var actual = prepareAndExecute();

            assertThat(actual).isNotNull();
            assertThat(actual.keadaanJamaahPc()).isEqualTo(1);
            assertThat(actual.keadaanPendidikanPc()).isEqualTo(1);
            assertThat(actual.angketPc()).isEqualTo(1);
            assertThat(actual.profilMuballighPc()).isEqualTo(1);
            assertThat(actual.keadaanJamaahPd()).isEqualTo(1);
            assertThat(actual.keadaanPendidikanPd()).isEqualTo(1);
            assertThat(actual.angketPd()).isEqualTo(1);
            assertThat(actual.profilMuballighPd()).isEqualTo(1);
            assertThat(actual.keadaanJamaahPw()).isEqualTo(1);
            assertThat(actual.keadaanPendidikanPw()).isEqualTo(1);
            assertThat(actual.angketPw()).isEqualTo(1);
            assertThat(actual.profilMuballighPw()).isEqualTo(1);
        }

        private GetDashboardSummaryDto prepareAndExecute() {
            given(userRepository.findKdPcByUserId(any(UUID.class)))
                .willReturn(Optional.of("1"));
            given(keadaanJamaahRepository.countByPcKdPc(anyString()))
                .willReturn(1);
            given(keadaanPendidikanRepository.countByPcKdPc(anyString()))
                .willReturn(1);
            given(angketCabangRepository.countByPcKdPc(anyString()))
                .willReturn(1);
            given(profilMuballighRepository.countByPcKdPc(anyString()))
                .willReturn(1);
            given(userRepository.findKdPdByUserId(any(UUID.class)))
                .willReturn(Optional.of("1"));
            given(keadaanJamaahRepository.countByKdPd(anyString()))
                .willReturn(1);
            given(keadaanPendidikanRepository.countByKdPd(anyString()))
                .willReturn(1);
            given(angketCabangRepository.countByKdPd(anyString()))
                .willReturn(1);
            given(profilMuballighRepository.countByKdPd(anyString()))
                .willReturn(1);
            given(userRepository.findKdPwByUserId(any(UUID.class)))
                .willReturn(Optional.of("1"));
            given(keadaanJamaahRepository.countByKdPw(anyString()))
                .willReturn(1);
            given(keadaanPendidikanRepository.countByKdPw(anyString()))
                .willReturn(1);
            given(angketCabangRepository.countByKdPw(anyString()))
                .willReturn(1);
            given(profilMuballighRepository.countByKdPw(anyString()))
                .willReturn(1);

            return service.getSummary(new GetDashboardSummaryRequest(
                UUID.fromString("7b7927dc-e8c2-478c-86c4-cd788f18d90d")));
        }

        @Test
        void shouldFindPcIdByUserId() {
            prepareAndExecute();

            verify(userRepository).findKdPcByUserId(UUID.fromString("7b7927dc-e8c2-478c-86c4-cd788f18d90d"));
        }

        @Test
        void shouldCountKeadaanJamaahByPcId() {
            prepareAndExecute();

            verify(keadaanJamaahRepository).countByPcKdPc("1");
        }

        @Test
        void shouldCountKeadaanPendidikanByPcId() {
            prepareAndExecute();

            verify(keadaanPendidikanRepository).countByPcKdPc("1");
        }

        @Test
        void shouldCountAngketCabangByPcId() {
            prepareAndExecute();

            verify(angketCabangRepository).countByPcKdPc("1");
        }

        @Test
        void shouldCountProfilMuballighByPcId() {
            prepareAndExecute();

            verify(profilMuballighRepository).countByPcKdPc("1");
        }

        @Test
        void shouldFindPdIdByUserId() {
            prepareAndExecute();

            verify(userRepository).findKdPdByUserId(UUID.fromString("7b7927dc-e8c2-478c-86c4-cd788f18d90d"));
        }

        @Test
        void shouldCountKeadaanJamaahByPdId() {
            prepareAndExecute();

            verify(keadaanJamaahRepository).countByKdPd("1");
        }

        @Test
        void shouldCountKeadaanPendidikanByPdId() {
            prepareAndExecute();

            verify(keadaanPendidikanRepository).countByKdPd("1");
        }

        @Test
        void shouldCountAngketCabangByPdId() {
            prepareAndExecute();

            verify(angketCabangRepository).countByKdPd("1");
        }

        @Test
        void shouldCountProfilMuballighByPdId() {
            prepareAndExecute();

            verify(profilMuballighRepository).countByKdPd("1");
        }

        @Test
        void shouldFindPwIdByUserId() {
            prepareAndExecute();

            verify(userRepository).findKdPwByUserId(UUID.fromString("7b7927dc-e8c2-478c-86c4-cd788f18d90d"));
        }

        @Test
        void shouldCountKeadaanJamaahByPwId() {
            prepareAndExecute();

            verify(keadaanJamaahRepository).countByKdPw("1");
        }

        @Test
        void shouldCountKeadaanPendidikanByPwId() {
            prepareAndExecute();

            verify(keadaanPendidikanRepository).countByKdPw("1");
        }

        @Test
        void shouldCountAngketCabangByPwId() {
            prepareAndExecute();

            verify(angketCabangRepository).countByKdPw("1");
        }

        @Test
        void shouldCountProfilMuballighByPwId() {
            prepareAndExecute();

            verify(profilMuballighRepository).countByKdPw("1");
        }
    }
}
