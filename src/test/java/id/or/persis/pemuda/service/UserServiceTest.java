package id.or.persis.pemuda.service;

import java.util.Collections;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import id.or.persis.pemuda.BaseTest;
import id.or.persis.pemuda.dto.GetUserDTO;
import id.or.persis.pemuda.entity.User;
import id.or.persis.pemuda.mapper.UserMapper;
import id.or.persis.pemuda.repository.UserRepository;
import id.or.persis.pemuda.specification.UserSpecification;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

class UserServiceTest extends BaseTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserMapper userMapper;

    @Nested
    class GetAllUser {
        @Test
        void shouldReturnDto() {
            assertThat(prepareAndExecute())
                .hasSize(1);
        }

        @SuppressWarnings({"rawtypes", "unchecked"})
        private Page<GetUserDTO> prepareAndExecute() {
            given(userRepository.findAll(any(Specification.class), any(Pageable.class)))
                .willReturn(new PageImpl(Collections.singletonList(
                    new User().withId(UUID.randomUUID())
                )));
            given(userMapper.toDto(any(User.class)))
                .willReturn(
                    new GetUserDTO()
                );

            return userService.getAllUser(new GetAllUsersRequest("12345",
                "rivaldi@rivaldi.dev",
                "rivaldi",
                "PC.1",
                "PD-1",
                "PW-1",
                Pageable.ofSize(5),
                "namaLengkap"
            ));
        }

        @Test
        void shouldCallUserRepositoryFindAll() {
            prepareAndExecute();

            verify(userRepository).findAll(any(UserSpecification.class), any(Pageable.class));
        }

        @Test
        void shouldMapToDto() {
            prepareAndExecute();

            verify(userMapper).toDto(any(User.class));
        }
    }
}
