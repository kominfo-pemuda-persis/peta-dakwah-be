package id.or.persis.pemuda;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.containers.output.Slf4jLogConsumer;
import org.testcontainers.utility.DockerImageName;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

/**
 * Created by IntelliJ IDEA.
 * Project : peta-dakwah-be
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/19/22
 * Time: 10:12
 * To change this template use File | Settings | File Templates.
 */
@SpringBootTest(webEnvironment = RANDOM_PORT)
public abstract class BaseIntegrationTest {
    private static final Logger logger = LoggerFactory.getLogger(BaseIntegrationTest.class);
    static final MySQLContainer<?> MySQLContainer;

    static {
        MySQLContainer = new MySQLContainer<>(DockerImageName.parse("mysql:8.0.30"))
                .withDatabaseName("peta_dakwah_test")
                .withUsername("luffy")
                .withPassword("s3cret")
                .withConfigurationOverride("mysql")
                .withLogConsumer(new Slf4jLogConsumer(logger))
                .withReuse(true);

        MySQLContainer.start();
    }

    @DynamicPropertySource
    static void datasourceConfig(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", MySQLContainer::getJdbcUrl);
        registry.add("spring.datasource.password", MySQLContainer::getPassword);
        registry.add("spring.datasource.username", MySQLContainer::getUsername);
    }
}
