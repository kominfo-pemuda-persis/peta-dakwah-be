package id.or.persis.pemuda;

import id.or.persis.pemuda.entity.PimpinanWilayah;
import id.or.persis.pemuda.repository.PimpinanWilayahRepository;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.StreamSupport;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Disabled("Disable because of too many logs in GitLab CI Runner")
class PetaDakwahBeApplicationTests extends BaseIntegrationTest {

    @Autowired
    private PimpinanWilayahRepository pimpinanWilayahRepository;

    @Test
    void findPWBySearchCriteria() {
        pimpinanWilayahRepository.save(new PimpinanWilayah(25, "PW-25", "KONOHA", LocalDate.now(), LocalDateTime.now(), LocalDateTime.now()));
        Iterable<PimpinanWilayah> pw = pimpinanWilayahRepository.findAll();
        List<PimpinanWilayah> result =
                StreamSupport.stream(pw.spliterator(), false).toList();

        assertThat(pw).hasSize(25);
        assertEquals("PW-1", result.get(0).getKdPw());
        assertEquals("Jawa Barat", result.get(0).getNamaPw());
    }
}
