# Peta Dakwah Backend

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=kominfo-pemuda-persis_peta-dakwah-be&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=kominfo-pemuda-persis_peta-dakwah-be)

Aplikasi backend Peta Dakwah

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

The things you need before installing the software.

* JDK 21
* Maven 3.9.5
* MySQL 8

### Installation

A step by step guide that will tell you how to get the development environment up and running.

```shell
$ mvn clean package
$ java -jar target/peta-dakwah-be-1.0.0.jar --spring.profiles.active=dev
```

## Usage

A few examples of useful commands and/or tasks.

```
$ First example
$ Second example
$ And keep this in mind
```

## Deployment

Additional notes on how to deploy this on a live or release system. Explaining the most important branches, what pipelines they trigger and how to update the database (if anything special).

## Database Migration Strategy

We use flyway as database migration strategy. run this command to migrate manually

### For Migrate in Dev
`mvn -Dflyway.user=$DB_USERNAME_DEV -Dflyway.schemas=$DB_NAME_DEV -Dflyway.password=$DB_PASSWORD_DEV -Dflyway.url=jdbc:mysql://$DB_SERVER_DEV:$DB_PORT_DEV/$DB_NAME_DEV -Dflyway.locations=db/migration/dev flyway:migrate`

### For Clean in Dev
`mvn -Dflyway.user=$DB_USERNAME_DEV -Dflyway.schemas=$DB_NAME_DEV -Dflyway.password=$DB_PASSWORD_DEV -Dflyway.url=jdbc:mysql://$DB_SERVER_DEV:$DB_PORT_DEV/$DB_NAME_DEV -Dflyway.locations=db/migration/dev flyway:clean`

make sure `DB_USERNAME_DEV`, `DB_NAME_DEV`, `DB_PASSWORD_DEV`, `DB_SERVER_DEV`, `DB_PORT_DEV`, `DB_NAME_DEV` has correctly added in system env variables

### User Accout

| #  | Username   | Role       | Password |
|----|------------|------------|----------|
| 1  | anggota    | anggota    | 12345    |
| 2  | superadmin | superadmin | 12345    |
| 3  | adminpp    | adminpp    | 12345    |
| 4  | tasykilpp  | tasykil_pp | 12345    |
| 5  | adminpw    | admin_pw   | 12345    |
| 6  | tasykilpw  | tasykil_pw | 12345    |
| 7  | adminpd    | admin_pd   | 12345    |
| 8  | tasykilpd  | tasykil_pd | 12345    |
| 9  | adminpc    | admin_pc   | 12345    |
| 10 | tasykilpc  | tasykil_pc | 12345    |
| 11 | adminpj    | admin_pj   | 12345    |


### Server

* Development: https://petadakwah-apidev.persis.or.id/swagger-ui/index.html
* Live: https://petadakwah-api.persis.or.id/swagger-ui/index.html

### Branches

* main: Latest code

## Additional Documentation and Acknowledgments

* Gitlab board: https://gitlab.com/kominfo-pemuda-persis/peta-dakwah-be/-/boards
